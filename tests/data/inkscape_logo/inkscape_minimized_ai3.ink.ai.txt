%!PS-Adobe-3.0 
%%Creator: Adobe Illustrator(TM) 3.2
%%AI8_CreatorVersion: 27.3.1
%%For: (Jonathan Neuhauser) ()
%%Title: (inkscape_minimized_ai3.ai)
%%CreationDate: 3/16/2023 12:01 PM
%%Canvassize: 16383
%%BoundingBox: 8 5 120 116
%%DocumentProcessColors: Cyan Magenta Yellow Black
%%DocumentNeededResources: procset Adobe_packedarray 2.0 0
%%+ procset Adobe_cmykcolor 1.1 0
%%+ procset Adobe_cshow 1.1 0
%%+ procset Adobe_customcolor 1.0 0
%%+ procset Adobe_IllustratorA_AI3 1.0 1
%AI3_ColorUsage: Color
%AI3_TemplateBox: 64 64 64 64
%AI3_TileBox: -242 -332 370 460
%AI3_DocumentPreview: None
%%PageOrigin:-233 -356.8898
%AI7_GridSettings: 72 8 72 8 1 0 0.8 0.8 0.8 0.9 0.9 0.9
%AI9_Flatten: 1
%AI12_CMSettings: 00.MS
%%EndComments
%%BeginProlog
%%IncludeResource: procset Adobe_packedarray 2.0 0
Adobe_packedarray /initialize get exec
%%IncludeResource: procset Adobe_cmykcolor 1.1 0
%%IncludeResource: procset Adobe_cshow 1.1 0
%%IncludeResource: procset Adobe_customcolor 1.0 0
%%IncludeResource: procset Adobe_IllustratorA_AI3 1.0 1
%%EndProlog
%%BeginSetup
Adobe_cmykcolor /initialize get exec
Adobe_cshow /initialize get exec
Adobe_customcolor /initialize get exec
Adobe_IllustratorA_AI3 /initialize get exec
%%EndSetup
0 A
u
U
u
*u
1 D
0 O
0.911711 0.786862 0.619532 0.974487 k
0 J 0 j 1 w 10 M []0 d63.4041 115.2646 m
59.9659 115.2646 56.5762 114.0189 54.1346 111.5283 C
13.076 69.4853 L
-2.47064 53.9435 23.1418 55.2402 32.41 49.3622 C
36.6952 46.5727 18.6562 42.9859 22.3435 39.2997 C
25.9312 35.6136 43.9707 32.2261 47.5584 28.6396 C
51.1461 24.9534 40.2832 21.068 43.8709 17.3818 C
47.3589 13.6956 55.7295 17.182 57.2244 8.81342 C
58.3206 2.63657 72.5729 5.72462 78.951 11.0048 C
82.9373 14.3921 72.0742 14.3929 75.6619 18.079 C
84.6311 27.1451 92.6037 22.1635 95.8924 30.5322 C
97.6862 35.0154 82.3377 38.2035 86.4236 41.0927 C
96.1901 47.9669 132.0684 51.4531 115.5252 67.9912 C
72.9705 111.5283 L
70.3296 114.0189 66.8423 115.2646 63.4041 115.2646 c
f
0 D
63.9978 113.2568 m
66.4644 113.2443 68.9306 112.3215 70.7244 110.5283 C
86.9705 93.9912 L
88.4654 92.4968 88.4651 89.4073 87.5682 88.5107 C
79.4959 95.0869 L
77.9002 85.4228 L
71.2244 89.0087 L
60.3611 82.1357 L
56.7732 96.581 L
50.9939 84.0283 L
36.5428 84.1279 L
33.7524 84.1279 34.1507 87.016 37.0408 89.9052 C
42.7213 96.1817 53.7833 106.8421 57.2713 110.5283 C
59.0651 112.3714 61.5313 113.2692 63.9978 113.2568 c
f
1 D
109.1346 35.5634 m
105.6839 35.4389 102.1702 33.7202 101.2732 30.5322 C
101.2732 28.44 117.5178 27.2444 116.6209 31.0302 C
115.9731 34.2183 112.5852 35.6879 109.1346 35.5634 c
f
37.7537 26.5107 m
33.0604 26.233 28.0377 22.8297 32.0115 19.4736 C
35.6989 16.2855 41.2802 20.1701 43.074 24.6533 C
41.9529 26.1165 39.887 26.6369 37.7537 26.5107 c
f
99.281 26.1494 m
94.6968 21.965 100.0777 17.5805 104.5623 20.4697 C
105.7582 21.2667 104.4632 25.1531 99.281 26.1494 C
f
*U
U
%%PageTrailer
gsave annotatepage grestore showpage
%%Trailer
Adobe_IllustratorA_AI3 /terminate get exec
Adobe_customcolor /terminate get exec
Adobe_cshow /terminate get exec
Adobe_cmykcolor /terminate get exec
Adobe_packedarray /terminate get exec
%%EOF
