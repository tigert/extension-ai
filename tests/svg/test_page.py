# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test that the page that Inkscape opens is how we expect it."""

from inkex import SvgDocumentElement, ShapeElement, TextElement, TextPath
import pytest


@pytest.mark.parametrize(
    "file, width, height",
    [
        ("rectangle", 595.275573730469, 841.889831542969),
        ("cmyk_rectangle", 595.275573730469, 841.889770507813),
        ("inkscape_minimized_ai3", 128.0, 128.0),
        ("2x4cm_origin_topleft_AI3", 58.0, 114.0),
        ("page_moved_to_1000_2000_rect200_300", 1920.0, 1080.0),
    ],
)
def test_viewbox(svgs, file, width, height):
    """Check the position of the viewbox."""
    svg: SvgDocumentElement = svgs[file]
    viewbox = [0, 0, width, height]
    assert svg.get_viewbox() == viewbox
    # assert False


@pytest.mark.parametrize(
    "file", ["page_moved_to_1000_2000_rect200_300", "525x801_origin_at_100_100_AI10"]
)
def test_elements_are_positioned_on_top_left_page_corner(svgs, file):
    """Check that elements on the page touch the top left page corner."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    assert layer.tag_name == "g"
    rect: ShapeElement = layer[0]
    bbox = rect.bounding_box()
    assert -1 < bbox.left < 1
    assert -1 < bbox.top < 1


def test_width_and_height_of_svg(process):
    """Check that the width and height attributes match the viewbox."""
    svg: SvgDocumentElement = process.svg
    bbox = svg.get_page_bbox()
    assert svg.viewport_height == bbox.height
    assert svg.viewport_width == bbox.width


def test_elements_are_inside_of_the_page(process):
    """All the SVG elements displayed should be inside of the page."""
    svg: SvgDocumentElement = process.svg
    bbox = svg.get_page_bbox()
    print(bbox, process)
    if process.name == "525x801_origin_at_100_100_AI3.ai":
        pytest.skip("AI3 sometimes does not have the correct page position.")
    if "text" in process.name:
        pytest.skip("BBox of text is bad in Inkex.")
    for element in reversed(
        list(svg.descendants())
    ):  # TODO: report error to inkex: reversed does not work on decendants()
        if hasattr(element, "bounding_box"):
            element_box = element.bounding_box()
            if element_box is not None:
                message = f" side of {element.tag_name} element '{element.get_id()}' is not visible in {process.name}."
                assert bbox.left <= element_box.left, "Left" + message
                assert bbox.right >= element_box.right, "Right" + message
                assert bbox.top <= element_box.top, "Top" + message
                assert bbox.bottom >= element_box.bottom, "Bottom" + message
