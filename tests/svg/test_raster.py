# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
from inkex import (
    SvgDocumentElement,
    Image,
    Layer,
    Transform,
)


def test_position_raster_image(svgs):
    """The position is specified in canvas coordinates inside the TextDocument"""
    svg: SvgDocumentElement = svgs.simple_raster
    layer_1: Layer = svg[-1]
    raster = None
    for i in layer_1:
        if isinstance(i, Image):
            raster = i
    assert raster is not None
    # This includes size and the first pixels of the first line
    assert raster.get("xlink:href").startswith(
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABb0AAAL8CAIAAABLeY/FAAEAAElEQVR4nOz9f5AT973g"
    )
    assert raster.transform == Transform("matrix(-1 0 0 -1 1508 772)")
