# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This adds tests for recognizing rectangles.

See https://gitlab.com/inkscape/extras/extension-ai/-/issues/27
"""

import pytest
import inkex
import math
from inkai.svg.coordinates import TestTransform
from inkai.svg.path import PathElement
from inkex import BaseElement
from inkai.svg.live_shape import LiveRectangle
from collections import namedtuple
from inkex import SvgDocumentElement
from inkai.parser.objects import objects

BEGIN_LAYER = "%AI5_BeginLayer\n"


def get_rect(data, file, cache={}) -> BaseElement:
    """Parse a rectangle and return the SVG shape of it."""
    if file in cache:
        return cache[file]
    cache[file] = (
        (PathElement() + LiveRectangle(TestTransform()))
        .generate_modifications_for(
            data.rectangle[file].read(binary=False)[len(BEGIN_LAYER) :],
            is_creating_a_compound_path=False,
        )
        .child
    )
    return cache[file]


@pytest.mark.parametrize(
    "file,tag_name",
    [
        ("rounded-1-different", "path"),
        ("rounded-2-different", "path"),
        ("rounded-all-different", "path"),
        ("rounded-equal-rotated", "rect"),
        ("rounded-equal", "rect"),
        ("90-deg-corners", "rect"),
    ],
)
def test_type_of_svg_element(data, file, tag_name):
    """Check the tag of the SVG rectangle."""
    path: BaseElement = get_rect(data, file)
    assert path.tag_name == tag_name
    assert "rect" in (
        path.tag_name,
        path.get("sodipodi:type"),
    ), "If we do not set the type to rect, the LPE does not work."


Rect = namedtuple("Rect", ["width", "height", "center", "corners", "angle"])
Point = namedtuple("Point", ["x", "y"])
Corner = namedtuple("Corner", ["CornerRadius"])

tt = TestTransform()


@pytest.mark.parametrize("width", [10, 100])
@pytest.mark.parametrize("height", [22.3, 88])
@pytest.mark.parametrize("center_x", [-1000, 0])
@pytest.mark.parametrize("center_y", [100, 20.234])
@pytest.mark.parametrize("angle", [0, math.pi / 4, math.pi / 6])
@pytest.mark.parametrize("corner_radius", [0, 2.3])
def test_SVG_rect_attributes(
    mock, width, height, center_x, center_y, angle, corner_radius
):
    """Check the coordinates of the rectangle."""
    p = LiveRectangle(tt)
    center = Point(center_x, center_y)
    top_left = Point(center_x - width / 2, center_y - height / 2)
    test_rect = Rect(width, height, center, [Corner(corner_radius)] * 4, angle)
    rect = p.create_regular_rectangle(test_rect)
    assert [float(rect.get("x")), float(rect.get("y"))] == tt.from_canvas(*top_left)
    assert float(rect.get("width")) == width * tt.large_canvas_scale
    assert float(rect.get("height")) == height * tt.large_canvas_scale
    if corner_radius == 0:
        assert rect.get("ry") is None
    else:
        assert float(rect.get("ry")) == corner_radius * tt.large_canvas_scale
    if angle == 0:
        assert rect.get("transform") is None
    else:
        tt_center = tt.from_canvas(*center)
        assert rect.get("transform") == str(
            inkex.Transform(
                f"rotate({math.degrees(angle)} {tt_center[0]} {tt_center[1]})"
            )
        )


@pytest.mark.parametrize(
    "file", ["rectangle", "cmyk_rectangle", "rectangle_with_rounded_corners"]
)
def test_last_element_is_rectangle(svgs, file):
    """Check the the path is replaced with a rectangle."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    rect: inkex.Rectangle = layer[-1]
    assert rect.tag_name == "rect"


@pytest.mark.parametrize("width", [10, 100])
@pytest.mark.parametrize("height", [22.3, 88])
@pytest.mark.parametrize("center_x", [-1000, 0])
@pytest.mark.parametrize("center_y", [100, 20.234])
@pytest.mark.parametrize("angle", [0, math.pi / 4, math.pi / 6])
@pytest.mark.parametrize("corner_index", [0, 1, 2, 3])
@pytest.mark.parametrize("corner_radius", [0, 2.3])
def test_SVG_path_attributes(
    mock, width, height, center_x, center_y, angle, corner_radius, corner_index
):
    """Check the coordinates of the rectangle path.

    Example path:

        m 102.65567,130.13068
        h 36.54752
        a 3.7041667,3.7041667 45 0 1 3.70417,3.70416
        v 27.39812
        a 3.7041667,3.7041667 135 0 1 -3.70417,3.70416
        h -36.54752
        a 3.7041667,3.7041667 45 0 1 -3.704162,-3.70416
        v -27.39812
        a 3.7041667,3.7041667 135 0 1 3.704162,-3.70416
        z
    """
    p = LiveRectangle(tt)
    center = Point(center_x, center_y)
    top_left = Point(center_x - width / 2, center_y - height / 2)
    test_corner_xy = [
        tt.from_canvas(*top_left),
        tt.from_canvas(center_x + width / 2, center_y - height / 2),
        tt.from_canvas(center_x + width / 2, center_y + height / 2),
        tt.from_canvas(center_x - width / 2, center_y + height / 2),
    ]
    corners = [Corner(1)] * 4  # fixed corner radius for all corners
    corners[corner_index] = Corner(
        corner_radius
    )  # changed radius for special corner to test
    test_rect = Rect(width, height, center, corners, angle)
    rect = p.create_irregular_rectangle(test_rect)
    assert [float(rect.get("x")), float(rect.get("y"))] == tt.from_canvas(*top_left)
    assert float(rect.get("width")) == width * tt.large_canvas_scale
    assert float(rect.get("height")) == height * tt.large_canvas_scale
    assert rect.get("ry") is None
    if angle == 0:
        assert rect.get("transform") is None
    else:
        tt_center = tt.from_canvas(*center)
        assert rect.get("transform") == str(
            inkex.Transform(
                f"rotate({math.degrees(angle)} {tt_center[0]} {tt_center[1]})"
            )
        )
    X = [test_corner_xy[i][0] for i in range(4)]
    Y = [test_corner_xy[i][1] for i in range(4)]
    R = [corners[i].CornerRadius * tt.large_canvas_scale for i in range(4)]
    W = width * tt.large_canvas_scale
    H = height * tt.large_canvas_scale
    expected_path = inkex.Path(
        f"""
        m {X[0] + R[0]},{Y[0]}
        h {W - R[0] - R[1]}
        a {R[1]},{R[1]} 45 0 1 {R[1]},{R[1]}
        v {H - R[1] - R[2]}
        a {R[2]},{R[2]} 135 0 1 {-R[2]},{R[2]}
        h -{W - R[2] - R[3]}
        a {R[3]},{R[3]} 45 0 1 {-R[3]},{-R[3]}
        v -{H - R[3] - R[0]}
        a {R[0]},{R[0]} 135 0 1 {R[0]},{-R[0]}
        z
    """
    )
    path = inkex.Path(rect.get("d"))
    print([(path[i].args, expected_path[i].args) for i in range(len(path))])
    assert list(map(str, path)) == list(map(str, expected_path))


@pytest.mark.parametrize(
    "file,radius",
    [
        ("rounded-1-different", ("81.0", "47.5", "47.5", "47.5")),
        ("rounded-2-different", ("81.0", "47.5", "18.5", "47.5")),
        ("rounded-all-different", ("81.0", "47.5", "18.5", "98.99982")),
    ],
)
def test_path_effect_parameters(data, file, radius):
    """Check the parameters of the path effects.

    <inkscape:path-effect
       effect="fillet_chamfer"
       id="path-effect4262"
       is_visible="true"
       lpeversion="1"
       satellites_param="F,0,0,1,0,12.93932,0,1 @ F,0,0,1,0,19.098553,0,1 @ F,0,0,1,0,0,0,1 @ F,0,0,1,0,32.144292,0,1"
       unit="px"
       method="auto"
       mode="F"
       radius="0"
       chamfer_steps="1"
       flexible="false"
       use_knot_distance="true"
       apply_no_radius="true"
       apply_with_radius="true"
       only_selected="false"
       hide_knots="false" />
    """
    lr = LiveRectangle(tt)
    mods = lr.generate_modifications_for(
        data.rectangle[file].read(binary=False)[len(BEGIN_LAYER) :]
    )
    pe = mods.svg.defs[0]
    # transform and reorder the corners
    radius = [
        round(float(r) * tt.large_canvas_scale, 8) for r in radius[2:] + radius[:2]
    ]
    params = f"F,0,0,1,0,{radius[0]},0,1 @ F,0,0,1,0,{radius[1]},0,1 @ F,0,0,1,0,{radius[2]},0,1 @ F,0,0,1,0,{radius[3]},0,1"
    properties = dict(
        effect="fillet_chamfer",
        is_visible="true",
        lpeversion="1",
        satellites_param=params,  # Inkscape 1.2
        nodesatellites_param=params,  # Inkscape 1.3
        unit="px",
        method="auto",
        mode="F",
        radius="0",
        chamfer_steps="1",
        flexible="false",
        use_knot_distance="true",
        apply_no_radius="true",
        apply_with_radius="true",
        only_selected="false",
        hide_knots="false",
    )
    for key, value in properties.items():
        assert (
            pe.get(key) == value
        ), f"path_effect.set({repr(key)}, {repr(value)}):\n    {repr(value)}\n == {repr(pe.get(key))}"


def test_the_path_effect_is_added_to_the_svg_file(svgs):
    """Path effects must be in the defs section."""
    svg: SvgDocumentElement = svgs.rectangles_origin
    defs: inkex.Defs = svg.defs
    assert len(defs) == 7, "We expect path effects."
    layer: inkex.Layer = svg[-1]
    assert len(layer) == 7, "We expect rectangles."
    for rect, effect in zip(layer, defs):
        assert rect.get("inkscape:path-effect") == "#" + effect.get_id()
