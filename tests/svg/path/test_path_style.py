# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the style of paths."""
from inkai.svg.coordinates import TestTransform
import pytest
from inkai.svg.path import PathCommands, NodeTypes, PathStyle
from inkex.paths import Path as SVGPath
from inkex import Style

# cmyk-fill-red-stroke-1pt-yellow-opacity-50
PATH_CMYK = """0 A
0 Xw
6 As
0 D
0 O
0 1 1 0 k
0 R
0 0 1 0 K
0 0.6 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
160 -200 m
267 -167 l
303 -227 l
184 -283 l
47 -160 l
108 -98 l
160 -200 l
b
"""

# fill-no-stroke-default
PATH_DEFAULT = """0 A
0 Xw
6 As
0 D
0 O
0 0 0 0 k
0 R
0 0 0 1 K
0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
160 -200 m
267 -167 l
303 -227 l
184 -283 l
47 -160 l
108 -98 l
160 -200 l
b
"""

# gray-fill-red-stroke-16pt-yellow
PATH_GRAY = """0 A
0 Xw
6 As
0 D
0 O
0.188235282897949 g
0 R
0.889999389648438 G
0 1 0 0 0 Xy
0 J 0 j 16 w 10 M []0 d
0 XR
160 -200 m
267 -167 l
303 -227 l
184 -283 l
47 -160 l
108 -98 l
160 -200 l
b
"""

# hsb-fill-red-stroke-16pt-yellow
PATH_HSB = """0 A
0 Xw
6 As
0 D
0 O
0 0.952239215373993 1 0 1 0.133333333333333 0 Xa
0 R
0.061829555779696 0 0.965423047542572 0 1 1 0 XA
0 1 0 0 0 Xy
0 J 0 j 16 w 10 M []0 d
0 XR
160 -200 m
267 -167 l
303 -227 l
184 -283 l
47 -160 l
108 -98 l
160 -200 l
b
"""

# rgb-fill-red-stroke-16pt-yellow-opacity-50
PATH_RGB = """0 A
0 Xw
6 As
0 D
0 O
0 0.993347048759461 1 0 1 0 0 Xa
0 R
0.061829555779696 0 0.965423047542572 0 1 1 0 XA
0 0.6 0 0 0 Xy
0 J 0 j 16 w 10 M []0 d
0 XR
160 -200 m
267 -167 l
303 -227 l
184 -283 l
47 -160 l
108 -98 l
160 -200 l
b
"""


# websave-rgb-fill-red-stroke-16pt-yellow
PATH_WEBSAVE = """0 A
0 Xw
6 As
0 D
0 O
0.003173876553774 0.855435967445374 1 0.001007095444947 0.937254901960784 0.294117647058824 0.050980392156863 Xa
0 R
0.061829555779696 0 0.965423047542572 0 1 1 0 XA
0 1 0 0 0 Xy
0 J 0 j 16 w 10 M []0 d
0 XR
160 -200 m
267 -167 l
303 -227 l
184 -283 l
47 -160 l
108 -98 l
160 -200 l
b
"""


def is_red(r, g, b, a):
    return r * 0.8 > g and r * 0.8 > b and a > 0


def is_yellow(r, g, b, a):
    return abs(g - r) < 0.2 and b * 1.2 < g and b * 1.2 < r and a > 0


def is_gray(gray):
    def is_gray(r, g, b, a):
        return r == g == b and 0.9 * gray <= r <= 1.1 * gray and a > 0

    is_gray.__name__ += f"_{gray}"
    return is_gray


def is_transparent(r, g, b, a):
    return a == 0


def rgb_string2float(s):
    assert len(s) == 7 and s.startswith("#")
    return (
        int(s[1:3], base=16) / 255,
        int(s[3:5], base=16) / 255,
        int(s[5:7], base=16) / 255,
    )


def test_is_red():
    """Make sure that testing for red works."""
    assert is_red(1, 0, 0, 1)
    assert is_red(1, 0.5, 0.5, 1)
    assert not is_red(1, 0.5, 0.5, 0)
    assert not is_red(0.5, 0.5, 0.5, 1)
    assert not is_red(0.5, 0.5, 0, 1)
    assert not is_red(0.5, 0, 0.5, 1)
    assert not is_red(0, 0.5, 0, 1)
    assert not is_red(0, 0, 0.5, 1)


def test_is_yellow():
    """Make sure that testing for red works."""
    assert is_yellow(1, 1, 0, 1)
    assert not is_yellow(1, 1, 1, 1)
    assert not is_yellow(0.4, 0.4, 0.4, 1)
    assert not is_yellow(0.4, 0.3, 0.3, 1)
    assert not is_yellow(0.3, 0.4, 0.3, 1)
    assert is_yellow(0.3, 0.4, 0.1, 1)
    assert is_yellow(0.3, 0.4, 0.1, 0.4)
    assert is_yellow(0.3, 0.4, 0.1, 0.01)
    assert not is_yellow(0.3, 0.4, 0.1, 0.0)


@pytest.mark.parametrize(
    "ai, color",
    [
        (PATH_RGB, is_red),
        (PATH_HSB, is_red),
        (PATH_GRAY, is_gray(1 - 0.188235282897949)),
        (PATH_CMYK, is_red),
        (PATH_DEFAULT, is_gray(1)),
        (PATH_WEBSAVE, is_red),
    ],
)
def test_FILL_of_example_path(ai, color):
    """Check the fill attribute of the example paths.

    The intention here is to cover all the types of color that AI offers.
    """
    style: Style = PathStyle().generate_modifications_for(ai).factory.style
    rgb = style["fill"]
    rgbf = rgb_string2float(rgb) + (float(style["fill-opacity"]),)
    assert color(*rgbf), f"{color.__name__}({rgb})"


@pytest.mark.parametrize(
    "ai, color",
    [
        (PATH_RGB, is_yellow),
        (PATH_HSB, is_yellow),
        (PATH_GRAY, is_gray(1 - 0.889999389648438)),
        (PATH_CMYK, is_yellow),
        (PATH_DEFAULT, is_gray(0)),
        (PATH_WEBSAVE, is_yellow),
    ],
)
def test_STROKE_of_example_path(ai, color):
    """Check the stroke attribute of the example paths.

    The intention here is to cover all the types of color that AI offers.
    """
    style: Style = PathStyle().generate_modifications_for(ai).factory.style
    rgb = style["stroke"]
    rgbf = rgb_string2float(rgb) + (float(style["stroke-opacity"]),)
    assert color(*rgbf), f"{color.__name__}({rgb})"


@pytest.mark.parametrize(
    "ai, line_width",
    [
        (PATH_RGB, "16.0"),
        (PATH_HSB, "16.0"),
        (PATH_GRAY, "16.0"),
        (PATH_CMYK, "1.0"),
        (PATH_WEBSAVE, "16.0"),
    ],
)
def test_STROKE_width_of_example_path(ai, line_width):
    """Check the stroke attribute of the example paths.

    The intention here is to cover all the types of color that AI offers.
    """
    style: Style = PathStyle().generate_modifications_for(ai).factory.style
    assert style["stroke-width"] == line_width


@pytest.mark.parametrize(
    "ai, opacity",
    [
        (PATH_RGB, 0.6),
        (PATH_HSB, 1),
        (PATH_GRAY, 1),
        (PATH_CMYK, 0.6),
        (PATH_WEBSAVE, 1),
    ],
)
def test_STROKE_opacity_of_example_path(ai, opacity):
    """Check the stroke attribute of the example paths.

    The intention here is to cover all the types of color that AI offers.
    """
    style: Style = PathStyle().generate_modifications_for(ai).factory.style
    assert float(style["stroke-opacity"]) == opacity


AI_DEFAULT_STYLE = (
    ""  # do not parse anything. This is for the style defined in the AI Spec
)


@pytest.mark.parametrize(
    "ai_string,style_property,style_value,message",
    [
        (AI_DEFAULT_STYLE, "fill", "none", "We can se the fill later"),
        (
            AI_DEFAULT_STYLE,
            "stroke",
            "#000000",
            "AI Spec page 55, d: The initial dash pattern is a solid line.",
        ),
        (
            AI_DEFAULT_STYLE,
            "stroke-opacity",
            "1",
            "Inkscape sets the opacity by default.",
        ),
        (
            AI_DEFAULT_STYLE,
            "stroke-width",
            "0.264583px",
            "AI Spec page 55, w: specified as a distance in user space. The initial linewidth is 1.0.",
        ),  # TODO: user space size calculation
        (
            AI_DEFAULT_STYLE,
            "stroke-linejoin",
            "miter",
            "AI Spec page 55, j: The initial linejoin is 0 [miter].",
        ),
        ("0 j", "stroke-linejoin", "miter", "AI Spec page 55, j"),
        ("1 j", "stroke-linejoin", "round", "AI Spec page 55, j"),
        ("2 j", "stroke-linejoin", "bevel", "AI Spec page 55, j"),
        (
            AI_DEFAULT_STYLE,
            "stroke-linecap",
            "butt",
            "AI Spec page 56 J: The initial linecap value is 0 [butt].",
        ),
        ("0 J", "stroke-linecap", "butt", "AI Spec page 56 J"),
        ("1 J", "stroke-linecap", "round", "AI Spec page 56 J"),
        ("2 J", "stroke-linecap", "square", "AI Spec page 56 J"),
        (
            AI_DEFAULT_STYLE,
            "stroke-miterlimit",
            None,
            "AI Spec page 55, M: The initial miter limit is 4. This is the same in Inkscape.",
        ),
        ("9.3 M", "stroke-miterlimit", "9.3", "AI Spec page 55, M"),
        ("1 1 1 Xa", "fill", "#FFFFFF", "Fill the path white."),
        ("0 0 0 Xa", "fill", "#000000", "Fill the path black."),
        ("b", "fill-opacity", "1", "Fill the path"),
        ("B", "fill-opacity", "1", "Fill the path"),
        ("f", "fill-opacity", "1", "Fill the path"),
        ("F", "fill-opacity", "1", "Fill the path"),
        ("N", "fill-opacity", "0", "Do not fill the path"),
        ("n", "fill-opacity", "0", "Do not fill the path"),
        ("S", "fill-opacity", "0", "Do not fill the path"),
        ("s", "fill-opacity", "0", "Do not fill the path"),
        ("b", "stroke-opacity", "1", "Stroke the path"),
        ("B", "stroke-opacity", "1", "Stroke the path"),
        ("s", "stroke-opacity", "1", "Stroke the path"),
        ("S", "stroke-opacity", "1", "Stroke the path"),
        ("N", "stroke-opacity", "0", "Do not stroke the path"),
        ("n", "stroke-opacity", "0", "Do not stroke the path"),
        ("f", "stroke-opacity", "0", "Do not stroke the path"),
        ("F", "stroke-opacity", "0", "Do not stroke the path"),
    ],
)
def test_style(ai_string, style_property, style_value, message):
    """Test the style of paths is properly retrieved from their spec."""
    style: Style = PathStyle().generate_modifications_for(ai_string).factory.style
    assert style.get(style_property) == style_value, message + f" [{ai_string}]"
