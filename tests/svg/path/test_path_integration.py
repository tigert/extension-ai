# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test integrating the SVG modifications for paths."""
import pytest
from inkai.svg.coordinates import TestTransform
from inkai.svg.path import Path
from inkex.paths import Curve, Line, Move, Path as SVGPath


d = TestTransform.from_drawing


PATH_SPEC_RECT_NO_GRADIENT = """268 631 m
268 649 L
245 649 L
245 631 L
268 631 L
B
"""  # from AI Specification page 73 - modified to not include the gradient

PATH_SPEC_CIRCLE_NO_GRADIENT = """237.5 636.4792 m
244.9198 636.4792 250.935 641.8612 250.935 648.5 c
250.935 655.1388 244.9198 660.5208 237.5 660.5208 c
230.0802 660.5208 224.065 655.1388 224.065 648.5 c
224.065 641.8612 230.0802 636.4792 237.5 636.4792 c
b
"""  # from AI Specification page 73 - modified to not include the gradient

DEFAULT_PATH_STYLE = "fill:none;stroke:#000000;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;stroke-width:0.264583px"


@pytest.mark.parametrize(
    "ai_path,svg_path",
    [
        (
            PATH_SPEC_RECT_NO_GRADIENT,
            {
                "d": "M -268 -631 L -268 -649 L -245 -649 L -245 -631 L -268 -631",
                "sodipodi:nodetypes": "scccc",
            },
        ),
        (
            PATH_SPEC_CIRCLE_NO_GRADIENT,
            {
                "d": "M -237.5 -636.479 C -244.92 -636.479 -250.935 -641.861 -250.935 -648.5 C -250.935 -655.139 -244.92 -660.521 -237.5 -660.521 C -230.08 -660.521 -224.065 -655.139 -224.065 -648.5 C -224.065 -641.861 -230.08 -636.479 -237.5 -636.479 Z",
                "sodipodi:nodetypes": "ssssss",
            },
        ),
        (PATH_SPEC_RECT_NO_GRADIENT, {"style": DEFAULT_PATH_STYLE + ";fill-opacity:1"}),
    ],
)
def test_path_creation(ai_path, svg_path):
    """Convert paths to SVG."""
    path: SVGPath = (
        Path(TestTransform())
        .generate_modifications_for(ai_path, is_creating_a_compound_path=False)
        .child
    )
    path_xml = path.tostring()
    print(f"path_xml = {path_xml}")
    print(f"expected = {svg_path}")
    for attr, value in svg_path.items():
        assert path.get(attr) == value


def test_the_path_style_changes_result_by_interpreting_the_path():
    """Check that the style receives the elements from the path."""
    path = Path(TestTransform())
    assert path.style.style["fill"] != "#FFFFFF"
    path.generate_modifications_for("1 1 1 Xa")
    assert path.style.style["fill"] == "#FFFFFF"
