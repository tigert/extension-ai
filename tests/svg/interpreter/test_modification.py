# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the modifications and the SVGBuilder as they both belong together."""
from inkai.svg.modification import (
    ModificationChain,
    NullModification,
    SetSVGAttributes,
    AddChildAttributes,
    ReplaceLastChild,
)
from inkai.svg.interpreter.observer import SVGBuilderAdapter
from unittest.mock import call
from inkai.svg.svg_builder import SVGBuilder, ImpossibleModificationException
import pytest
from inkex import PathElement, Layer


m1 = NullModification()
m2 = NullModification()
m3 = NullModification()


def test_equality():
    """Two modifications should be equal of they do the same."""
    x = NullModification()
    y = NullModification()
    y.id = x.id
    assert y == x


def test_inequality():
    """They differ in values."""
    assert m1 != m2
    assert m3 != m2
    assert m1 != m3


def test_modification_chain_takes_elements():
    """Check that elements are in the chain."""
    m = ModificationChain([m1, m2, m3])
    assert m.modifications == [m1, m2, m3]


def test_append_modifications():
    """Append modifications."""
    m = ModificationChain()
    m.append(m1)
    m.append(m2)
    assert m.modifications == [m1, m2]


def test_append_a_chain():
    m = ModificationChain()
    m.append(ModificationChain([m1, m2, m3]))
    assert m.modifications == [m1, m2, m3]


def test_modification_adapter_for_svg_builder(mock):
    """Check the the builder gets built."""
    observer = SVGBuilderAdapter(mock.builder)
    observer.new_modification(mock.modification)
    mock.modification.modify.assert_called_once_with(mock.builder)


def test_svg_modification(mock):
    """Modify the SVG."""
    SetSVGAttributes(x=3, y="asd").modify(mock)
    mock.svg.set.assert_has_calls([call("x", 3), call("y", "asd")])


def test_child_attributes_modification(mock):
    mock.last_child
    AddChildAttributes({"a": 2}, test="asd").modify(mock)
    mock.last_child.set.assert_has_calls([call("a", 2), call("test", "asd")])


def test_svg_builder_has_no_last_child(svg_builder: SVGBuilder):
    """The builder does not start with a child."""
    with pytest.raises(ImpossibleModificationException) as e:
        svg_builder.last_child


def test_svg_builder_has_last_child(svg_builder: SVGBuilder):
    """The builder should have a child when added."""
    e = PathElement()
    svg_builder.append_child(e)
    assert svg_builder.last_child is e


def test_svg_builder_changes_last_child(svg_builder: SVGBuilder):
    """The builder changes to the last added child."""
    e = PathElement()
    svg_builder.append_child(e)
    svg_builder.append_child(PathElement())
    assert svg_builder.last_child is not e


def test_last_child_is_in_the_parent(svg_builder: SVGBuilder):
    """The child must reside in the parent."""


def test_svg_is_current_parent(svg_builder: SVGBuilder):
    """The svg we start with."""
    assert svg_builder.current_parent is svg_builder.svg


def test_adding_a_parent(svg_builder: SVGBuilder):
    """We add a layer."""
    parent = Layer.new("Layer 1")
    old_parent = svg_builder.current_parent
    svg_builder.enter(parent)
    assert svg_builder.current_parent is parent
    svg_builder.exit()
    assert svg_builder.current_parent is old_parent
    assert old_parent[-1] is parent
    assert svg_builder.last_child is parent


def test_adding_a_child_to_a_parent(svg_builder: SVGBuilder):
    """We make sure the child is in the parent and the order matches."""
    c1 = PathElement()
    c2 = PathElement()
    parent = Layer.new("Layer 1")
    svg_builder.enter(parent)
    svg_builder.append_child(c1)
    svg_builder.append_child(c2)
    svg_builder.exit()
    assert len(parent) == 2
    assert parent[0] == c1
    assert parent[1] == c2


def test_add_a_definition(svg_builder: SVGBuilder):
    """Add defs to the svg defs section.

    We add a path.
    """
    p = PathElement()
    svg_builder.append_def(p)
    assert svg_builder.svg.defs[0] is p


def test_replace_a_child(svg_builder: SVGBuilder):
    """Replace the last child."""
    c1 = PathElement()
    c2 = PathElement()
    assert c1 != c2
    svg_builder.append_child(c1)
    assert len(svg_builder.current_parent) == 1
    svg_builder.replace_last_child_with(c2)
    assert len(svg_builder.current_parent) == 1
    assert svg_builder.last_child == c2
    assert svg_builder.current_parent[0] == c2


def test_replace_last_child_modification(mock):
    """Check that the call to the SVGBuilder is made."""
    p = PathElement()
    m = ReplaceLastChild(p)
    assert m.new_child is p
    m.modify(mock)
    mock.replace_last_child_with.assert_called_once_with(p)
