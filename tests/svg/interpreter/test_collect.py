# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test collecting AIElements."""

from inkai.svg.interpreter import CollectAIElements


def test_collection_through_interpretation():
    """Test that the elements get collected."""
    c = CollectAIElements()
    c.generate_modifications_for("1 1 m")
    assert len(c.ai_elements) == 1
    c.generate_modifications_for("1 1 m\nf")
    assert len(c.ai_elements) == 1 + 2
