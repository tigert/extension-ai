# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the conversion of groups."""

from inkai.svg.group import Group
from inkex import Group as SVGGroup
from inkai.svg.modification import AddAndEnterParent, ExitParent

GROUPS = """u
u
U
u
(1) Ln
U
U
"""


def test_groups_are_created():
    """Check that all are groups."""
    modifications = Group().generate_modifications_for(
        GROUPS, is_creating_a_compound_path=False
    )
    for modification in modifications:
        group: SVGGroup = modification.parent
        assert group.tag_name == "g"


def test_empty_group_is_omitted():
    """Empty groups are removed."""
    modifications = Group().generate_modifications_for(
        GROUPS, is_creating_a_compound_path=False
    )
    modifications.assert_matches(
        [
            AddAndEnterParent,
            AddAndEnterParent,
            ExitParent,
            ExitParent,
        ]
    )


def test_groups_are_not_created_in_a_compound_path():
    """No groups must be created inside of a compound path.

    Compound paths use grouping inside.
    But this is not the same as a group in an SVG file.
    """
    modifications = Group().generate_modifications_for(
        GROUPS, is_creating_a_compound_path=True
    )
    modifications.assert_matches([])
