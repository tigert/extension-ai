# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import pytest
from inkai.parser.text_element_ai_11 import AI11BasicTextDocumentParser

from inkai.modular_parser import Parser, ParseActions
from pyparsing import (
    ParseResults,
    ParserElement,
    Regex,
    Forward,
    ZeroOrMore,
    QuotedString,
    Literal,
    pyparsing_common,
    Suppress,
)


class AI11TextDocumentParser(Parser):
    """This parser is the reference implementation for text parsing.
    It's slow, so we only use it for unit testing."""

    def add_default_parse_actions(self):
        """Add a set of default parse actions."""
        # TODO
        self.actions.add(TextElementObjectification())

    def create_tokens(self):
        pass
        self.create_text_document()

    def create_text_document(self):
        ParserElement.set_default_whitespace_chars("\r ")
        self.flag = Regex(r"/(\d\d?|[\w\d]+)")
        object = Forward()
        value = Forward()
        self.list = Suppress("[") + ZeroOrMore(value) + Suppress("]")
        self.subobject = Suppress("<<") + object + Suppress(">>")
        self.text = QuotedString("(", end_quote_char=")", esc_char="\\", multiline=True)
        self.datatype = Regex(r"/[\w\d]+")
        value <<= (
            (pyparsing_common.number)
            | self.text
            | self.datatype
            | self.subobject
            | self.list
            | ((Literal("false") | "true")).add_parse_action(lambda x: x[0] == "true")
        )
        object <<= ZeroOrMore(self.flag + value)
        self.textdocument = Suppress(Regex(r"\s*")) + object
        self.object = object
        self.value = value


class TextElementObjectification(ParseActions):
    def parse_flag(self, p: ParseResults):
        return p[0]

    def parse_subobject(self, p: ParseResults):
        """Process a parsed object into a dictionary"""
        it = iter(p)
        result = {}
        for x in it:
            result[x] = next(it)
        return result

    def parse_textdocument(self, p: ParseResults):
        return self.parse_subobject(p)

    def parse_list(self, p: ParseResults):
        return [list(p)]

    def parse_text(self, p: ParseResults):
        """Encode parsed text back to bytes."""
        return p


@pytest.fixture()
def textdoc(data):
    return data.grammar["extracted_text_document"].read(binary=True).decode("latin-1")


@pytest.fixture()
def reference_result(textdoc):
    parser = AI11TextDocumentParser()
    return parser.textdocument.parse_string(textdoc, parse_all=True)[0]


@pytest.fixture()
def parse_result(textdoc):
    return AI11BasicTextDocumentParser(textdoc).parse()


def test_parser_completes(parse_result):
    """Check that the implemented parser completes."""
    assert len(parse_result) == 3
    assert "/1" in parse_result
    assert len(parse_result["/1"]) == 4


def test_reference_parser(reference_result):
    """Check that the reference parser completes."""
    assert len(reference_result) == 3
    assert "/1" in reference_result
    assert len(reference_result["/1"]) == 4


def test_compare_parsers(reference_result, parse_result):
    """Check that both parsers give the same result."""

    def dict_compare(this, other):
        assert isinstance(other, dict)
        assert isinstance(this, dict)
        assert len(this) == len(other)
        assert set(this.keys()) == set(other.keys())
        for key in this:
            value = this[key]
            otherval = other[key]
            value_compare(value, otherval)

    def list_compare(this, other):
        assert isinstance(other, list)
        assert isinstance(this, list)
        assert len(this) == len(other)
        for value, otherval in zip(this, other):
            value_compare(value, otherval)

    def value_compare(value, otherval):
        if isinstance(value, dict):
            dict_compare(value, otherval)  # compare dicts
        elif isinstance(value, list):
            list_compare(value, otherval)
        else:
            # strings might differ (pyparsing removes escape chars)
            if not (isinstance(value, str)):
                assert value == otherval

    dict_compare(reference_result, parse_result)
