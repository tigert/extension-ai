# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""

SimpleStyle.Paint

Example:

    %AI9_BeginArtStyles
    /KnownStyle :
    ([Default]) /Name ,
    /SimpleStyle :
    0 O
    0 0 0 0 1 1 1 Xa
    0 R
    0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0 0 0 XA
    0 1 0 0 0 Xy
    0 J 0 j 1 w 10 M []0 d
    0 XR
    1 1 Xd
    /Paint ;
    /Def ;
    %AI9_EndArtStyles


"""

import pytest
from inkai.parser.hierarchy_objects import SimpleStyle
from inkai.parser.lazy import CollectedSection


def test_convert_simple_style_content_to_ai_elements():
    """Check that we can access the AIElements."""
    s = SimpleStyle()
    s.Paint = ["", "0 O\n0 0 0 0 1 1 1 Xa"]
    commands: CollectedSection = s.paint
    assert len(commands.children) == 2
    assert commands.children[0].first_line == "0 O"
