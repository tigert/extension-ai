# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test how the text hierarchy works."""

import pytest
from inkai.parser.text.hierarchy import TextDocumentObject, Def


def test_access_attribtues():
    """Check a simple attribute access."""
    o = TextDocumentObject(Def("test", "lalala", {}), {})
    assert o.name == "test"
    assert o.documentation == "lalala"


@pytest.mark.parametrize("name", ["attr", "NoseBleed"])
@pytest.mark.parametrize("value", [1, 0.4, "valuable!"])
@pytest.mark.parametrize("index", [1, 100, 0])
def test_defined_attributes(name, value, index):
    """Check a simple attribute access."""
    o = TextDocumentObject(
        Def("test", "lalala", {index: Def(name, "", {})}), {f"/{index}": value}
    )
    assert getattr(o, name) == value


@pytest.mark.parametrize("name1", ["attr", "NoseBleed"])
@pytest.mark.parametrize("name2", ["x", "Y"])
@pytest.mark.parametrize("value", [1, 0.4, "valuable!"])
@pytest.mark.parametrize("index1", [1, 100])
@pytest.mark.parametrize("index2", [99, 0])
def test_hierarchy(name1, name2, value, index1, index2):
    """Check that attributes can be nested."""
    o = TextDocumentObject(
        Def("test", "lalala", {index1: Def(name1, "", {index2: Def(name2, "", {})})}),
        {f"/{index1}": {f"/{index2}": value}},
    )
    o1 = getattr(o, name1)
    assert getattr(o1, name2) == value


@pytest.mark.parametrize("name1", ["attr", "NoseBleed"])
@pytest.mark.parametrize("index1", [1, 100])
@pytest.mark.parametrize("index2", [99, 0])
def test_list_access(name1, index1, index2):
    """Check that lists get mapped."""
    o = TextDocumentObject(
        Def("test", "lalala", {index1: Def(name1, "", {index2: Def("name2", "", {})})}),
        {f"/{index1}": [{f"/{index2}": 10}, {f"/{index2}": 20}]},
    )
    o1 = getattr(o, name1)
    assert isinstance(o1, list)
    print(o1)
    assert o1[0].name2 == 10
    assert o1[1].name2 == 20


def test_name_with_no_value():
    """We inform when a value is absent."""
    o = TextDocumentObject(Def("test", "lalala", {1: Def("asd", "", {})}), {})
    assert o.asd == None


def test_invalid_attribute():
    """We inform when the name is wrong."""
    o = TextDocumentObject(Def("test", "lalala", {}), {})
    with pytest.raises(AttributeError) as e:
        o.asd
    assert "asd" in str(e.value)
