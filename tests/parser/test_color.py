# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test creating colors from strings.

See AI Spec page 60+
"""
import pytest
from inkai.parser.color import Color
from inkai.parser.lines import LineParser


@pytest.mark.parametrize(
    "string,value,fill,name,alpha,identity",
    [
        ("1 g", [1], True, "", 1, (False, False, True)),
        ("0 G", [0], False, "", 1, (False, False, True)),
        ("0.03 G", [0.03], False, "", 1, (False, False, True)),
        ("1 1 1 1 k", [1, 1, 1, 1], True, "", 1, (False, True, False)),
        ("1 0 0.2 .3 K", [1, 0, 0.2, 0.3], False, "", 1, (False, True, False)),
        ("0 0 0 0 k", [0, 0, 0, 0], True, "", 1, (False, True, False)),
        (
            "0.45 0 0.25 0 (PANTONE 570 CV) 0 x",
            [0.45, 0, 0.25, 0],
            True,
            "PANTONE 570 CV",
            1,
            (False, True, False),
        ),
        (
            "0.45 0 0.25 1 (PANTONE 570 CV Stroke) 0.3 X",
            [0.45, 0, 0.25, 1],
            False,
            "PANTONE 570 CV Stroke",
            0.7,
            (False, True, False),
        ),
        ("1 0.3 0 Xa", [1, 0.3, 0], True, "", 1, (True, False, False)),
        ("0 0.3 0.4 XA", [0, 0.3, 0.4], False, "", 1, (True, False, False)),
        (
            "0 0.3 0.4 (RGB) 0.3 1 Xx",
            [0, 0.3, 0.4],
            True,
            "RGB",
            0.7,
            (True, False, False),
        ),
        (
            "0 0.3 0.4 0.5 (cmyk) 0 0 XX",
            [0, 0.3, 0.4, 0.5],
            False,
            "cmyk",
            1,
            (False, True, False),
        ),
    ],
)
def test_color(string, value, fill, name, alpha, identity):
    """Check the all the colors."""
    color = Color.from_string(string)
    print(color)
    assert color.is_fill() == fill
    assert color.is_stroke() == (not fill)
    assert color.name == name
    assert color.alpha == alpha
    assert color.rgb == (value if identity[0] else [])
    assert color.cmyk == (value if identity[1] else [])
    assert color.gray == (value if identity[2] else [])


@pytest.mark.parametrize("fill", (True, False))
@pytest.mark.parametrize(
    "string,rgb,cmyk,alpha,name",
    [
        (
            "0.9 0.7 0.6 0.9 0 0 0 ([Name]) 0 1 Xz",
            [],  # not sure about RGB
            [0.9, 0.7, 0.6, 0.9],
            1,
            "[Name]",
        ),
        ("0 0 0 0 1 1 1 Xa", [1, 1, 1], [0, 0, 0, 0], 1, ""),
        (
            "0.911711275577545 0.786861956119537 0.619531512260437 0.974486887454987 0 0 0 ([Passermarken]) 0 1 Xz",
            [],  # not sure abour RGB
            [
                0.911711275577545,
                0.786861956119537,
                0.619531512260437,
                0.974486887454987,
            ],
            1,
            "[Passermarken]",
        ),
        (
            "0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 ([Registration]) 0 Xs",
            [],
            [
                0.749721467494965,
                0.679194271564484,
                0.670496642589569,
                0.901457190513611,
            ],
            1,
            "[Registration]",
        ),
    ],
)
def test_new_colors(fill, string, rgb, cmyk, alpha, name):
    """Test the colors that we reverse-engineered.

    Some of this might be a bit unsure so if you would like to change something,
    feel free to reach out.
    """
    if not fill:
        string = string[:-1] + string[-1].upper()
    color = Color.from_string(string)
    print(color)
    assert color.is_fill() == fill
    assert color.is_stroke() == (not fill)
    assert color.name == name
    assert color.alpha == alpha
    assert color.rgb == rgb
    assert color.cmyk == cmyk
    assert color.gray == []


class CSSColor(Color):
    """Subclass of color to remove paring."""

    def __init__(self, values):
        super().__init__(LineParser.from_string(""))
        self.values = values

    @property
    def gray(self):
        return self.values.get("gray", [])

    @property
    def rgb(self):
        return self.values.get("rgb", [])

    @property
    def cmyk(self):
        return self.values.get("cmyk", [])


@pytest.mark.parametrize(
    "value, css",
    [
        (
            {"gray": [0]},
            "#FFFFFF",
        ),  # Second cell is white (gray scale of 0). AI Spec page 80.
        (
            {"gray": [1]},
            "#000000",
        ),  # First cell is black (gray scale of 1). AI Spec page 80.
        ({"gray": [0.5]}, "#808080"),
        ({"rgb": [0.503, 0.503, 0.503]}, "#808080"),
        ({"rgb": [1, 1, 1]}, "#FFFFFF"),
        ({"rgb": [0, 0, 0]}, "#000000"),
        ({"cmyk": [0.5, 0.14, 0.69, 0]}, "#80DB4F"),
        ({"cmyk": [0.09, 0.72, 0.22, 0.56]}, "#661F58"),
        ({"cmyk": [0.09, 0.72, 0.22, 1]}, "#000000"),
        ({}, ""),
    ],
)
def test_convert_color_to_a_CSS_usable_string(value, css):
    """This converts the color to CSS."""
    color = CSSColor(value)
    assert color.rgb_string == css


def test_palette_color_none():
    """There is a color for no fill, no stroke.

    See AI Spec page 79.
    """
    color: Color = Color.from_string("Pn")
    assert color.rgb == [0, 0, 0]
    assert color.gray == [0]
    assert color.cmyk == [0, 0, 0, 1]
    assert color.name == "none"
    assert color.alpha == 0
    assert color.is_fill()
    assert color.is_stroke()
