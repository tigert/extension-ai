# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This tests the utility functions."""
import pytest
from inkai.parser.units import header_comments_to_units, to_px

VALID_UNITS = [
    ("", "", "px"),
    ("2", "", "pt"),
    ("6", "", "px"),
    ("0", "", "in"),
    ("3", "", "pc"),
    ("2", "10", "ft"),
    ("2", "7", "ft,in"),
    ("2", "9", "yd"),
    ("1", "", "mm"),
    ("4", "", "cm"),
    ("2", "8", "m"),
    ("5", "", "hpgl"),
]


@pytest.mark.parametrize("ai5,ai24,unit", VALID_UNITS)
def test_unit_table(ai5, ai24, unit):
    """This tests the table for the units

    See docs/specification_amendments/units.md
    """
    assert header_comments_to_units(ai5, ai24) == unit


INVALID_UNITS = [
    (ai5, ai24)
    for ai5 in ("2", "", "7", "4", "111")
    for ai24 in ("2", "", "4", "111")
    if not any(unit[0] == ai5 and unit[1] == ai24 for unit in VALID_UNITS)
]


@pytest.mark.parametrize("ai5,ai24", INVALID_UNITS)
def test_invalid_unit(ai5, ai24):
    """ "Test unit invalidity."""
    with pytest.raises(ValueError):
        header_comments_to_units(ai5, ai24)


@pytest.mark.parametrize(
    "unit,px",
    [
        ("pt", 1),
        ("px", 1),
        ("in", 72),
        ("pc", 12),
        ("ft", 12 * 72),
        ("ft,in", 12 * 72),
        ("yd", 36 * 72),
        ("mm", 72 / 25.4),
        ("cm", 72 / 2.54),
        ("m", 72 / 0.254),
        ("hpgl", 72 / 25.4 / 4),
    ],
)
@pytest.mark.parametrize("length", [0, 10, 200])
def test_pixels_per_unit(unit, px, length):
    """Check the unit calclulation."""
    assert to_px(unit, 1) == px
    assert to_px(unit, length) == px * length


@pytest.mark.parametrize("invalid_unit", ["", "12", "PX"])
def test_pixels_per_unit(invalid_unit):
    """Check the unit calclulation raises an error."""
    with pytest.raises(ValueError):
        to_px(invalid_unit, 1)
