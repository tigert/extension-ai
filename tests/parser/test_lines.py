# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test parsing lines."""
from inkai.parser.lines import LineParser, ReachedEndOfFile, CollectedLines, Lines
import pytest


class LineReader:
    """Read lines."""

    def __init__(self, lines, newline):
        """Create a new LineReader."""
        self.lines = lines
        self.newline = newline

    def readline(self):
        """Read a line or return an empty string."""
        if not self.lines:
            return ""
        if len(self.lines) == 1:
            return self.lines.pop(0)
        return self.lines.pop(0) + self.newline


@pytest.mark.parametrize("newline", ["\r", "\r\n", "\n"])
@pytest.mark.parametrize("eos", ["\r", "\n", "\r\n", ""])
@pytest.mark.parametrize(
    "check_end", [True, False]
)  # Switch on and off because of possible side-effects
def test_lines(eos, newline, check_end):
    """Test reading lines.

    eos is the end of the string
    newline is what the readline() method assumes to be a newline.
        This tests Windows, Mac, Linux.
    """
    string = "line1\nline2\r\n\rline4" + eos
    lines = string.split(newline)
    reader = LineReader(lines, newline)
    lp = LineParser(reader)
    # no line read, yet
    assert not check_end or not lp.reached_end()
    assert lp.current_line == ""
    assert lp.current_line_end == ""
    assert lp.current_line_number == 0
    # line 1
    assert not check_end or not lp.reached_end()
    assert lp.read_line() == "line1"
    assert lp.current_line == "line1"
    assert lp.current_line_end == "\n"
    assert lp.current_line_number == 1
    # line 2
    assert not check_end or not lp.reached_end()
    assert lp.read_line() == "line2"
    assert lp.current_line == "line2"
    assert lp.current_line_end == "\r\n"
    assert lp.current_line_number == 2
    # line 3
    assert not check_end or not lp.reached_end()
    assert lp.read_line() == ""
    assert lp.current_line == ""
    assert lp.current_line_end == "\r"
    assert lp.current_line_number == 3
    # line 4
    assert not check_end or not lp.reached_end()
    assert lp.read_line() == "line4"
    assert lp.current_line == "line4"
    assert lp.current_line_end == eos
    assert lp.current_line_number == 4
    # line 5 does not exist
    assert not check_end or lp.reached_end()
    with pytest.raises(ReachedEndOfFile) as e:
        lp.read_line()
    assert not check_end or lp.reached_end()
    assert lp.current_line == "line4"
    assert lp.current_line_end == eos
    assert lp.current_line_number == 4
    error = e.value
    assert error.args[0] == "Reached end of file after line 4."


STRING = """a
b
c
d
e
f
"""


@pytest.mark.parametrize(
    "start,until_value,expected_lines",
    [
        (0, "a", ["a"]),
        (0, "b", ["a", "b"]),
        (0, "c", ["a", "b", "c"]),
        (1, "d", ["b", "c", "d"]),
        (1, "f", ["b", "c", "d", "e", "f"]),
        (3, "f", ["d", "e", "f"]),
    ],
)
def test_collect_until(start, until_value, expected_lines):
    """Make sure we can collect lines from the files."""
    lines = LineParser.from_string(STRING)
    for i in range(start + 1):
        lines.read_line()
    collected = lines.collect_until(
        lambda line: line == until_value,
        start_at_current_line=True,
        include_last_tested_line=True,
    )
    result = []
    line_number = start
    while not collected.reached_end():
        line_number += 1
        result.append(collected.read_line())
        assert collected.current_line_number == line_number, repr(
            collected.current_line
        )
    assert collected.current_line_number == line_number
    assert result == expected_lines
    with pytest.raises(ReachedEndOfFile) as e:
        collected.read_line()
    assert collected.current_line == result[-1]
    assert collected.current_line_number == line_number
    error = e.value
    assert error.args[0] == f"Reached end of collected lines after line {line_number}."


@pytest.mark.parametrize("start", [2, 10])
def test_collect_from_collection(start):
    lines = CollectedLines(["a", "b", "c", "d"], start)
    collected = lines.collect_until(
        lambda line: line == "c", start_at_current_line=True
    )
    result = []
    assert collected.current_line == "a"
    while not collected.reached_end():
        assert collected.current_line_number == start
        line = collected.read_line()
        result.append(line)
        assert collected.current_line == line
        start += 1
    assert collected.current_line_number == start
    assert result == ["a", "b"]


def test_collect_with_first_line():
    """It might be that we want to include the first line.
    In that case, the line numbers should match.
    """
    lines = CollectedLines(["1", "2", "3", "4"], 4)
    lines.read_line()  # We are in this line that should be included.
    assert lines.current_line_number == 5
    collected = lines.collect_until(
        lambda line: line == "3", start_at_current_line=True
    )
    assert collected.read_line() == "1"
    assert collected.current_line == "1"
    assert collected.current_line_number == 5


def collected_lines():
    return CollectedLines(["a", "b", "c", "d", "e"], 0)


def collected_lines_with_one_line_read():
    l = collected_lines()
    l.read_line()
    return l


def line_parser():
    return LineParser.from_string("a\nb\nc\nd\ne\n")


def line_parser_with_one_line_read():
    l = line_parser()
    l.read_line()
    return l


@pytest.mark.parametrize(
    "get_lines",
    [
        collected_lines,
        collected_lines_with_one_line_read,
        line_parser,
        line_parser_with_one_line_read,
    ],
)
@pytest.mark.parametrize(
    "before,line,index,next",
    [
        ("", "a", 1, "b"),
        ("a", "b", 2, "c"),
        ("b", "c", 3, "d"),
        ("c", "d", 4, "e"),
    ],
)
@pytest.mark.parametrize("consume_last", [True, False])
@pytest.mark.parametrize("include_last", [True, False])
@pytest.mark.parametrize("include_first", [False, True])
@pytest.mark.parametrize("test_index", list(range(8)))
def test_collection_not_including_the_last_line(
    get_lines,
    before,
    line,
    index,
    next,
    include_first,
    consume_last,
    include_last,
    test_index,
):
    """Check that the last line can also be excluded from collection."""
    message = f" | {get_lines.__name__} line={repr(line)} consume_last={consume_last} include_first={include_first} include_last={include_last}"
    lines: Lines = get_lines()
    lines_first_line = lines.current_line
    collected = lines.collect_until(
        lambda l: l == line,
        continue_after_last_tested_line=consume_last,
        include_last_tested_line=include_last,
        start_at_current_line=include_first,
    )
    if test_index == 1:
        assert lines.current_line == (line if consume_last else before), (
            f"{test_index}: line before read_line()" + message
        )
    if test_index == 2:
        assert lines.current_line_number == (index if consume_last else index - 1), (
            f"{test_index}: line index before read_line()" + message
        )
    read_line = lines.read_line()
    if test_index == 3:
        assert read_line == (next if consume_last else line), (
            f"{test_index}: line of read_line()" + message
        )
    if test_index == 4:
        assert lines.current_line == (next if consume_last else line), (
            f"{test_index}: line after read_line()" + message
        )
    if test_index == 5:
        assert lines.current_line_number == (index + 1 if consume_last else index), (
            f"{test_index}: line index after read_line()" + message
        )
    last_line = first_line = no_line = "no line"
    while not collected.reached_end():
        last_line = collected.read_line()
        if first_line is no_line:
            first_line = last_line
    if test_index == 6:
        if line == "a":
            assert last_line == ("a" if include_last or include_first else no_line), (
                f"{test_index}: last line a " + message
            )
        elif line == "b":
            assert last_line == (
                "b" if include_last else ("a" if include_first else no_line)
            ), (f"{test_index}: last line b " + message)
        else:
            assert last_line == (line if include_last else before), (
                f"{test_index}: last line" + message
            )
    if test_index == 7:
        if line == "a":
            assert first_line == ("a" if include_first or include_last else no_line), (
                f"{test_index}: line a: first and last lines are the same" + message
            )
        elif line == "b":
            assert first_line == (
                "a" if include_first else ("b" if include_last else no_line)
            ), (f"{test_index}: line b: can have no line" + message)
        else:
            assert first_line == ("a" if include_first else "b"), (
                f"{test_index}: first line" + message
            )


@pytest.mark.parametrize(
    "Lines", [line_parser_with_one_line_read, collected_lines_with_one_line_read]
)
def test_current_line_is_known_when_collected(Lines):
    """This created more meaningful str(AIElements)."""
    lines = Lines()
    collected = lines.collect_until(lambda line: line == "b")
    assert collected.current_line == "a"
    assert collected.current_line_end == "\n"


BEGIN_DATA_EXAMPLE = """[ 1 0 0 1 0 0 ] 0 0 667 789 667 789 8 3 0 0 0 0
%%BeginData: 13
XI
%FFFFF\n\n\n
%%EndData
XH
"""


@pytest.mark.parametrize("use_collected_lines", [False, True])
def test_special_case_begin_data(use_collected_lines):
    """BeginData: CHARS is a special case to cover:
    There is no reason to split this binaru blob into lines and join them again.
    They should be skipped for performance reasons.
    """
    lp = LineParser.from_string(BEGIN_DATA_EXAMPLE)
    if use_collected_lines:
        lp = lp.collect_until(
            lambda line: line == "XH",
            include_last_tested_line=True,
            start_at_current_line=True,
        )
    assert lp.read_line() == "[ 1 0 0 1 0 0 ] 0 0 667 789 667 789 8 3 0 0 0 0"
    assert lp.read_line() == "%%BeginData: 13"
    assert lp.current_line_number == 2
    assert (
        lp.read_line()
        == """XI
%FFFFF\n\n\n
"""
    )
    assert lp.current_line_number == 3
    assert lp.read_line() == "%%EndData"
    assert lp.current_line_number == 8
    assert lp.read_line() == "XH"
    assert lp.current_line_number == 9
