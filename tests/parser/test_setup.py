# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the grammar of the Adobe Illustrator format for the lazy parser."""

import pytest
from inkai.parser.setup.setup import Setup
from inkai.parser.setup.procset_exec import ProcsetExec

SETUP_PGI_1 = """%%BeginSetup
%AI5_Begin_NonPrinting
Np
%AI8_PluginGroupInfo
(Adobe Vectorized Object) (Image Tracing) (Vectorize.aip)
%AI5_End_NonPrinting--
%%EndSetup
"""

ARTSTYLE_1_CONTENT = """/KnownStyle :
([Default]) /Name ,
/SimpleStyle :
0 O
0 0 0 0 1 1 1 Xa
0 R
0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0 0 0 XA
0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
1 1 Xd
/Paint ;
 /Def ;"""

ARTSTYLE_1 = f"""%%BeginSetup
%AI9_BeginArtStyles
{ARTSTYLE_1_CONTENT}
%AI9_EndArtStyles
%%EndSetup
"""

EMPTY_SETUP = "%%BeginSetup\n%%EndSetup\n"

PALETTE_SETUP = """%%BeginSetup
%AI5_BeginPalette
0 0 Pb
0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0.000136697562994 0.000626976019703 0.000423457007855 ([Registration]) 0 1 Xz
([Registration])
Pc
PB
%AI5_EndPalette
%%EndSetup
"""


def test_parser_default_attributes():
    """Empty setup attributes."""
    setup = Setup.from_string(EMPTY_SETUP)
    assert setup.palettes == []
    assert setup.plugin_group_infos == []
    assert setup.art_styles == {}


def test_plugin_group_infos():
    """Check the plugin infos."""
    setup = Setup.from_string(SETUP_PGI_1)
    assert len(setup.plugin_group_infos) == 1
    pg = setup.plugin_group_infos[0]
    assert pg.raw_content == "(Adobe Vectorized Object) (Image Tracing) (Vectorize.aip)"


def test_art_styles():
    """ "Check the art styles."""
    setup = Setup.from_string(ARTSTYLE_1)
    assert "[Default]" in setup.art_styles


def test_palettes():
    """Check that all palettes are included."""
    setup = Setup.from_string(PALETTE_SETUP)
    assert len(setup.palettes) == 1


PROCSET_SETUP_AI3 = """%%BeginSetup
Adobe_cmykcolor /initialize get exec
Adobe_cshow /initialize get exec
Adobe_customcolor /initialize get exec
Adobe_IllustratorA_AI3 /initialize get exec
%%EndSetup
"""


def test_procset_exec():
    """Check that the procset is recognized."""
    setup = Setup.from_string(PROCSET_SETUP_AI3)
    assert len(setup.children) == 4
    assert all(isinstance(procset, ProcsetExec) for procset in setup.children)
