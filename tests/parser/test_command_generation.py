# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the shortcut generation of commands."""
from inkai.parser.lazy import generate_eol, AIElement


def test_docs(command):
    """Check the docstring."""
    assert command.__doc__ == "attr name Cmd\n\nexample"


def test_mro(command):
    """Check superclass."""
    assert AIElement in command.mro()


def test_parsing(command):
    """Parse and access attributes."""
    r = command.from_string("123 name Cmd")
    assert r.attr == 123
    assert r.name == "name"


def test_attribute_docs(command):
    """Check that the attributes are nicely documented."""
    assert command.name.__doc__ == "the name argument of Cmd"
    assert command.name.fget.__name__ == "name"
    assert command.attr.fget.__name__ == "attr"
    assert command.attr.fget.__annotations__["return"] == int
    assert command.name.fget.__annotations__["return"] == str


def test_module(command, command_module):
    """Check that the module name is correct."""
    assert command.__module__ == command_module
