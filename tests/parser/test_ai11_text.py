# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the parsing of AI11Texts and their connected information

- document.setup.text_document
- document.objects -> category[AI11Text]

This is reverse-engineered.
"""
import pytest
from inkai.parser.objects import objects
from inkai.parser.objects.ai11_text import AI11Text
from inkai.parser.data_hierarchy import DataHierarchyParser
from inkai.parser.document import AIDocument

SIMPLE_AI_11 = """/AI11Text :
0 /FreeUndo ,
123 /FrameIndex ,
33 /StoryIndex ,
2 /TextAntialiasing ,
;

"""
ART_CONTENT = """X=
1 Ap
8 As
976 -667 m
694 -667 l
n
%_/ArtDictionary :
%_;
%_
X+"""
COMPLEX_TEXT_AI_11 = f"""/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
3 /StoryIndex ,
/Art :
{ART_CONTENT}
; /ConfiningPath ,
2 /TextAntialiasing ,
;

"""


@pytest.mark.parametrize(
    "text,expected_value",
    [
        (
            SIMPLE_AI_11,
            {
                "FreeUndo": 0,
                "FrameIndex": 123,
                "StoryIndex": 33,
                "TextAntialiasing": 2,
                "ConfiningPath": None,
                "_type": "AI11Text",
            },
        ),
        (
            COMPLEX_TEXT_AI_11,
            {
                "FreeUndo": 0,
                "FrameIndex": 0,
                "StoryIndex": 3,
                "ConfiningPath": {"Data": ART_CONTENT, "_type": "Art"},
                "TextAntialiasing": 2,
                "_type": "AI11Text",
            },
        ),
    ],
)
@pytest.mark.parametrize(
    "parser",
    [
        DataHierarchyParser.parse_string,
        lambda text: objects.from_string(text).parsed_content,
    ],
)
def test_parse_string_only_with_data_hierarchy_parser(text, expected_value, parser):
    """Make sure that the DataHierarchyParser understands the text."""
    ai11_text = parser(text)
    print(ai11_text)
    print(expected_value)
    assert ai11_text == expected_value


@pytest.mark.parametrize(
    "document",
    ["simple_v2020", "simple_cs"],
)
def test_parse_document(documents, document):
    """Access the text definitions."""
    document: AIDocument = documents[document]
    text_document = document.setup.text_document
    story = text_document.stories[0]
    assert "Hellow mom\r" == story.text, '["/0"]["/0"]'


@pytest.mark.parametrize(
    "text,story_index", [(SIMPLE_AI_11, 33), (COMPLEX_TEXT_AI_11, 3)]
)
def test_access_parsed_content_of_AI11Text(text, story_index):
    """There are attributes to access the text."""
    assert objects.from_string(text).story_index == story_index
