# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests how to recurse through the element structure."""
from inkai.parser.color import Color
from inkai.parser.document import AIDocument


def test_color_does_not_have_children():
    """Check the children of these elements."""
    assert Color.from_string("0 g").children == []


def test_children_of_document(documents):
    """Check that all the sections are in it."""
    document: AIDocument = documents.rgb_rectangle
    assert document.children[0] == document.header_comments
    assert document.children[1] == document.prolog
    assert document.children[2] == document.setup
    assert document.children[3:-2] == document.objects
    assert document.children[-2] == document.page_trailer
    assert document.children[-1] == document.document_trailer
