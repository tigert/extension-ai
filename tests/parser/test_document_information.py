# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the calculation of meta information of the documents."""
import pytest
from inkai.parser.document import AIDocumentInformation, AIDocument
from inkai.parser.units import to_px, mm, cm
from inkai.parser.objects.layer import Layer
from inkai.parser.objects.new import ArtDictionary
from inkai.parser.objects.live_shapes import Rectangle
from pprint import pprint
from inkai.parser.objects.path import L, m, l

TOP = 0
LEFT = 0


@pytest.mark.parametrize(
    "file,x, y",
    [
        ("inkscape_minimized_ai10", 0, 128),
        ("inkscape_minimized_ai3", 0, 0),  # AI3 should not have a ruler origin
        ("inkscape_minimized_ai7", 0, 128),
        ("inkscape_minimized_ai9", 0, 128),
        ("inkscape_minimized_ai_cc", 0, 128),
        ("inkscape_minimized_ai_cc_current", 0, 128),
        ("inkscape_minimized_ai_cs2", 0, 128),
        ("inkscape_minimized_ai_cs3", 0, 128),
        ("inkscape_minimized_ai_cs4", 0, 128),
        ("inkscape_minimized_ai_cs5", 0, 128),
        ("inkscape_minimized_ai_cs6", 0, 128),
        ("inkscape_minimized_ai_cs", 0, 128),
        ("20000x10000_origin_at_100_0_CC", 100, 0),
        ("20000x10000_origin_at_100_100_CC", 100, 100),
        ("20000x10000_origin_topleft_CC", 0, 0),
        ("20x40mm_origin_topleft_CC", 0, 0),
        ("20x40mm_origin_topleft_CS2", 0, 0),
        ("2x4cm_origin_10.2306_0mm_AI10_v2", round(10.2306 * mm), 0),
        ("2x4cm_origin_10.2306_0mm_CC", round(10.2306 * mm), 0),
        ("2x4cm_origin_topleft_AI10", 0, 0),
        ("2x4cm_origin_topleft_AI3", 0, 0),
        ("2x4cm_origin_topleft_AI8", 0, 0),
        ("2x4cm_origin_topleft_AI9", 0, 0),
        ("2x4cm_origin_topleft_CC", 0, 0),
        ("525x801_origin_at_100_0_CC", 100, 0),
        ("525x801_origin_at_100_0_CS3", 100, 0),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000", 100, 0),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000_CS4", 100, 0),
        ("525x801_origin_at_100_100_AI10", 100, 100),
        ("525x801_origin_at_100_100_AI8", 100, 100),
        ("525x801_origin_at_100_100_AI9", 100, 100),
        ("525x801_origin_at_100_100_CC", 100, 100),
        ("525x801_origin_at_100_100_CC_Legacy", 100, 100),
        ("525x801_origin_at_100_100_CS2", 100, 100),
        ("525x801_origin_at_100_100_CS3", 100, 100),
        ("525x801_origin_at_100_100_CS4", 100, 100),
        ("525x801_origin_at_100_100_CS5", 100, 100),
        ("525x801_origin_at_100_100_CS6", 100, 100),
        ("525x801_origin_at_100_100_CS", 100, 100),
        ("525x801_origin_topleft_CC", 0, 0),
        ("525x801_origin_topleft_CS6", 0, 0),
    ],
)
def test_ruler_origin(documents, file, x, y):
    """Compute the ruler origin.

    > x = (artboard width - templateBox.left - templateBox.right) / 2
    > y = (artboard height + templateBox.top + templateBox.bottom) / 2

    See page 30 AI Spec.
    """
    print(file)
    info: AIDocumentInformation = documents[file].info
    assert info.drawing_ruler_origin == (x, y)


@pytest.mark.parametrize(
    "file, left, bottom, right, top",
    [
        ("inkscape_minimized_ai10", 0, 0, 128, -128),
        # The page size of AI3 documents is incorrectly reported on my AI version
        # ("inkscape_minimized_ai3",           0, 0, 128, -128),
        ("inkscape_minimized_ai7", 0, 0, 128, -128),
        ("inkscape_minimized_ai9", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cc", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cc_current", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cs2", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cs3", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cs4", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cs5", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cs6", 0, 0, 128, -128),
        ("inkscape_minimized_ai_cs", 0, 0, 128, -128),
    ],
)
def test_first_page_position(documents, file, left, bottom, right, top):
    """Check the ruler origin for all files."""
    print(file)
    info: AIDocumentInformation = documents[file].info
    pp = info.drawing_first_page_position
    assert pp == (left, bottom, right, top), f"{pp} == {(left, bottom, right, top)}"
    # assert False


@pytest.mark.parametrize(
    "file, width, height",
    [
        ("20000x10000_origin_at_100_0_CC", 20000, 10000),
        ("20000x10000_origin_at_100_100_CC", 20000, 10000),
        ("20000x10000_origin_topleft_CC", 20000, 10000),
        ("20x40mm_origin_topleft_CC", 20 * mm, 40 * mm),
        ("20x40mm_origin_topleft_CS2", 20 * mm, 40 * mm),
        ("2x4cm_origin_10.2306_0mm_AI10", 2 * cm, 4 * cm),
        ("2x4cm_origin_10.2306_0mm_CC", 2 * cm, 4 * cm),
        ("2x4cm_origin_topleft_AI10", 2 * cm, 4 * cm),
        (
            "2x4cm_origin_topleft_AI3",
            58,
            114,
        ),  # 2 * cm, 4 * cm), # We jusyt test this roughly.
        ("2x4cm_origin_topleft_AI8", 2 * cm, 4 * cm),
        ("2x4cm_origin_topleft_AI9", 2 * cm, 4 * cm),
        ("2x4cm_origin_topleft_CC", 2 * cm, 4 * cm),
        ("525x801_origin_at_100_0_CC", 525, 801),
        ("525x801_origin_at_100_0_CS3", 525, 801),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000", 525, 801),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000_CS4", 525, 801),
        ("525x801_origin_at_100_100_AI10", 525, 801),
        ("525x801_origin_at_100_100_AI8", 525, 801),
        ("525x801_origin_at_100_100_AI9", 525, 801),
        ("525x801_origin_at_100_100_CC", 525, 801),
        ("525x801_origin_at_100_100_CC_Legacy", 525, 801),
        ("525x801_origin_at_100_100_CS2", 525, 801),
        ("525x801_origin_at_100_100_CS3", 525, 801),
        ("525x801_origin_at_100_100_CS4", 525, 801),
        ("525x801_origin_at_100_100_CS5", 525, 801),
        ("525x801_origin_at_100_100_CS6", 525, 801),
        ("525x801_origin_at_100_100_CS", 525, 801),
        ("525x801_origin_topleft_CC", 525, 801),
        ("525x801_origin_topleft_CS6", 525, 801),
    ],
)
def test_first_page_size(documents, file, width, height):
    """Check the ruler origin for all files."""
    print(file)
    info: AIDocumentInformation = documents[file].info
    page = info.drawing_first_page_position
    left, bottom, right, top = page
    assert abs(page.width - width) < 0.001, f"{file}: width {page.width} == {width}"
    assert (
        abs(page.height - height) < 0.001
    ), f"{file}: height {page.height} == {height}"


@pytest.mark.parametrize(
    "file, expected_left, expected_top",
    [
        ("20000x10000_origin_at_100_0_CC", -100, 0),
        ("20000x10000_origin_at_100_100_CC", -100, -100),
        ("20000x10000_origin_topleft_CC", TOP, LEFT),
        ("20x40mm_origin_topleft_CC", TOP, LEFT),
        ("20x40mm_origin_topleft_CS2", TOP, LEFT),
        #        ("2x4cm_origin_10.2306_0mm_AI10", to_px("mm", -10.2306), 0),
        ("2x4cm_origin_10.2306_0mm_AI10_v2", to_px("mm", -10.2306), 0),
        ("2x4cm_origin_10.2306_0mm_CC", to_px("mm", -10.2306), 0),
        ("2x4cm_origin_topleft_AI10", TOP, LEFT),
        ("2x4cm_origin_topleft_AI3", 0, 0),  # AI3 should not have a ruler origin
        ("2x4cm_origin_topleft_AI8", TOP, LEFT),
        ("2x4cm_origin_topleft_AI9", TOP, LEFT),
        ("2x4cm_origin_topleft_CC", TOP, LEFT),
        ("525x801_origin_at_100_0_CC", -100, 0),
        ("525x801_origin_at_100_0_CS3", -100, 0),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000", 1000, 1000),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000_CS4", 1000, 1000),
        ("525x801_origin_at_100_100_AI10", -100, -100),
        ("525x801_origin_at_100_100_AI8", -100, -100),
        ("525x801_origin_at_100_100_AI9", -100, -100),
        ("525x801_origin_at_100_100_CC", -100, -100),
        ("525x801_origin_at_100_100_CC_Legacy", -100, -100),
        ("525x801_origin_at_100_100_CS2", -100, -100),
        ("525x801_origin_at_100_100_CS3", -100, -100),
        ("525x801_origin_at_100_100_CS4", -100, -100),
        ("525x801_origin_at_100_100_CS5", -100, -100),
        ("525x801_origin_at_100_100_CS6", -100, -100),
        ("525x801_origin_at_100_100_CS", -100, -100),
        ("525x801_origin_topleft_CC", TOP, LEFT),
        ("525x801_origin_topleft_CS6", TOP, LEFT),
    ],
)
def test_first_page_corner(documents, file, expected_left, expected_top):
    """Check the ruler origin for all files."""
    print(file)
    info: AIDocumentInformation = documents[file].info
    left, bottom, right, top = info.drawing_first_page_position
    assert abs(left - expected_left) < 0.001, f"{file} left: {left} == {expected_left}"
    assert abs(top - expected_top) < 0.001, f"{file} top: {top} == {expected_top}"


@pytest.mark.parametrize(
    "file,units",
    [
        ("20000x10000_origin_at_100_0_CC", "px"),
        ("20000x10000_origin_at_100_100_CC", "px"),
        ("20000x10000_origin_topleft_CC", "px"),
        ("20x40mm_origin_topleft_CC", "mm"),
        ("20x40mm_origin_topleft_CS2", "mm"),
        ("2x4cm_origin_10.2306_0mm_AI10", "mm"),
        ("2x4cm_origin_10.2306_0mm_AI10_v2", "cm"),
        ("2x4cm_origin_10.2306_0mm_CC", "cm"),
        ("2x4cm_origin_topleft_AI10", "cm"),
        ("2x4cm_origin_topleft_AI3", "px"),  # AI5+ has units
        ("2x4cm_origin_topleft_AI8", "cm"),
        ("2x4cm_origin_topleft_AI9", "cm"),
        ("2x4cm_origin_topleft_CC", "cm"),
        ("525x801_origin_at_100_0_CC", "px"),
        ("525x801_origin_at_100_0_CS3", "px"),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000", "px"),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000_CS4", "px"),
        ("525x801_origin_at_100_100_AI10", "px"),
        ("525x801_origin_at_100_100_AI8", "px"),
        ("525x801_origin_at_100_100_AI9", "px"),
        ("525x801_origin_at_100_100_CC", "px"),
        ("525x801_origin_at_100_100_CC_Legacy", "px"),
        ("525x801_origin_at_100_100_CS2", "px"),
        ("525x801_origin_at_100_100_CS3", "px"),
        ("525x801_origin_at_100_100_CS4", "px"),
        ("525x801_origin_at_100_100_CS5", "px"),
        ("525x801_origin_at_100_100_CS6", "px"),
        ("525x801_origin_at_100_100_CS", "px"),
        ("525x801_origin_topleft_CC", "px"),
        ("525x801_origin_topleft_CS6", "px"),
    ],
)
def test_units_of_the_document(documents, file, units):
    """Check that the units used in the document are correct."""
    print(file)
    info: AIDocumentInformation = documents[file].info
    assert info.units == units


@pytest.mark.parametrize(
    "file, x, y",
    [
        ("cmyk_rectangle", 7895, 8612),
        ("circle", 7893.0, 7770.0),
        ("525x801_origin_at_100_0_first_page_moved_to_1000_1000", 8029, 7791),
        ("525x801_origin_at_100_100_CC", 8029, 7891),
        ("simple_v10", 7231.0, 7651.0),
        ("simple_cs4", 7231.0, 7651.0),
        ("rectangle", 7893.0, 7770.0),
    ],
)
def test_canvas_ruler_origin(documents, file, x, y):
    """Check the canvas ruler origin."""
    print(file)
    info: AIDocumentInformation = documents[file].info
    assert info.canvas_ruler_origin == (x, y)


@pytest.mark.parametrize(
    "rectangle_file",
    [
        "cmyk_rectangle",
        "rectangle_with_rounded_corners",
        "page_moved_to_1000_2000_rect200_300",
    ],
)
def test_location_of_canvas_ruler_origin(documents, rectangle_file):
    """Check that the location of the canvas ruler origin is correct within the document.

    We can use a rectangle to test this.
    """
    print(rectangle_file)
    document: AIDocument = documents[rectangle_file]
    cro = document.info.canvas_first_page_position
    layer: Layer = document.first(Layer)
    rect: Rectangle = layer.categories[ArtDictionary][-1].live_shape
    lines = layer.categories[L] + layer.categories[m] + layer.categories[l]
    page = document.info.drawing_first_page_position
    print(f"first page: {document.info.drawing_first_page_position}")
    print(f"rect: {rect.center}, {rect.width}x{rect.height}")
    x = {l.x for l in lines}
    y = {l.y for l in lines}
    print(f"x={x} y={y}")
    assert {
        rect.center.x - rect.width / 2 - cro.x + page.left,
        rect.center.x + rect.width / 2 - cro.x + page.left,
    } <= x
    print(
        rect.center,
        cro,
        rect.center.y - cro.y,
        document.info.drawing_first_page_position,
    )
    assert {
        round(-page.top - (rect.center.y - rect.height / 2 - cro.y), 5),
        round(-page.top - (rect.center.y + rect.height / 2 - cro.y), 5),
    } <= y


@pytest.mark.parametrize(
    "file",
    [
        "simple_v9",
        "simple_v10",
        "simple_cs5",
        "simple_cs",
    ],
)
def test_first_page_canvas_position_is_the_same_simple(documents, file):
    """All these documents in different versions have the page in the same place."""
    document: AIDocument = documents[file]
    assert document.info.canvas_first_page_position == (7231.0, 7651.0)
    assert (
        document.info.canvas_first_page_position.x - document.info.canvas_ruler_origin.x
        == 0
    )
    assert (
        document.info.canvas_first_page_position.y - document.info.canvas_ruler_origin.y
        == 0
    )
