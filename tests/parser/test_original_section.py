# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test that commands start with %_ and are added to an OriginalSection defining the original path.

"""

EXAMPLE_STRING = """u
0 Ap
3 As
0 O
0.697260975837708 0.147798880934715 0 0 0.16078431372549 0.670588235294118 0.886274509803922 Xa
182 -410 m
395 -416 l
271 -522 l
182 -410 l
U
0 0 Xd
6 () XW
%_3 As
%_182 -410 m
%_395 -416 l
%_271 -522 l
%_182 -410 l
%_n
%_/ArtDictionary : /NotRecorded ,
%_;
%_
1 (BlueWithGradient) XW
"""

from inkai.parser.objects import ObjectSection
from inkai.parser.lazy import AIElement
import pytest
from inkai.parser.objects.path import PathRender


@pytest.mark.parametrize(
    "ai_string,original",
    [
        ("%_0 0 m", True),
        ("0 0 m", False),
    ],
)
def test_parse_as_section(ai_string, original):
    """Test that the values are inside of a section."""
    s = ObjectSection.collect_from_string(ai_string)
    assert len(s.children) == 1
    m: AIElement = s.children[0]
    assert m.is_original == original


def test_example_string():
    """Check that the commands are correctly tagged as original."""
    s = ObjectSection.collect_from_string(EXAMPLE_STRING)
    print(s.children)
    for i in range(3):
        assert not s.children[i].is_original
    for i in range(3, 10):
        assert s.children[i].is_original
    for i in range(10, 11):
        assert not s.children[i].is_original


def test_n_is_recognized():
    """When parsing lines starting with %_, commands that do not have arguments suffer."""
    s = ObjectSection.collect_from_string(EXAMPLE_STRING)
    print(s.children)
    assert isinstance(s.children[8], PathRender)
