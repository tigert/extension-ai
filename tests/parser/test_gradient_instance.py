# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test parsing gradient instances.

This is the usage of gradients.
See AI Spec page 44+
"""

from inkai.parser.objects.gradient_instance import BB, Bg, Xm, Gradient, Bm, Bc, Bh
from inkai.parser.objects.path import PathRender
from inkai.parser.objects import objects
import pytest


@pytest.mark.parametrize(
    "string,stroke,close",
    [
        ("0 BB", False, False),
        ("1 BB", True, False),
        ("2 BB", True, True),
    ],
)
def test_gradient_end(string, stroke, close):
    """A gradient instance must be bracketed with begin (Bb) and end (BB)
    operators. The gradient begin operator Bb does not take arguments.
    See page 45 AI Spec.

    The end has has stroke and fill as attributes.
    """
    bb: BB = BB.from_string(string)
    assert bb.stroke == stroke
    assert bb.close == close


@pytest.mark.parametrize(
    "string,flag,name,xOrigin,yOrigin,angle,length,a,b,c,d,tx,ty",
    [
        (
            "0 (test name) 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 Bg",
            0,
            "test name",
            1.0,
            1.1,
            1.2,
            1.3,
            1.4,
            1.5,
            1.6,
            1.7,
            1.8,
            1.9,
        ),
        (
            "1 (name2) 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 Bg",
            1,
            "name2",
            2.0,
            2.1,
            2.2,
            2.3,
            2.4,
            2.5,
            2.6,
            2.7,
            2.8,
            2.9,
        ),
        (
            "(la la la) 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 Bg",
            0,
            "la la la",
            3.0,
            3.1,
            3.2,
            3.3,
            3.4,
            3.5,
            3.6,
            3.7,
            3.8,
            3.9,
        ),
        (
            "(new flag at the end) 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 1 Bg",
            0,
            "new flag at the end",
            3.0,
            3.1,
            3.2,
            3.3,
            3.4,
            3.5,
            3.6,
            3.7,
            3.8,
            3.9,
        ),
        (
            "2 (new flag at the end) 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 1 Bg",
            2,
            "new flag at the end",
            3.0,
            3.1,
            3.2,
            3.3,
            3.4,
            3.5,
            3.6,
            3.7,
            3.8,
            3.9,
        ),
    ],
)
def test_Bg_parsing(
    string, flag, name, xOrigin, yOrigin, angle, length, a, b, c, d, tx, ty
):
    """Check the parsing of Bg."""
    bg: Bg = Bg.from_string(string)
    assert bg.flag == flag
    assert bg.name == name
    assert bg.xOrigin == xOrigin
    assert bg.yOrigin == yOrigin
    assert bg.angle == angle
    assert bg.length == length
    assert bg.a == a
    assert bg.b == b
    assert bg.c == c
    assert bg.d == d
    assert bg.tx == tx
    assert bg.ty == ty


@pytest.mark.parametrize("values", [list(range(6)), [1.2, 2.2, 3.111, 333.3, 6.06, 7]])
@pytest.mark.parametrize(
    "cls,names",
    [
        (Xm, "a b c d x y"),
        (Bm, "a b c d tx ty"),
        (Bc, "a b c d tx ty"),
        (Bh, "xHilight yHilight angle length"),
    ],
)
def test_Xm_parsing(cls, values, names):
    """Check the parsing of Xm and others."""
    names = names.split()
    values = values[: len(names)]
    string = " ".join(list(map(str, values)) + [cls.__name__])
    element = cls.from_string(string)
    result = [getattr(element, name) for name in names]
    assert result == values


EXAMPLE_FROM_PAGE_49_AI_SPEC = """Bb
1 (Purple, Red & Yellow) 36 696 -30 243 1 0 0 1 0 0 Bg
10431 -6060 -108 -186 -10341 6849 Bc
105 -61 -108 -186 90 789 Bm
105 -61 -108 -186 195 728 Bm
10431 -6060 -108 -186 300 667 Bc
% Close and fill path, then end the instance definition.
f
% Added additionally for testing:
0 0 0 0 0 0 Xm
0 0 0 0 Bh
0 BB
"""


@pytest.mark.parametrize(
    "cls,count",
    [
        (Bg, 1),
        (Bc, 2),
        (Bm, 2),
        (PathRender, 1),
        (Xm, 1),
        (Bh, 1),
        (BB, 1),
    ],
)
def test_all_types_are_included_in_the_gradient(cls, count):
    """We want all to be included in the gradient."""
    g: Gradient = Gradient.from_string(EXAMPLE_FROM_PAGE_49_AI_SPEC)
    assert (
        len(g.categories[cls]) == count
    ), f"{cls.__name__} should be there {count} times"


def test_gradient_is_in_object_section():
    g: Gradient = objects.from_string(EXAMPLE_FROM_PAGE_49_AI_SPEC)
    assert isinstance(g, Gradient)
