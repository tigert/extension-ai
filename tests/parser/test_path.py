# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Path operators, geometry and other path specifications.

See page 54+ AI Spec.
"""
from inkai.parser.objects import objects
from inkai.parser.objects.path import (
    PathRender,
    PathOperator,
    PathAttributes,
    CompoundPath,
    Overprint,
)
import pytest


@pytest.mark.parametrize(
    "string,close,fill,stroke",
    [
        ("b", True, True, True),
        ("f", True, True, False),
        ("s", True, False, True),
        ("N", True, False, False),
        ("B", False, True, True),
        ("F", False, True, False),
        ("S", False, False, True),
        ("n ", False, False, False),
    ],
)
def test_path_render(string, close, fill, stroke):
    """Check attributes of the paths renders."""
    path_render: PathRender = objects.from_string(string)
    assert path_render.close == close
    assert path_render.fill == fill
    assert path_render.stroke == stroke


@pytest.mark.parametrize(
    "s,x,y",
    [
        ("10 20 m", 10, 20),
        ("-0.3 2e+4 m", -0.3, 2e4),
        ("-33333.2 0 l", -33333.2, 0),
        ("0.0000000000 -0 L", 0, 0),
    ],
)
def test_xy(s, x, y):
    """Test all path operators with x, y."""
    path_operator = objects.from_string(s)
    assert path_operator.x == x
    assert path_operator.y == y


@pytest.mark.parametrize(
    "string,xy",
    [
        ("1 2 3 4 5 6 c", [1, 2, 3, 4, 5, 6]),
        ("1.1 2.2 3.3 4.4 5.5 6.6 C", [1.1, 2.2, 3.3, 4.4, 5.5, 6.6]),
        ("3.3 4.4 5.5 6.6 v", [None, None, 3.3, 4.4, 5.5, 6.6]),
        ("-3.3 -4.4 -5.5 -6.6 V", [None, None, -3.3, -4.4, -5.5, -6.6]),
        ("3.3 4.4 5.5 6.6 y", [3.3, 4.4, None, None, 5.5, 6.6]),
        ("-3.3 -4.4 -5.5 -6.6 Y", [-3.3, -4.4, None, None, -5.5, -6.6]),
    ],
)
def test_parse_other_operators(string, xy):
    """Check the location of the other operators."""
    operator: PathOperator = objects.from_string(string)
    assert [
        operator.x1,
        operator.y1,
        operator.x2,
        operator.y2,
        operator.x3,
        operator.y3,
    ] == xy
    assert operator.x is None
    assert operator.y is None


@pytest.mark.parametrize(
    "string, corner",
    [
        (" 1 1 m", False),
        (" 1 1 l", False),
        (" 1 1 L", True),
        ("1 1 1 1 1 1 c", False),
        ("1 1 1 1 1 1 C", True),
        ("1 1 1 1 y", False),
        ("1 1 1 1 Y", True),
        ("1 1 1 1 v", False),
        ("1 1 1 1 V", True),
    ],
)
def test_corner(string, corner):
    """Check if it is smooth or a corner."""
    operator: PathOperator = objects.from_string(string)
    assert operator.is_corner() == corner


all_attrs = "phase line_join line_cap clockwise miter_limit line_width flatness".split()


@pytest.mark.parametrize(
    "string,attr,value",
    [
        ("[]0 d", "phase", 0),
        ("0 j", "line_join", 0),
        ("2 j", "line_join", 2),
        ("33.33 i", "flatness", 33.33),
        ("0 D", "clockwise", True),
        ("1 D", "clockwise", False),
        ("0 J", "line_cap", 0),
        ("2.2 M", "miter_limit", 2.2),
        ("33.01 w", "line_width", 33.01),
    ],
)
def test_path_attributes(string, attr, value):
    """Check the path attributes."""
    path_attribute: PathAttributes = objects.from_string(string)
    assert getattr(path_attribute, attr) == value
    for none_attr in all_attrs:
        assert none_attr == attr or getattr(path_attribute, none_attr) is None


def test_multiline_path_attributes():
    """Path attributes can be in one line."""
    path_attribute: PathAttributes = objects.from_string("0 J 0 j 1 w 10 M []0 d\n")
    assert path_attribute.phase == 0
    assert path_attribute.line_cap == 0
    assert path_attribute.line_join == 0
    assert path_attribute.line_width == 1
    assert path_attribute.miter_limit == 10
    assert path_attribute.flatness is None


def test_compound_path():
    compound_path: CompoundPath = objects.from_string("*u\n10 10 m\n*U")
    assert compound_path.children[0].x == 10
    assert compound_path.children[0].y == 10


@pytest.mark.parametrize(
    "string,flag,fill,stroke",
    [
        ("0 O", False, True, False),
        ("1 O", True, True, False),
        ("0 R", False, False, True),
        ("1 R", True, False, True),
    ],
)
def test_overprint(string, flag, fill, stroke):
    """Check the overprint operator."""
    overprint: Overprint = objects.from_string(string)
    assert overprint.overprint == flag
    assert overprint.fill == fill
    assert overprint.stroke == stroke
