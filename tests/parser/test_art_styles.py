# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This tests the access to the art styles.

Integration test.
"""

import pytest
from inkai.parser.document import AIDocument
from inkai.parser.hierarchy_objects import KnownStyle


@pytest.mark.parametrize(
    "name",
    [
        "Anon 926",
        "Anon",
        "Anon 929",
        "Anon 928",
        "Anon 927",
        "BlueWithGradient",
        "Anon pTgwrio7jgE=",
        "Anon rv0o30BODG4=",
    ],
)
def test_can_access_art_styles_by_name(documents, name):
    """Check that the art style is present."""
    document: AIDocument = documents.styles
    print(document.setup.art_styles)
    art_style = document.setup.art_styles[name]
    print(art_style)
    assert isinstance(art_style, KnownStyle)
    assert art_style.Name == name
