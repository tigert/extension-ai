# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Images as defined in the AI Spec page 68+."""

from inkai.parser.objects.image import XI, XF, Raster, File, Xh, XN, XG, XH
from inkai.parser.objects import objects
from inkai.parser.document import AIDocument
from inkai.parser.objects.layer import Layer
from inkai.parser.data_section import Data

import pytest


@pytest.mark.parametrize(
    "operator,name",
    [
        (XI, "XI"),
        (XF, "XF"),
        (objects, "XI"),
        (objects, "XF"),
    ],
)
def test_XI(operator, name):
    """[ a b c d tx ty ] llx lly urx ury h w bits ImageType AlphaChannelCount reserved bin-ascii ImageMask XI"""
    xi = operator.from_string(
        f"[ 0.2049 0 0.1 0.198 783 164 ] 0.2 0.3 526 620 526 620 1 1 0 0 0 1 {name}"
    )
    assert xi.a == 0.2049
    assert xi.b == 0
    assert xi.c == 0.1
    assert xi.d == 0.198
    assert xi.tx == 783
    assert xi.ty == 164
    assert xi.llx == 0.2
    assert xi.lly == 0.3
    assert xi.urx == 526
    assert xi.ury == 620
    assert xi.h == 526
    assert xi.w == 620
    assert xi.bits == 1
    assert xi.image_type == 1
    assert xi.alpha_channel_count == 0
    assert xi.binary == False
    assert xi.transparent == True
    assert xi.is_internal() == (name == "XI")


def test_image_path():
    """Check the XG operator."""
    xg = objects.from_string("(/home/user/image.png) 0 XG")
    assert xg.path == "/home/user/image.png"
    assert xg.modified == False


def test_image_color_space():
    """Check the XN operator."""
    x = objects.from_string("/DeviceRGB XN")
    assert x.color_space == "/DeviceRGB"


#
# TODO: This is before the BeginData section. It seems that this is a modified XI command that has BeginData inside.
# [ a b c d tx ty       ] llx lly urx  ury h    w   bits ImageType AlphaChannelCount reserved bin-ascii ImageMask
# [ -1 0 0 -1 1508 -772 ] 0   0   1469 764 1469 764 8    3         0                 1        1         0         4 4 0 0
#


def test_Xh():
    """The Xh operator is newly added."""
    x = objects.from_string("[ -1 0 0 -1 1508 -772 ] 1469 764 0 Xh")
    assert x.a == -1
    assert x.b == 0
    assert x.c == 0
    assert x.d == -1
    assert x.tx == 1508
    assert x.ty == -772
    assert x.urx == 1469
    assert x.ury == 764


def test_commands_in_the_raster_file(documents):
    """Check the the commands are recognosed in the raster file."""
    ai: AIDocument = documents.simple_raster
    layer: Layer = ai.first(Layer)
    print(layer.children)
    raster: Raster = layer.first(Raster)
    xg: XG = raster.first(XG)
    assert xg.path == ""
    xn: XN = raster.first(XN)
    assert xn.color_space == "/DeviceRGB"
    xh: Xh = raster.first(Xh)
    assert xh.urx == 1469
    assert xh.ury == 764
    data: Data = raster.first(Data)
    assert len(data.data) == (xh.urx * xh.ury * 3)
    xH: XH = raster.first(XH)
    xi = raster.first(XI)
    assert xi.urx == 1469
    assert xi.ury == 764
