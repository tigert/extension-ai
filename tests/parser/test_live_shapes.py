# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the liveshape info.

We test at least these rectangles in the data/rectangles folder:
    "90-deg-corners",
    "rounded-1-different",
    "rounded-2-different",
    "rounded-all-different",
    "rounded-equal-rotated",
    "rounded-equal",

"""

from inkai.parser.objects.new import ArtDictionary
from inkai.parser.objects.live_shapes import Rectangle
from pprint import pprint
import pytest

RECT = """%_/ArtDictionary :
%_/Dictionary :
%_/Dictionary :
%_211 /Real (ai::Rectangle::Height) ,
%_8093.5 /Real (ai::Rectangle::CenterY) ,
%_0 /Real (ai::Rectangle::Angle) ,
%_180 /Real (ai::Rectangle::Width) ,
%_8188 /Real (ai::Rectangle::CenterX) ,
%_0 /Real (ai::Rectangle::CornerRadius::1) ,
%_3 /Int (ai::Rectangle::InitialQuadrant) ,
%_0 /Real (ai::Rectangle::CornerRadius::0) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::0) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::0) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::1) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::1) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::2) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::2) ,
%_0 /Real (ai::Rectangle::CornerRadius::2) ,
%_1 /Bool (ai::Rectangle::Clockwise) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::3) ,
%_0 /Real (ai::Rectangle::CornerRadius::3) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::3) ,
%_; (ai::LiveShape::Params) ,
%_(ai::Rectangle) /UnicodeString (ai::LiveShape::HandlerName) ,
%_; (ai::LiveShape) ,
%_;
%_
"""


def test_dimensions_of_rect():
    a: ArtDictionary = ArtDictionary.from_string(RECT)
    rect: Rectangle = a.live_shape
    pprint(rect._art)
    assert rect.handler == "ai::Rectangle"
    assert rect.center == (8188, 8093.5)
    assert rect.width == 180
    assert rect.height == 211


@pytest.mark.parametrize(
    "file,corner_index,attr,value",
    [
        ("90-deg-corners", 0, "RoundingType", "Invalid"),
        ("90-deg-corners", 1, "RoundingType", "Invalid"),
        ("90-deg-corners", 2, "RoundingType", "Invalid"),
        ("90-deg-corners", 3, "RoundingType", "Invalid"),
        ("90-deg-corners", 0, "CornerType", "Invalid"),
        ("90-deg-corners", 1, "CornerType", "Invalid"),
        ("90-deg-corners", 2, "CornerType", "Invalid"),
        ("90-deg-corners", 3, "CornerType", "Invalid"),
        ("rounded-equal-rotated", 0, "CornerType", "Normal"),
        ("rounded-equal-rotated", 1, "CornerType", "Normal"),
        ("rounded-equal-rotated", 2, "CornerType", "Normal"),
        ("rounded-equal-rotated", 3, "CornerType", "Normal"),
        ("rounded-equal-rotated", 0, "RoundingType", "Absolute"),
        ("rounded-equal-rotated", 1, "RoundingType", "Absolute"),
        ("rounded-equal-rotated", 2, "RoundingType", "Absolute"),
        ("rounded-equal-rotated", 3, "RoundingType", "Absolute"),
    ],
)
def test_corners_of_rect(rect, file, corner_index, attr, value):
    """The corners of the rectangle should be accessible."""
    r: Rectangle = rect(file)
    assert len(r.corners) == 4
    corner = r.corners[corner_index]
    rect_value = getattr(corner, attr)
    assert (
        rect_value == value
    ), f"{file}.rect.corners[{corner_index}].{attr} == {rect_value} == {value} (expected)"


@pytest.mark.parametrize(
    "file,corner_index,value",
    [
        ("90-deg-corners", 0, 0),
        ("90-deg-corners", 1, 0),
        ("90-deg-corners", 2, 0),
        ("90-deg-corners", 3, 0),
        ("rounded-equal-rotated", 0, 47.4999999999997),
        ("rounded-equal-rotated", 1, 47.4999999999995),
        ("rounded-equal-rotated", 2, 47.4999999999997),
        ("rounded-equal-rotated", 3, 47.4999999999998),
        ("rounded-1-different", 0, 47.5),
        ("rounded-1-different", 1, 47.5),
        ("rounded-1-different", 2, 81),
        ("rounded-1-different", 3, 47.5),
        ("rounded-2-different", 0, 18.5),
        ("rounded-2-different", 1, 47.5),
        ("rounded-2-different", 2, 81),
        ("rounded-2-different", 3, 47.5),
        ("rounded-all-different", 0, 18.5),
        ("rounded-all-different", 1, 98.99982),
        ("rounded-all-different", 2, 81),
        ("rounded-all-different", 3, 47.5),
    ],
)
def test_corner_radius_of_rect(rect, file, corner_index, value):
    """The corners of the rectangle should be accessible."""
    r: Rectangle = rect(file)
    assert len(r.corners) == 4
    cr = r.corners[corner_index].CornerRadius
    assert (
        abs(cr - value) < 0.0001
    ), f"{file}.rect.corners[{corner_index}].CornerRadius == {cr} == {value} (expected)"


@pytest.mark.parametrize(
    "file, equal",
    [
        ("90-deg-corners", True),
        ("rounded-equal-rotated", True),
        ("rounded-all-different", False),
        ("rounded-1-different", False),
        ("rounded-2-different", False),
    ],
)
def test_equal_corners_check(rect, file, equal):
    """Check whether all corners are equal."""
    assert rect(file).has_equal_corners() == equal


@pytest.mark.parametrize(
    "file, angle",
    [
        ("rounded-1-different", 0),
        ("rounded-2-different", 0),
        ("rounded-all-different", 0),
        ("rounded-equal-rotated", 5.38268506183307),
        ("rounded-equal", 0),
    ],
)
def test_angle(rect, file, angle):
    """Check the angle of the rectangles."""
    r: Rectangle = rect(file)
    assert r.angle == angle


def test_corner_position():
    h = 100
    w = 200
    r0 = 0
    r1 = 10
    r2 = 20
    r3 = 30
    r = Rectangle(
        {
            "ai::LiveShape::HandlerName": "ai::Rectangle",
            "ai::LiveShape::Params": {
                "_type": "Dictionary",
                "ai::Rectangle::Angle": 0.0,
                "ai::Rectangle::CenterX": 8038.0,
                "ai::Rectangle::CenterY": 7898.0,
                "ai::Rectangle::Clockwise": True,
                "ai::Rectangle::CornerRadius::0": r2,  # 2
                "ai::Rectangle::CornerRadius::1": r3,  # 3
                "ai::Rectangle::CornerRadius::2": r0,  # 0
                "ai::Rectangle::CornerRadius::3": r1,  # 1
                "ai::Rectangle::CornerType::0": "Normal",
                "ai::Rectangle::CornerType::1": "Normal",
                "ai::Rectangle::CornerType::2": "Normal",
                "ai::Rectangle::CornerType::3": "Normal",
                "ai::Rectangle::Height": h * 2,
                "ai::Rectangle::InitialQuadrant": 3,
                "ai::Rectangle::RoundingType::0": "Absolute",
                "ai::Rectangle::RoundingType::1": "Absolute",
                "ai::Rectangle::RoundingType::2": "Absolute",
                "ai::Rectangle::RoundingType::3": "Absolute",
                "ai::Rectangle::Width": w * 2,
            },
        }
    )
    assert r.corners.top_left.left == (-w, h - r0)  #
    assert r.corners.top_left.right == (-w + r0, h)
    assert r.corners.top_right.left == (w - r1, h)
    assert r.corners.top_right.right == (w, h - r1)  #
    assert r.corners.bottom_right.left == (w, -h + r2)  #
    assert r.corners.bottom_right.right == (w - r2, -h)
    assert r.corners.bottom_left.left == (-w + r3, -h)
    assert r.corners.bottom_left.right == (-w, -h + r3)  #
