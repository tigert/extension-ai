# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the objects from the specificcation."""

from inkai.parser.objects import objects
from inkai.parser.objects.specified_1998 import Group

GROUPS = """u
u
U
u
(1) Ln
U
U
"""


def test_parse_groups():
    """Check that groups can be parsed recursively."""
    group = objects.from_string(GROUPS)
    assert isinstance(group, Group)
    assert len(group.children) == 2
    assert all(isinstance(child, Group) for child in group.children)
