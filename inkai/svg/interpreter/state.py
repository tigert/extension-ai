# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This interpreter holds the AI state of interpretation."""
from inkai.parser.lazy import AIElement
from inkai.svg.interfaces import IModification, IModificationObserver
from ..interfaces import (
    IElementInterpreter,
    IAIState,
    IInterpreter,
    IModificationSubject,
)
from typing import Dict
from .ai_state.stm import AISTM
from .ai_state.adapter import AISTMAdapter
from .category_map import CategoryMap
from .selector import StateSelector, CategorySelector
from .modification_factory import ModificationFactory
from .observer import ObserverProxy


class StatefulInterpreter(IInterpreter, IModificationSubject):
    """This is an interpreter that holds the AI state."""

    def __init__(self) -> None:
        """Create a new element interpreter."""
        super().__init__()
        self._stm = AISTM()
        self._before = AISTMAdapter(self._stm, self._stm.transitions_before)
        self._on_element = CategoryMap()
        self._after = AISTMAdapter(self._stm, self._stm.transitions_after)
        self._observers = ObserverProxy()

    @property
    def before(self) -> IElementInterpreter:
        """Interpretation before the element to make state changes."""
        return self._before

    @property
    def after(self) -> IElementInterpreter:
        """Interpretation after an element to make state changes."""
        return self._after

    @property
    def state(self) -> IAIState:
        """The state of interpretation."""
        return self._stm.state

    def interpret(self, ai_element: AIElement) -> None:
        """Interpret the element and modify the selector with the state."""
        state = self.state
        categories = ai_element.interpreter_categories
        for category in categories:
            self._before.interpret_element(CategorySelector(category), ai_element, self)
        for category in categories:
            self._on_element.interpret_element(
                StateSelector(category, state), ai_element, self
            )
        for category in categories:
            self._after.interpret_element(CategorySelector(category), ai_element, self)

    def add_factory(self, *factories: ModificationFactory):
        """Add a modification factory."""
        for factory in factories:
            self._on_element.add(factory)
            factory.attach(self._observers)

    def attach(self, modification_observer: IModificationObserver) -> None:
        """Attach an observer."""
        self._observers.attach(modification_observer)

    def notify_observers(self, modification: IModification) -> None:
        """Notify all my observers."""
        self._observers.notify_observers(modification)


__all__ = ["StatefulInterpreter"]
