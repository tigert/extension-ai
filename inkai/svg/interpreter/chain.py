# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from inkai.parser.lazy import AIElement
from inkai.svg.interfaces import IElementInterpreter, IInterpreter, ISelector


from typing import List


class ChainElementInterpreter(IElementInterpreter):
    """Chain the calls to the ElementInterpreters."""

    def __init__(self) -> None:
        """Create a dispatch for the chain."""
        super().__init__()
        self._element_interpreters: List[IElementInterpreter] = []

    def add(self, element_interpreter: IElementInterpreter) -> None:
        self._element_interpreters.append(element_interpreter)

    def interpret_element(
        self, selector: ISelector, ai_element: AIElement, interpreter: IInterpreter
    ) -> None:
        """Give interpretation to all the"""
        for element_interpreter in self._element_interpreters:
            element_interpreter.interpret_element(selector, ai_element, interpreter)

    def __repr__(self):
        """repr(self)"""
        return f"<{self.__class__.__name__} for {len(self._element_interpreters)}>"


__all__ = ["ChainElementInterpreter"]
