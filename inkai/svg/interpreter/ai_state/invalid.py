# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This module contains an IAIState that is unusable."""
from ...interfaces import IAIState


class InvalidAIState(IAIState):
    """This is an invalid state that throws runtime exceptions of accessed."""

    def raise_runtime_error(self, name) -> bool:
        """Error!"""
        raise RuntimeError(f"{name} is not valid to use!")

    def is_creating_a_compound_path(self) -> bool:
        """Error!"""
        return self.raise_runtime_error("is_creating_a_compound_path")

    def is_creating_a_path(self) -> bool:
        """Error!"""
        return self.raise_runtime_error("is_creating_a_path")

    def is_creating_a_raster_image(self) -> bool:
        """Error!"""
        return self.raise_runtime_error("is_creating_a_raster_image")


__all__ = ["InvalidAIState"]
