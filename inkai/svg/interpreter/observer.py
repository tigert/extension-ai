# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

""""""
from typing import List

from inkai.svg.interfaces import IModification
from ..interfaces import IModification, IModificationObserver, IModificationSubject
from ..svg_builder import SVGBuilder


class PrintModifications(IModificationObserver):
    """Print out all the modifications made.
    This is for debugging.
    """

    def new_modification(self, modification: IModification):
        """Print the modification."""
        print("modification:", modification)


class ModificationSubject(IModificationSubject):
    """This is a base class that allows to attach ModificationObservers."""

    def __init__(self):
        """Create a new object for observing modifications."""
        self._observers: List[IModificationObserver] = []

    def attach(self, observer: IModificationObserver):
        """Attach an observer."""
        self._observers.append(observer)

    def notify_observers(self, modification: IModification):
        """Send a modification off to the observers."""
        for observer in self._observers:
            observer.new_modification(modification)


class ObserverProxy(ModificationSubject, IModificationObserver):
    """This is a subject that is also an observer.

    This class hands all the modifications onwards when notified.
    """

    def new_modification(self, modification: IModification) -> None:
        """Send all stored modifications onwards."""
        self.notify_observers(modification)


class SVGBuilderAdapter(IModificationObserver):
    """Execute modifications and modify the SVG."""

    def __init__(self, svg_builder: SVGBuilder) -> None:
        """Adapt the builder so it can be used as an observer."""
        super().__init__()
        self._builder = svg_builder

    def new_modification(self, modification: IModification) -> None:
        """Modify the SVG builder."""
        modification.modify(self._builder)


__all__ = ["ModificationSubject", "PrintModifications", "ObserverProxy"]
