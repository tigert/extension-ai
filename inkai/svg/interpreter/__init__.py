# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This is an interpreter for AIElements coming from the parser to create SVGs."""
from .modification_factory import ModificationFactory
from .mapping import on_element
from .state import StatefulInterpreter
from .observer import SVGBuilderAdapter
from .collect import CollectAIElements


__all__ = [
    "ModificationFactory",
    "on_element",
    "StatefulInterpreter",
    "SVGBuilderAdapter",
    "CollectAIElements",
]
