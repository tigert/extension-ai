# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This contains convenience methods for testing."""
from __future__ import annotations
from inkai.svg.modification import Modification, ModificationChain
from inkai.svg.svg_builder import SVGBuilder, SvgDocumentElement
from inkex import BaseElement, Defs
from . import modification_factory
from ..interfaces import IModification


from typing import List, Sequence, Type, Union


class TestInterpretationResult(ModificationChain):
    """A result of interpretations for the tests."""

    def __init__(
        self,
        modifications: List[IModification],
        factory: modification_factory.ModificationFactory,
    ):
        super().__init__(modifications)
        self.factory = factory

    @property
    def svg(self) -> SvgDocumentElement:
        """Create the SVG from the chain of modifications."""
        builder = SVGBuilder()
        self.modify(builder)
        return builder.svg

    @property
    def child(self) -> BaseElement:
        """Return the created last child of the SVG."""
        return self.svg[-1]

    def assert_matches(self, l: List[Union[Type[Modification], Modification]]) -> None:
        """Whether this chain matches"""
        assert len(l) == len(
            self.modifications
        ), f"Expected {len(l)} modifications but got {len(self.modifications)}"
        for t, m in zip(l, self.modifications):
            if isinstance(t, Modification):
                assert m == t
            else:
                assert isinstance(m, t)


__all__ = ["TestInterpretationResult"]
