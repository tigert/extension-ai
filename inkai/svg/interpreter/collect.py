# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This is a simple factory that does not modify anything and only collect AIElements on the way."""
from .modification_factory import ModificationFactory, on_element
from inkai.parser.lazy import AIElement
from typing import List


class CollectAIElements(ModificationFactory):
    """Collect all the ai elements."""

    def __init__(self):
        """Create a new collection."""
        super().__init__()
        self._elements: List[AIElement] = []

    @property
    def ai_elements(self):
        """The AIElements that were traversed during interpretation."""
        return self._elements

    @on_element(AIElement)
    def collect(self, ai_element: AIElement):
        """Collect an AIElement."""
        self._elements.append(ai_element)


__all__ = ["CollectAIElements"]
