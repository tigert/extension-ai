# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create path elements.

This takes care of paths and compound paths.
"""
from inkai.svg.interpreter import ModificationFactory, on_element
from inkai.parser.objects.path import m, CompoundPath
from inkai.svg.modification import AddChild
from inkex import PathElement as SVGPathElement


class PathElement(ModificationFactory):
    """Create path elements in the correct locations:

    - Paths (m)
    - Compound paths (*u)
    """

    @on_element(m)
    def create_path(self, _m: m):
        """Create a normal path."""
        if not self.state.is_creating_a_compound_path():
            self.submit_modification(AddChild(SVGPathElement()))

    @on_element(CompoundPath)
    def create_compound_path(self, c: CompoundPath):
        """Create a compound path."""
        self.submit_modification(AddChild(SVGPathElement()))
        for child in c.children:
            self.interpreter.interpret(child)


__all__ = ["PathElement"]
