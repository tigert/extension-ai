# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Set the node types for Inkscape."""
from inkai.parser.objects.path import PathOperator, PathRender
from inkai.svg.interpreter.modification_factory import ModificationFactory
from inkai.svg.interpreter.mapping import on_element
from inkai.svg.modification import AddChildAttributes


class NodeTypes(ModificationFactory):
    """Collect the sodipodi:nodetypes attribute.
    c = corner node
    s = smooth node
    z = symmetric
    a = auto smooth
    """

    def reset_modification(self):
        """Initialize."""
        self.nodetypes = ""

    @on_element(PathOperator)
    def add_node_type(self, operator: PathOperator):
        """Add the corner/smooth information."""
        self.nodetypes += "c" if operator.is_corner() else "s"

    @on_element(PathRender)
    def close(self, render: PathRender):
        """Close the path if necessary."""
        if render.close:
            self.nodetypes += self.nodetypes[:1]
        self.submit_modification(
            AddChildAttributes({"sodipodi:nodetypes": self.nodetypes})
        )


__all__ = ["NodeTypes"]
