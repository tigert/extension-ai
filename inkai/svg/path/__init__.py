# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create paths."""
from .commands import PathCommands
from .node_types import NodeTypes
from .style import PathStyle
from .element import PathElement
from ..interpreter.modification_factory import CombinedFactory
from inkai.svg.coordinates import CoordinateTransform


class Path(CombinedFactory):
    """Combine all the path conversions together."""

    def __init__(self, transform: CoordinateTransform):
        """Create path commands in a transformed space."""
        self.commands = PathCommands(transform)
        self.node_types = NodeTypes()
        self.style = PathStyle()
        self.element = PathElement()
        super().__init__(self.commands, self.node_types, self.style, self.element)


__all__ = ["PathCommands", "PathStyle", "NodeTypes", "PathElement", "Path"]
