# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Conversion shortcuts and utilities."""

from .svg_builder import SVGBuilder
from ..parser.document import AIDocument
from .group import Group
from .layer import Layer
from .document import Document
from inkex import SvgDocumentElement
from .coordinates import SVGTransform
from typing import Union
from .ai11_text import AIText
from .raster import Raster
from .interpreter import StatefulInterpreter, SVGBuilderAdapter
from .path import Path
from .live_shape import LiveShapes
from .gradient import Gradients


def _ai2state(document: AIDocument) -> SVGBuilder:
    """Convert an AIDocument to an SVG."""
    transform = SVGTransform(document.info)
    interpreter = StatefulInterpreter()
    interpreter.add_factory(
        Group(),
        Layer(),
        Document(),
        Path(transform),
        LiveShapes(transform),
        Gradients(),
        Raster(),
    )
    if document.setup.text_document:
        interpreter.add_factory(AIText(document.setup.text_document, transform))
    svg_builder = SVGBuilder()
    interpreter.attach(SVGBuilderAdapter(svg_builder))
    interpreter.interpret(document)
    return svg_builder


def ai2svg(ai_document: Union[AIDocument, str]) -> SvgDocumentElement:
    """Convert an AIDocument to an SVG."""
    return _ai2state(
        AIDocument.from_string(ai_document)
        if isinstance(ai_document, str)
        else ai_document
    ).svg


__all__ = ["ai2svg", "SvgDocumentElement"]
