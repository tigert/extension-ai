# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parsing the AIDocument."""

from .interpreter import on_element, ModificationFactory
from .modification import SetSVGAttributes
from ..parser.document import AIDocument


class Document(ModificationFactory):
    """Convert the ai document."""

    @on_element(AIDocument)
    def set_svg_metadata(self, document: AIDocument):
        """Set the dimensions and other metadata of the SVG."""
        page = document.info.drawing_first_page_position
        self.submit_modification(
            SetSVGAttributes(
                width=page.width,
                height=page.height,
                viewBox=f"{0} {0} {page.width} {page.height}",
            )
        )

    @on_element(AIDocument)
    def add_objects_to_svg(self, document: AIDocument):
        """Add all the objects to the document."""
        for child in document.objects:
            self.interpreter.interpret(child)


__all__ = ["Document"]
