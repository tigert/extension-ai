# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create SVG texts."""
import math
from typing import Dict
import warnings
from .interpreter.mapping import on_element

from inkex import TextElement, ShapeElement, TextPath, Transform, Path, PathElement
from inkex.paths import Curve, Move

from inkai.warnings import NotWorthImplementingWarning
from .interpreter import ModificationFactory, on_element
from inkai.parser.objects.ai11_text import AI11Text, Xd, XW
from inkai.parser.objects.new import ArtDictionary
from inkai.parser.setup.text import TextDocument
from .coordinates import SVGTransform
from inkai.svg.modification import AddDef, AddChild


class AIText(ModificationFactory):
    """Create text from AI specs."""

    def __init__(self, text_info: TextDocument, transform: SVGTransform):
        """Create a converter of text objects."""
        super().__init__()
        self._text_info = text_info
        self._transform = transform
        self.result: ShapeElement
        # We also need to store all created text elements in case there are
        # multiple frames.
        self.story_results: Dict[int, ShapeElement] = {}

    @on_element(AI11Text)
    def create_svg_text(self, text: AI11Text):
        self.frames = [
            self._text_info.frames[i.Resource]
            for i in self._text_info.stories[text.story_index].View.Frames
        ]
        if text.story_index in self.story_results:
            # This is an additional frame for an already-defined text.
            # We can ignore this.
            return
        else:
            type = self.frames[0].Resource.Data.Type or 0
            self.success = True
            if type == 0:
                self.result = TextElement()
                self.container = self.result
                self.set_position()
            elif type == 1:
                self.result = TextElement()
                self.container = self.result
                self.set_position_framed()
            elif type == 2:
                self.result = TextElement()
                tp = TextPath()
                self.result.append(tp)
                self.container = tp
                self.set_position_textpath()
            self.apply_transform()
        self.create_children(text)
        self.story_results[text.story_index] = self.result
        if self.success:
            self.submit_modification(AddChild(self.result))

    def create_children(self, text: AI11Text):
        """Add the content to the text element.

        <text>Hello<tspan style="font-size: 10pt">World</tspan></text>"""
        self.container.text = self._text_info.stories[text.story_index].text

    def set_position(self):
        """Set the position of the current (simple) text element"""
        pos = self.frames[0].Resource.Position or self.frames[0].Resource.Bezier.Points

        self.result.set("x", pos[0])
        self.result.set("y", pos[1])
        self.apply_transform()

    @staticmethod
    def get_equivalent_path(frame):
        p = Path()
        pos = frame.Resource.Bezier.Points
        p.append(Move(*pos[0:2]))
        for i in range(0, len(pos), 8):
            chunk = pos[i : i + 8]
            p.append(Curve(*chunk[2:]))
        return p

    def apply_transform(self):
        trans = Transform(matrix=self.frames[0].Resource.Data.FrameMatrix)
        origin_trans = Transform(
            translate=(
                -self._transform._canvas_ruler_x - self._transform._ruler_x,
                -self._transform._canvas_ruler_y - self._transform._ruler_y,
            ),
            scale=(1, 1),
        )
        print("in apply_transform", origin_trans @ trans)
        self.result.transform = origin_trans @ trans

    def set_position_framed(self):
        """Set the shape of a shape-inside text"""
        # Construct a PathParser to parse the shape
        sinside = ""
        # Set transform of first text (this will be correct)
        self.apply_transform()
        for frame in self.frames:
            pe = PathElement.new(path=self.get_equivalent_path(frame))
            self.submit_modification(AddDef(pe))
            sinside += pe.get_id(as_url=2)
            #
            current = Transform(matrix=frame.Resource.Data.FrameMatrix)
            # Try to find out if the second text is just translated compared to the first
            diff = (
                -Transform(matrix=self.frames[0].Resource.Data.FrameMatrix)
            ) @ current
            pe.path = pe.path.transform(diff)
            if not diff.is_translate():
                warnings.warn(
                    NotWorthImplementingWarning(
                        "Flowtexts flowing into multiple shapes that have different transforms "
                        "(and the transforms differ by more than a pure translation) are not "
                        "supported by SVG."
                    )
                )

        self.result.style["shape-inside"] = sinside
        # TODO set shape padding,

        # interpreter = SVGInterpreter()
        # interpreter.add_strategy(Path(self._transform))
        # layer = Layer()
        # interpreter.state.append_child(layer)
        # interpreter.state.enter(layer)
        # for child in text.parsed_art.children:
        #    interpreter.interpret(child)
        # Check whether we need to care about LiveShapes here, once implemented.
        # if len(layer) == 0:
        #    errormsg("TextPath with empty ConfiningPath found, ignoring")
        #    self.success = False
        #    return
        # p: ShapeElement = layer[0]
        # self.state.svg.defs.append(p)
        # self.result.style["shape-inside"] = p.get_id(as_url=2)

    def set_position_textpath(self):
        """Set the shape of a text-on-path element"""

        frame = self.frames[0]
        pe = PathElement.new(path=self.get_equivalent_path(frame))
        self.submit_modification(AddDef(pe))
        self.container.set("xlink:href", pe.get_id(as_url=1))
        self.apply_transform()

        for frame in self.frames[1:]:
            path = self.get_equivalent_path(frame)
            #
            current = Transform(matrix=frame.Resource.Data.FrameMatrix)
            # Try to find out if the second text is just translated compared to the first. In this case, we can join the paths.
            diff = (
                -Transform(matrix=self.frames[0].Resource.Data.FrameMatrix)
            ) @ current
            path = path.transform(diff)
            if diff._is_URT() and math.isclose(
                (diff.a * diff.d) - (diff.c * diff.b), 1
            ):
                # This is a uniform scale / translation / rotation and the determinant is 1 -> we can append the path
                # (caveat: paint order might be wrong)
                pe.path += path
                print(pe.path)
            else:
                warnings.warn(
                    NotWorthImplementingWarning(
                        "A TextPath following multiple shapes was detected, and the characters are differently transformed"
                        "on those paths. Components of the TextPath may be missing."
                    )
                )
        # TODO additional settings for textPath: offset, alignment, effect type

    @on_element(XW)
    def use_XW(self, xw: XW):
        pass

    @on_element(ArtDictionary)
    def use_art(self, d: ArtDictionary):
        pass

    # def parse_text_object_reference(self, p: ParseResults):
    #     text = p.text_object_reference[0]
    #     parser = data_hierarchy.DataHierarchyParser()
    #     objectHierarchy = parser.parse_string(text)[0]
    #     print(objectHierarchy)
    #     # TODO these should be ints
    #     story_index = objectHierarchy["FrameIndex"]
    #     story = self.text_document["/1"]["/1"][story_index]
    #     text = story["/0"]["/0"]

    #     element = inkex.TextElement()
    #     element.text = text
    #     return element


__all__ = ["AIText"]
