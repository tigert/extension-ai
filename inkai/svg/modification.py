# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This module contains the implementations for the modification commands."""
from __future__ import annotations
from typing import List, Type, Sequence
from inkai.svg.interfaces import IModification
from inkai.svg.svg_builder import SVGBuilder, SvgDocumentElement
from .svg_builder import SVGBuilder
from inkex import BaseElement
from .interfaces import IModification, IModificationChain


class Modification(IModification):
    """A modification to be made to the SVGBuilder.

    This follows the Command Pattern.
    See https://en.wikipedia.org/wiki/Command_pattern.

    The changes are accumulated and when ready, all done
    to the SVG.
    """

    def __eq__(self, other):
        """Test equality: self == other."""
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __hash__(self):
        """hash(self)"""
        return hash(self.__class__)

    def __repr__(self):
        """str(self)"""
        return f"{self.__class__.__name__}({', '.join(name + '=' + str(value) for name, value in self.__dict__.items())})"


class AddChild(Modification):
    """Add a child element to the parent of the SVGBuilder."""

    def __init__(self, child: BaseElement):
        """Create a modification to add a child."""
        self.child = child

    def modify(self, svg_builder: SVGBuilder):
        """Add the child."""
        svg_builder.append_child(self.child)


class AddDef(Modification):
    """Add a definition to the defs section of the SVG."""

    def __init__(self, definition: BaseElement):
        """Create a modification to add a child."""
        self.definition = definition

    def modify(self, svg_builder: SVGBuilder):
        """Append the definition."""
        svg_builder.svg.defs.append(self.definition)


class AddAndEnterParent(Modification):
    """Add a parent element for the children to follow."""

    def __init__(self, parent: BaseElement):
        """Create a modification to enter a parent."""
        self.parent = parent

    def modify(self, svg_builder: SVGBuilder):
        """Append the definition."""
        svg_builder.enter(self.parent)


class ExitParent(Modification):
    """Exit the parent."""

    def __init__(self, parent: BaseElement):
        """Create a modification to exit the parent."""
        self.parent = parent

    def modify(self, svg_builder: SVGBuilder):
        """Append the definition."""
        svg_builder.exit()


class ModificationChain(IModificationChain):
    """This is a chain of modifications to perform on the SVG file."""

    def __init__(self, modifications: Sequence[IModification] = []):
        """Create a new chain of modifications."""
        self._modifications: List[IModification] = []
        for modification in modifications:
            self.append(modification)

    @property
    def modifications(self) -> List[IModification]:
        """The list of modifications."""
        return self._modifications

    def new_modification(self, modification: IModification):
        """Add a modification."""
        self._modifications.append(modification)


class ChangeAttributes(Modification):
    def __init__(self, attributes: dict = {}, **kw):
        """Set the attributes of the current child."""
        self.attributes = kw
        self.attributes.update(attributes)


class AddChildAttributes(ChangeAttributes):
    """This sets attibutes of the child."""

    def modify(self, svg_builder: SVGBuilder) -> None:
        """Add the attribute to the last child."""
        for key in sorted(self.attributes):
            svg_builder.last_child.set(key, self.attributes[key])


class SetSVGAttributes(ChangeAttributes):
    """Set attributes of the SVG directly."""

    def modify(self, svg_builder: SVGBuilder) -> None:
        """Set the SVG attributes."""
        for key, value in self.attributes.items():
            svg_builder.svg.set(key, value)


class NullModification(Modification):
    """Do not modify the SVGBuilder."""

    def __init__(self) -> None:
        super().__init__()
        self.id = id(self)

    def modify(self, svg_builder: SVGBuilder) -> None:
        """Do nothing."""


class ReplaceLastChild(Modification):
    """Replace the last child with a newer version."""

    def __init__(self, new_child: BaseElement):
        """Replace the last child in the SVG."""
        self.new_child = new_child

    def modify(self, svg_builder: SVGBuilder) -> None:
        """Replace the last child with this one."""
        if svg_builder.has_last_child():
            svg_builder.replace_last_child_with(self.new_child)
        else:
            svg_builder.append_child(self.new_child)


__all__ = [
    "Modification",
    "ModificationChain",
    "AddAndEnterParent",
    "ExitParent",
    "AddChild",
    "AddDef",
    "NullModification",
    "ReplaceLastChild",
]
