# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""A parser that goes from token to token."""
from typing import List, Callable, Optional
import re
from .lines import LineParser, Lines
from .error import UnexpectedToken, UnexpectedStartOfLine

TokenList = List[str]
TOKEN_REGEX = re.compile(r"[^ \n\r()]+|\((?:[^()]|\\.)*(?<!\\)\)")


UntilType = Callable[[str], bool]


def tokenize(string) -> TokenList:
    """Return the tokens."""
    result = TOKEN_REGEX.findall(string)
    return result


class Tokenizer:
    """Turn a line into tokens."""

    @classmethod
    def from_string(cls, string: str):
        """Create a new DataHierarchyParsingProcess from a string."""
        line_parser = LineParser.from_string(string)
        return cls(line_parser)

    tokenize = staticmethod(tokenize)

    def __init__(self, lines: Lines):
        """Create a new tokenizer."""
        self._lines = lines
        self._tokens: List[str] = []
        self._index = 0
        self._lines_start_with = ""
        self._is_first_line = True

    def next_line(self, line_start: Optional[str] = None) -> str:
        """Return the next line.

        line_start - if given, this overrides the expected start of the line.
        """
        if self._has_tokens():
            raise UnexpectedToken(
                f"To read a line, I expect all tokens to be used: {self._tokens} {self.error_position}"
            )
        line = self._lines.read_line()
        if self._is_first_line and line.startswith("%"):
            if line.startswith("%_"):
                self._lines_start_with = "%_"
            else:
                self._lines_start_with = "%"
        self._is_first_line = False
        line_start = self._lines_start_with if line_start is None else line_start
        if not line.startswith(line_start):
            raise UnexpectedStartOfLine(
                f"Line {self.line} should start with {repr(line_start)} but it does not!"
            )

        self._line = line[len(line_start) :]
        # print(repr(line))
        return self._line

    def _has_tokens(self) -> bool:
        """Whether there are tokens to use."""
        return len(self._tokens) != self._index

    @property
    def error_position(self) -> str:
        """Return a message."""
        return f"at token {self.index} in line {self.line}."

    def _ensure_tokens_are_there(self):
        """Make sure we have tokens.

        This also skips % or %_ at the start to make sure we
        can read the tokens.
        """
        while not self._has_tokens():
            line = self.next_line()
            if line.startswith("_"):
                line = line[1:]
            self._tokens = self.tokenize(line)
            self._index = 0

    def next(self) -> str:
        """Return the next token."""
        self._ensure_tokens_are_there()
        token = self._tokens[self.index]
        self._index += 1
        return token

    def peek(self) -> str:
        """Return the next token without advancing the position"""
        self._ensure_tokens_are_there()
        return self._tokens[self.index]

    @property
    def index(self) -> int:
        """How much of the tokens have been processed."""
        return self._index

    @property
    def line(self) -> int:
        """Return the current line index."""
        return self._lines.current_line_number

    def skip(self, expected_token: str):
        """Skip a token."""
        token = self.next()
        if token != expected_token:
            raise UnexpectedToken(
                f"Expected {expected_token} but found {token} {self.error_position}"
            )

    def skip_optional(self, expected_token: str) -> bool:
        """Skips a token if it is found at the current position.
        Returns whether the token was skipped."""
        token = self.peek()
        if token == expected_token:
            self._index += 1
            return True
        return False

    def is_next(self, token: str) -> bool:
        """Whether a token is next."""
        self._ensure_tokens_are_there()
        return self._tokens[self._index] == token

    def collect_lines_until(self, until: UntilType) -> str:
        """Collect all the lines until the condition is met."""
        result = []
        line = self.next_line()
        while not until(line):
            result.append(line)
            line = self.next_line()
        self._tokens = self.tokenize(line)
        self._index = 0
        return "\n".join(result) + "\n"


__all__ = ["Tokenizer", "tokenize", "UntilType"]
