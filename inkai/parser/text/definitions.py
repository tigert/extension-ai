# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This file maps the hierarchical data structure for the TextDocument."""

from .hierarchy import Frame, Story, Def

cai_text_paint = {
    99: Def("StreamTag", "/SimplePaint", {}),
    0: Def(
        "Color",
        "Object",
        {
            1: Def(
                "Values",
                """List of components.
                RGB documents: [1 R G B, values between 0 and 1]
                CMYK documents: [1 C M Y K, values between 0 and 1]""",
                {},
            ),
            2: Def("Type", "int", {}),
        },
    ),
}
character_style_dict = {
    0: Def("Font", "Font [index in the font list]", {}),
    1: Def("FontSize", "Font size [pt]", {}),
    2: Def("FauxBold", "bool", {}),
    3: Def("FauxItalic", "bool", {}),
    4: Def("AutoLeading", "bool", {}),
    5: Def("Leading", "Line spacing [pt]", {}),
    6: Def("HorizontalScale", "Horizontal Scaling [factor]", {}),
    7: Def("VerticalScale", "Vertical Scaling [factor]", {}),
    8: Def("Tracking", "Tracking [percent]", {}),
    9: Def("BaselineShift", "Baseline shift [pt]", {}),
    10: Def("CharacterRotation", "Character rotation [degrees, counterclockwise]", {}),
    11: Def("AutoKern", "2: Optical Kerning 3: Metrics - Roman Kerning", {}),
    12: Def("FontCaps", "1: Small caps, 2: All caps", {}),
    13: Def("FontBaseline", "0: normal, 1: Superscript, 2: Subscript", {}),
    14: Def(
        "FontOTPosition",
        """OpenType Features: Position. 1: Superscript/Superior, 2: Subscript/Inferior,
        3: Numerator, 4: Denominator""",
        {},
    ),
    15: Def("StrikethroughPosition", "0: no strikethrough, 1: Strikethrough", {}),
    16: Def("UnderlinePosition", "0: no underline, 1: Underline", {}),
    17: Def("UnderlineOffset", "float", {}),
    18: Def("Ligatures", "OpenType Features: Standard Ligatures bool", {}),
    19: Def(
        "DiscretionaryLigatures", "OpenType Features: Discretionary Ligures bool", {}
    ),
    20: Def("ContextualLigatures", "OpenType Features: Contextual Alternates bool", {}),
    21: Def("AlternateLigatures", "bool", {}),
    22: Def("OldStyle", "bool", {}),
    23: Def("Fractions", "OpenType Features: Fractions bool", {}),
    24: Def("Ordinals", "OpenType Features: Ordinals bool", {}),
    25: Def("Swash", "OpenType Features: Swash bool", {}),
    26: Def("Titling", "OpenType Features: Titling Alternates bool", {}),
    27: Def("ConnectionForms", "bool", {}),
    28: Def("StylisticAlternates", "OpenType Features: Stylistic Alternates bool", {}),
    29: Def("Ornaments", "bool", {}),
    30: Def(
        "FigureStyle",
        """OpenType Features: Figure. 0: Default Figure, 1: Tabular Lining,
        2: Proportional Old Style, 3: Proportional Lining, 4: Tabular Old Style""",
        {},
    ),
    31: Def("ProportionalMetrics", "bool", {}),
    32: Def("Kana", "bool", {}),
    33: Def("Italics", "bool", {}),
    34: Def("Ruby", "bool", {}),
    35: Def("BaselineDirection", "[*] 1: Standard Vertical Roman Alignment", {}),
    33: Def("Tsume", "float", {}),
    34: Def("StyleRunAlignment", "int", {}),
    38: Def("Language", '"Language" [enum]', {}),
    39: Def("JapaneseAlternateFeature", "int", {}),
    40: Def("EnableWariChu", "bool", {}),
    41: Def("WariChuLineCount", "int", {}),
    42: Def("WariChuLineGap", "int", {}),
    43: Def(
        "WariChuSubLineAmount", "Dict", {0: Def("WariChuSubLineScale", ": float", {})}
    ),
    44: Def("WariChuWidowAmount", "int", {}),
    45: Def("WariChuOrphanAmount", "int", {}),
    46: Def("WariChuJustification", "int", {}),
    47: Def("TCYUpDownAdjustment", "int", {}),
    48: Def("TCYLeftRightAdjustment", "int", {}),
    49: Def("LeftAki", "float", {}),
    50: Def("RightAki", "float", {}),
    51: Def("JiDori", "int", {}),
    52: Def("NoBreak", "bool", {}),
    53: Def(
        "FillColor",
        "Fill Color: Object of type /CAITextPaint",
        cai_text_paint,
    ),
    54: Def(
        "StrokeColor", "Stroke Color: Object of type /CAITextPaint", cai_text_paint
    ),
    55: Def(
        "Blend",
        "Opacity: Object of type /CAITextBlender",
        {
            0: Def(
                "Mode",
                """Blend mode. 1: Multiply, 2: Screen, 3: Overlay, 
                4: Soft Light, 5: Hard Light, 6: Color Dodge, 
                7: Color Burn, 8: Darken, 9: Lighten,
                10: Difference, 11: Exclusion, 12: Hue, 
                13: Saturation, 14: Color, 15: Luminosity""",
                {},
            ),
            1: Def("Opacity", "Opacity [0-1]", {}),
            2: Def("Isolated", "1: Isolate Blending enabled", {}),
            3: Def(
                "Knockout",
                """1: Knockout group enabled, 
                2: Knockout group "half enabled" (minus in checkbox)""",
                {},
            ),
            4: Def("AlphaIsShape", '1: "Opacity Mask and define Knockout shape"', {}),
            99: Def("StreamTag", '"/CAITextBlender"', {}),
        },
    ),
    56: Def("FillFlag", "bool", {}),
    57: Def("StrokeFlag", "bool", {}),
    58: Def("FillFirst", "bool", {}),
    59: Def("FillOverPrint", "[*] Overprint Fill bool", {}),
    60: Def("StrokeOverPrint", "[*] Overprint Stroke bool", {}),
    61: Def("LineCap", "Stroke caps. 0: Butt, 1: Round, 2: Square", {}),
    62: Def(
        "LineJoin", "Stroke linejoin. 0: Miter Join, 1: Round join, 2: Bevel join", {}
    ),
    63: Def(
        "LineWidth", "Stroke width [px]. Default: 1px (usually /54 is not defined)", {}
    ),
    64: Def("MiterLimit", "Stroke miterlimit [factor of stroke width]", {}),
    65: Def("LineDashOffset", "float", {}),
    66: Def("LineDashArray", "Stroke dasharray [Array of px values]", {}),
    67: Def("Type", "EncodingNames]: [Array]", {}),  # TODO
    68: Def("Kashidas", "int", {}),
    69: Def("DirOverride", "int", {}),
    70: Def("DigitSet", "int", {}),
    71: Def("DiacVPos", "int", {}),
    72: Def("DiacXOffset", "float", {}),
    73: Def("DiacYOffset", "float", {}),
    74: Def("OverlapSwash", "bool", {}),
    75: Def("JustificationAlternates", "bool", {}),
    76: Def("StretchedAlternates", "bool", {}),
    77: Def("FillVisibleFlag", "bool, true", {}),
    78: Def("StrokeVisibleFlag", "bool, true", {}),
    79: Def("FillBackgroundColor", "Object of type /CAITextPaint", {}),
    80: Def("FillBackgroundFlag", "bool", {}),
    81: Def("UnderlineStyle", "int", {}),
    82: Def("DashedUnderlineGapLength", "float", {}),
    83: Def("DashedUnderlineDashLength", "float", {}),
    84: Def("SlashedZero", "bool", {}),
    85: Def(
        "StylisticSets",
        """OpenType Features: Stylistic Sets
        [binary mask, 1 = Set 1, 2 = Set 2, 4 = Set 3 etc.]""",
        {},
    ),
    86: Def(
        "CustomFeature",
        "Dict",
        {99: Def("StreamTag", 'e.g. "/SimpleCustomFeature"', {})},
    ),
    87: Def("MarkYDistFromBaseline", "float", {}),
    88: Def("AutoMydfb", "bool", {}),
    89: Def("RefFontSize", "Font size [pt]", {}),
    90: Def("FontSizeRefType", "int", {}),
    92: Def("MagicLineGap", "float", {}),
    93: Def("MagicWordGap", "float", {}),
}

paragraph_style_dict = {
    0: Def(
        "Justification",
        """Justification
        0: align left, 1: align right, 2: align center
        3: justify, last line aligned left
        4: justify, last line aligned right
        5: justify, last line center-aligned
        6: justify all lines""",
        {},
    ),
    1: Def("FirstLineIndent", "First-line left indent [pt]", {}),
    2: Def("StartIndent", "Left indent [pt]", {}),
    3: Def("EndIndent", "Right indent [pt]", {}),
    4: Def("SpaceBefore", "Space before paragraph [pt]", {}),
    5: Def("SpaceAfter", "Space after paragraph [pt]", {}),
    6: Def("DropCaps", "int", {}),
    7: Def("AutoLeading", "Auto Leading float", {}),
    8: Def("LeadingType", "int", {}),
    9: Def("AutoHyphenate", "hypenation enabled bool", {}),
    10: Def("HyphenatedWordSize", "Hypenate words longer than int letters", {}),
    11: Def("PreHyphen", "Hypenate after first int letters", {}),
    12: Def("PostHyphen", "Hypenate before last int letters", {}),
    13: Def("ConsecutiveHyphens", "Hyphen limit: int letters", {}),
    14: Def("Zone", "Hyphenation zone [pt]", {}),
    15: Def("HyphenateCapitalized", "bool", {}),
    16: Def(
        "HyphenationPreference",
        "Float between 0: Better Spacing, 0.5: standard, 1: Fewer Hyphens",
        {},
    ),
    17: Def(
        "WordSpacing",
        "Justification (Word spacing): Listfloat: [Minimum, Desired, Maximum]",
        {},
    ),
    18: Def(
        "LetterSpacing",
        "Justification (Letter spacing): Listfloat: [Minimum, Desired, Maximum]",
        {},
    ),
    19: Def(
        "GlyphSpacing",
        "Justification (Glyph spacing): Listfloat: [Minimum, Desired, Maximum]",
        {},
    ),
    20: Def(
        "SingleWordJustification",
        """Single-word justification: 
        0: align Left, 1: align right, 2: align center, 6: Full justify""",
        {},
    ),
    21: Def("Hanging", "Roman Hanging Punctuation bool", {}),
    22: Def("AutoTCY", "int", {}),
    23: Def("KeepTogether", "bool", {}),
    24: Def("BurasagariType", "int", {}),
    25: Def("KinsokuOrder", "int", {}),
    26: Def("Kinsoku", '"/nil"', {}),
    27: Def("KurikaeshiMojiShori", "bool", {}),
    28: Def("MojiKumiTable", '"/nil"', {}),
    29: Def(
        "EveryLineComposer",
        """Composer:
            true: Adobe Every-Line Composer, false: Adobe Single Line Composer""",
        {},
    ),
    30: Def(
        "TabStops",
        "Tabs",
        {
            0: Def(
                "TabStops",
                "List of tabs",
                {
                    0: Def("TabAdvance", "Position [pt]", {}),
                    1: Def(
                        "TabType",
                        """Type. 
                        0: left-justified tab, 1: Center-justified tab, 
                        2: Right-justified tab, 3: Decimal-justified tab""",
                        {},
                    ),
                    2: Def("TabLeader", "Leader [str]", {}),
                    3: Def(
                        "DecimalCharacter",
                        "for decimal-justified tabs: character to align at [str]",
                        {},
                    ),
                },
            )
        },
    ),
    31: Def("DefaultTabWidth", "float", {}),
    32: Def(
        "DefaultStyle",
        "CharacterStyle, Default character style for this paragraph style.",
        character_style_dict,
    ),
    33: Def("ParagraphDirection", "int", {}),
    34: Def("JustificationMethod", "int", {}),
    35: Def("ComposerEngine", "int", {}),
    36: Def(
        "ListStyle",
        """Bullets/Enumeration [index in the enumeration and itemization type defs],
        no enumeration: "/nil" """,
        {},
    ),
    37: Def("ListTier", "Bullets/Enumeration level [0-based index]", {}),
    38: Def("ListSkip", "bool", {}),
    39: Def("ListOffset", "int", {}),
    40: Def("KashidaWidth", "int", {}),
}
story_def = Def(
    "Model",
    "content",
    {
        0: Def(
            "Text",
            r"""actual text, characters are at least two bytes long (i.e. UTF-16).
            Emojis?
            Paragraphs are created with \r, newlines with \u03.""",
            {},
        ),
        5: Def(
            "ParagraphRun",
            """Dict, contains overrides of the paragraph styles
            If multiple styles are used in a text element, their scope is given in
            "number of affected characters".""",
            {
                0: Def(
                    "RunArray",
                    "List",
                    {
                        0: Def(
                            "RunData",
                            "Dict",
                            {
                                0: Def(
                                    "ParagraphSheet",
                                    "int (index in the default paragraph styles) or dict, in which case:",
                                    {
                                        97: Def("UUID", "", {}),
                                        0: Def("Name", "empty", {}),
                                        5: Def(
                                            "Features",
                                            "ParagraphStyle, Overrides of the paragraph style",
                                            paragraph_style_dict,
                                        ),
                                        6: Def("Parent", "", {}),
                                    },
                                )
                            },
                        ),
                        1: Def("Length", "Number of affected characters", {}),
                    },
                )
            },
        ),
        6: Def(
            "StyleRun",
            """Dict, contains overrides of the character styles.
            If multiple styles are used in a text element, their scope is given in
            "number of affected characters".""",
            {
                0: Def(
                    "RunArray",
                    "List",
                    {
                        0: Def(
                            "RunData",
                            "Dict",
                            {
                                0: Def(
                                    "StyleSheet",
                                    "int (index in the default character styles) or dict, in which case:",
                                    {
                                        97: Def("UUID", "UUID", {}),
                                        0: Def("Name", "[string]", {}),
                                        5: Def("Parent", "int", {}),
                                        6: Def(
                                            "Features",
                                            "CharacterStyle, Overrides of the character style",
                                            character_style_dict,
                                        ),
                                    },
                                )
                            },
                        ),
                        1: Def("Length", "Number of affected characters", {}),
                    },
                )
            },
        ),
        # ? : Def("KernRun", "", {})
        # ? : Def("AlternateGlyphRun", "", {})
        10: Def(
            "StorySheet",
            "Dict (small), probably some settings",
            {
                0: Def("AntiAlias", "", {}),
                2: Def("UseFractionalGlyphWidths", "", {}),
            },
        ),
        # ? : Def("HyperlinkRun", "", {})
    },
)
strike_def = {
    99: Def(
        "StreamTag",
        """string. Stream tags in here:
                    /PC: /PathSelectGroupCharacter
                    /F: /FrameStrike
                    /R: /RowColStrike
                    /L: /LineStrike
                    /S: /Segment
                    /G: /GlyphStrike""",
        {},
    ),
    1: Def("Bounds", "List[float]", {}),
    0: Def("Transform", ": Dict", {0: Def("Origin", "", {})}),
}

strike_def[6] = Def("Children", "List[Strikes]", strike_def)

view_def = Def(
    "View",
    "layout information",
    {
        0: Def(
            "Frames",
            "List of Dicts",
            {
                0: Def(
                    "Resource",
                    """index of Frame. Note that flowtexts can have multiple defined.
                    This appears to be duplicated in the main file ("/FrameIndex")""",
                    {},
                )
            },
        ),
        2: Def("Strikes", "List[Strikes]", strike_def),
    },
)
font_dict = {
    99: Def("StreamTag", "Datatype: /CoolTypeFont", {}),
    97: Def("UUID", "UUID", {}),
    0: Def(
        "Identifier",
        "Dict",
        {
            0: Def(
                "Name",
                """PostScript font name, encoded in UTF-16.
                Also includes variant (e.g. "-Italic")
                as well as the weight for variable font: "_535.000wght". """,
                {},
            ),
            2: Def("Type", "Int, ?", {}),
            4: Def("MMAxis", "Listint ?", {}),
            5: Def("VersionString", "Font version", {}),
        },
    ),
}

listing_style = {
    97: Def("UUID", "UUID", {}),
    0: Def(
        "Name",
        """Name, either predefined ("kPredefinedEmptyCircleBulletListStyleTag")
            or random-custom ("Custom_1680435898608793")""",
        {},
    ),
    5: Def(
        "LevelStyle",
        "List[ListingStyleLevel]",
        {
            0: Def("IndentUnits", "int, always 1 (?)", {}),
            1: Def("TextIndent", "left indent [pt]", {}),
            2: Def("LabelIndent", "first line indent [pt], usually negative", {}),
            3: Def("LabelAlignment", "0", {}),
            5: Def(
                "SequenceGenerator",
                "Object",
                {
                    99: Def(
                        "StreamTag",
                        """Type of sequence generator. Types:
                            /AlphabeticSequenceGenerator, 
                            /ArabicNumberSequenceGenerator, 
                            /BulletSequenceGenerator
                            /RomanNumeralSequenceGenerator
                            """,
                        {},
                    ),
                    0: Def("Prefix", "Character(s) before enumeration symbol", {}),
                    1: Def("Postfix", "Character(s) after enumeration symbol", {}),
                    2: Def("MinDigits", "int", {}),
                    3: Def(
                        "CaseType",
                        """EnumerationChar. Meaning depends on type of sequence generator:
                            For /AlphabeticSequenceGenerator: 
                                Either 64 (upper-case) or 96 (lower case). 
                                This is the character code one before the first enumeration 
                                symbol (A = 65, a = 97).
                            For /RomanNumeralSequenceGenerator:
                                Either 64 (upper-case) or 96 (lower case). 
                            For /BulletSequenceGenerator: indicates type of bullet used, 
                                encountered values:
                                    ' "' - U+2022 center dot
                                    '%\\aa' - filled square
                                    '%\\e6' - empty center circle
                                    '%\\ab' - empty center square
                            For /ArabicNumberSequenceGenerator: 0
                            """,
                        {},
                    ),
                },
            ),
            6: Def("Font", "int - Reference to the font list", {}),
            7: Def("UseOriginalFont", "bool, if true, 6 is not specified", {}),
        },
    ),
    6: Def("PredefinedTag", ": int", {}),
}

frame_dict = {
    97: Def("UUID", "UUID", {}),
    0: Def(
        "Position",
        """Position of the start of the baseline of the first character,
        in canvas coordinates. In CS, the position is only contained in "/Bezier" 
        (coordinate is repeated four times - start position, handle1, handle2, end position).
        Name is guessed.""",
        {},
    ),
    1: Def(
        "Bezier",
        "Dict",
        {0: Def("Points", "List of canvas coordinates, only for text-in-shape", {})},
    ),
    2: Def(
        "Data",
        "Dict",
        {
            0: Def("Type", "2 = Text on path, 1 = Area type, 0 = normal text", {}),
            1: Def(
                "LineOrientation",
                "Orientation. 2: Vertical (top to bottom), also applies to Text on Path",
                {},
            ),
            2: Def(
                "FrameMatrix",
                """Transform [a b c d e f]. Used for translate, rotate and shear.
                Scale transforms are performed by changing the font size 
                (and scale_x / scale_y if uneven scaling is desired).""",
                {},
            ),
            3: Def("RowCount", "(Area type): Number of rows", {}),
            4: Def("ColumnCount", "(Area type): Number of columns", {}),
            5: Def(
                "RowMajorOrder",
                "(Area type): Row/column order: false: Row-major, true: Column-major",
                {},
            ),
            6: Def(
                "TextOnPathTRange",
                """(Text on path): Listfloat, specifies the start and end position on the path.
                A value of 3.2 indicates "at t=0.2 on the bezier segment between Node 3 and Node 4".""",
                {},
            ),
            7: Def("RowGutter", "(Area type): Row gutter", {}),
            8: Def("ColumnGutter", "(Area type): Column gutter", {}),
            9: Def("Spacing", "(Area type): Inset spacing [pt]", {}),
            10: Def(
                "FirstBaseAlignment",
                "(Area type): Settings for the first baseline",
                {
                    0: Def(
                        "Flag",
                        """First baseline: 
                        0: Fixed, Unspecified: Ascent, 2: Cap height, 3: Leading, 
                        4: By x, 5: Em Box Height, 6: Legacy""",
                        {},
                    ),
                    1: Def("Min", "Minimum width of first baseline [pt]", {}),
                },
            ),
            11: Def(
                "PathData",
                "Settings for text on path",
                {
                    0: Def("Flip", "Flipped bool, default: false", {}),
                    1: Def(
                        "Effect",
                        "Effect type: 0: Rainbow (default), 1: Skew, 2: Ribbon, 3: 3D Stairstep, 4: Gravity,",
                        {},
                    ),
                    2: Def(
                        "Alignment",
                        "Align to: Unspecified: Baseline, 0: Ascender, 1: Descender, 2: Center",
                        {},
                    ),
                    4: Def("Spacing", "Spacing [pt] *", {}),
                    18: Def("Spacing", "]: Spacing [pt] *", {}),
                },
            ),
            13: Def(
                "VerticalAlignment",
                "(Area type): Vertical Alignment: Unspecified: Top, 1: Center, 2: Bottom, 3: Justify *",
                {},
            ),
        },
    ),
}


defs = Def(
    "TextDocument",
    "Top level text document",
    {
        0: Def(
            "DocumentResources",
            "Dict, definitions",
            {
                1: Def(
                    "FontSet",
                    "Dict, contains a list of used fonts",
                    {
                        0: Def("Resources", "List[FontDefinition]", font_dict),
                        1: Def(
                            "DisplayList",
                            "List - could be the index of the default font?",
                            {
                                0: Def(
                                    "Resource",
                                    "Index of the font in the FontDefinitions",
                                    {},
                                )
                            },
                        ),
                    },
                ),
                2: Def("MojiKumiCodeToClassSet", "Dict", {}),
                3: Def("MojiKumiTableSet", "Dict", {}),
                4: Def("KinsokuSet", "Dict", {}),
                9: Def(
                    "ListStyleSet",
                    "Dict, enumeration and itemization type definitions",
                    {
                        0: Def(
                            "Resources",
                            "List[ListingStyle]",
                            {0: Def("Resource", "", listing_style)},
                        ),
                        1: Def(
                            "DisplayList",
                            "List, one entry for each default enumeration style",
                            {
                                0: Def(
                                    "Resource",
                                    ": Index of the enumeration style in the /Resources",
                                    {},
                                )
                            },
                        ),
                    },
                ),
                5: Def(
                    "StyleSheetSet",
                    "Dict, Character Styles defined in the document",
                    {
                        0: Def(
                            "Resources",
                            "List",
                            {
                                0: Def(
                                    "Resource",
                                    "Object",
                                    {
                                        97: Def("UUID", "UUID", {}),
                                        0: Def(
                                            "Name",
                                            "Name in the Window -> Type -> Character Style dialog",
                                            {},
                                        ),
                                        5: Def("Parent", "int", {}),
                                        6: Def("Features", "CharacterStyle", {}),
                                    },
                                )
                            },
                        ),
                        1: Def(
                            "DisplayList",
                            "List",
                            {
                                0: Def(
                                    "Resource",
                                    "",
                                    {},
                                )
                            },
                        ),
                    },
                ),
                6: Def(
                    "ParagraphSheetSet",
                    "Dict, Paragraph Styles defined in the document",
                    {
                        0: Def(
                            "Resources",
                            "List",
                            {
                                0: Def(
                                    "Resource",
                                    "Object",
                                    {
                                        97: Def("UUID", "UUID", {}),
                                        0: Def(
                                            "Name",
                                            "Name in the Window -> Type -> Character Style dialog",
                                            {},
                                        ),
                                        5: Def("Features", "CharacterStyle", {}),
                                        6: Def("Parent", "int", {}),
                                    },
                                )
                            },
                        ),
                        1: Def(
                            "DisplayList",
                            "List",
                            {
                                0: Def(
                                    "Resource",
                                    "",
                                    {},
                                )
                            },
                        ),
                    },
                ),
                8: Def(
                    "TextFrameSet",
                    "Dict",
                    {
                        0: Def(
                            "Resources",
                            "List",
                            {0: Def("Resource", "Frame", frame_dict, Frame)},
                        )
                    },
                ),
            },
        ),
        1: Def(
            "DocumentObjects",
            "Dict",
            {
                0: Def(
                    "DocumentSettings",
                    "Dict",
                    {
                        0: Def(
                            "HiddenGlyphFont",
                            "Dict",
                            {
                                0: Def("AlternateGlyphFont", "int", {}),
                                1: Def(
                                    "WhitespaceCharacterMapping",
                                    ": List",
                                    {
                                        0: Def("WhitespaceCharacter", "", {}),
                                        1: Def("AlternateCharacter", "", {}),
                                    },
                                ),
                            },
                        ),
                        1: Def("NormalStyleSheet", ": int", {}),
                        2: Def("NormalParagraphSheet", "int", {}),
                        3: Def("SuperscriptSize", "float", {}),
                        4: Def("SuperscriptPosition", "float", {}),
                        5: Def("SubscriptSize", "float", {}),
                        6: Def("SubscriptPosition", "float", {}),
                        7: Def("SmallCapSize", "float", {}),
                        8: Def("UseSmartQuotes", "bool", {}),
                        9: Def(
                            "SmartQuoteSets",
                            "List",
                            {
                                0: Def("Language", "int", {}),
                                1: Def("OpenDoubleQuote", "str", {}),
                                2: Def("CloseDoubleQuote", "str", {}),
                                3: Def("OpenSingleQuote", "str", {}),
                                4: Def("CloseSingleQuote", "str", {}),
                            },
                        ),
                        11: Def("GreekingSize", ": int", {}),
                        15: Def(
                            "LinguisticSettings",
                            ": Dict",
                            {0: Def("PreferredProvider", ': string ("Hunspell")', {})},
                        ),
                        16: Def("UseSmartLists", ": bool", {}),
                        17: Def("DefaultStoryDir", ": int", {}),
                    },
                ),
                1: Def(
                    "TextObjects", "List[Story]", {0: story_def, 1: view_def}, cls=Story
                ),
                2: Def("OriginalNormalStyleFeatures", "CharacterStyle", {}),
                3: Def("OriginalNormalParagraphFeatures", "ParagraphStyle", {}),
            },
        ),
    },
)
