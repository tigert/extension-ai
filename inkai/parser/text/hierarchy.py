# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This file creates a nice interface to work with the TextDocument data."""
# from .definitions import Def
from __future__ import annotations
from dataclasses import dataclass
from typing import Dict, Any, List


class AttrGetter:
    def __getattr__(self, item):
        raise AttributeError(item)


class TextDocumentObject(AttrGetter):
    """The objects inside of a TextDocument."""

    def __init__(self, definition: Def, data: Dict[str, Any]):
        """"""
        self._definition = definition
        self._data = data
        self._values_set = False
        self._names: List[str] = []

    def _set_attributes(self):
        """Set the attribute values from the definitions."""
        for attr, value in self._data.items():
            assert attr.startswith("/")
            if attr[1:].isnumeric():
                index = int(attr[1:])
                if index not in self._definition.defs:
                    continue  # We don't have a def for this index
                definition = self._definition.defs[index]
            else:
                # For CS documents, the keys are verbose (not numeric).
                # In this case, we only need the defs to find the correct class.
                # This is a reverse dict lookup, but only for a single AI version.
                filtered = filter(
                    lambda x: x.name == attr[1:], self._definition.defs.values()
                )
                try:
                    definition = next(filtered)
                except StopIteration:
                    continue  # We don't have a def for this key
            if len(definition.defs) > 0:
                # Leaf nodes should not get wrapped
                if isinstance(value, dict):
                    value = definition.cls(definition, value)
                elif isinstance(value, list):
                    value = [definition.cls(definition, item) for item in value]
            self.__dict__[definition.name] = value

        self._names = [definition.name for definition in self._definition.defs.values()]
        self._values_set = True

    def __getattr__(self, name):
        """When we miss the attributes, first access."""
        if not self._values_set:
            self._set_attributes()
        if name in self.__dict__:
            return self.__dict__[name]
        if name in self._names:
            return None
        return super().__getattr__(name)

    @property
    def name(self) -> str:
        """The name/type of this object."""
        return self._definition.name

    @property
    def documentation(self) -> str:
        """The documentation of this object."""
        return self._definition.docstring


@dataclass
class Def:
    name: str
    docstring: str
    defs: Dict[int, Def]
    cls: type = TextDocumentObject


class Story(TextDocumentObject):
    """A story in the text document."""

    def __init__(self, definition: Def, data: Dict[str, Any]):
        """Create a new story."""
        super().__init__(definition, data)

    @property
    def text(self) -> str:
        """The text of this story."""
        return self.Model.Text


class Frame(TextDocumentObject):
    """A frame in the text document."""

    def __init__(self, definition: Def, data: Dict[str, Any]):
        """Create a new story."""
        super().__init__(definition, data)


__all__ = ["TextDocumentObject", "Story", "Frame"]
