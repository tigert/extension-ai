# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This contains functions for the units in the AI documents.

See docs/specification_amendments/units.md
"""
from typing import overload, Tuple

AI5 = {
    "2": "pt",
    "6": "px",
    "0": "in",
    "3": "pc",
    "1": "mm",
    "4": "cm",
    "5": "hpgl",
}

AI24 = {
    "10": "ft",
    "7": "ft,in",
    "9": "yd",
    "8": "m",
}


UNITS = list(AI5.values()) + list(AI24.values())


def header_comments_to_units(ai5: str, ai24: str):
    """Return the units for the header comments.

    ai5 = AI5_RulerUnits
    ai24 = AI24_RulerUnitsLarge

    An empty string counts as no value.
    """
    if not ai5:
        if ai24:
            raise ValueError(f"ai5 == {ai5} but should be 2 if ai24 is set.")
        return "px"
    if ai24:
        if ai5 != "2":
            raise ValueError(f"ai5 == {ai5} but should be 2 if ai24 is set.")
        if ai24 not in AI24:
            raise ValueError(f"ai24 == {ai24} but should be one of {', '.join(AI24)}.")
        return AI24[ai24]
    else:
        if ai5 not in AI5:
            raise ValueError(f"ai5 == {ai5} but should be one of {', '.join(AI5)}.")
        return AI5[ai5]


SCALE = {
    "pt": 1,
    "px": 1,
    "in": 72,
    "pc": 12,
    "ft": 12 * 72,
    "ft,in": 12 * 72,
    "yd": 36 * 72,
    "mm": 72 / 25.4,
    "cm": 72 / 2.54,
    "m": 72 / 0.254,
    "hpgl": 72 / 25.4 / 4,
}


def to_px(unit: str, value_in_units: float = 1) -> float:
    """Returns the value in units as pixels."""
    scale = SCALE.get(unit)
    if scale is None:
        raise ValueError(
            f"{unit} is not valid. These are valid units: {', '.join(UNITS)}"
        )
    return value_in_units * scale


globals().update(SCALE)

__all__ = ["header_comments_to_units", "UNITS", "to_px"] + list(SCALE)
