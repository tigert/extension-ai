# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Base classes for lazy evaluation of the AI grammar."""
from __future__ import annotations

from .lines import LineParser, Lines, UntilType
from . import parse
from .util import TypeAlias
from typing import (
    List,
    Dict,
    Optional,
    Union,
    Any,
    Callable,
    Type,
    Sequence,
    overload,
    Tuple,
)
from types import ModuleType
from collections import defaultdict
from io import TextIOBase
from .tokens import tokenize, TokenList
from io import StringIO
import inspect
from .error import EmptyCategoryError

UntilWithString = Union[str, UntilType]


class ParsingSpecificationInterface:
    """Base class for parsing specifications used by the CommandParser."""

    def add_to(self, parser: CommandParser):
        """Add this spec to a parser."""

    @staticmethod
    def is_spec(object: Any) -> bool:
        """Check if an object has the correct interface to be added to a parser as specification."""
        return (
            isinstance(object, ParsingSpecificationInterface)
            or isinstance(object, type)
            and issubclass(object, AIElement)
        )

    def from_string(self, string: str):
        """Create from a string."""
        return self.from_file(StringIO(string))

    def from_file(self, file: TextIOBase):
        """Create from a file-like object."""
        return self.from_lines(LineParser(file))

    def from_lines(self, lines: Lines):
        """Parse this class using a CommandParser."""
        # We create a parser especially for this call because it only contains one element.
        # This should not be used in production and is intended for a test shortcut.
        parser = CommandParser()
        parser.add_spec(self)
        return parser.parse(lines)


class BoundParsingSpecification(ParsingSpecificationInterface):
    """A parsing specification bound to an object that receives the lines collected."""

    def __init__(
        self, spec: UnboundParsingSpecification, AIElement: AIElementFromLines
    ):
        """Bind a parsing specification to a callable that create an AIElement."""
        self.spec = spec
        self.create_element_from = AIElement

    def add_to(self, parser: CommandParser):
        """Add this spec to the parser."""
        self.spec.add_to(parser, self.collect_from_lines)

    def collect_from_lines(self, lines: Lines) -> AIElement:
        """Collect the lines and create an element."""
        collected_lines = self.spec.collect_from_lines(lines)
        return self.create_element_from(collected_lines)

    def __repr__(self):
        """A string representation."""
        return f"<{self.__class__.__qualname__} for {self.create_element_from}>"


class UnboundParsingSpecification:
    """A specification of how to parse."""

    def collect_from_lines(self, lines: Lines) -> Lines:
        """Collect lines according to the specification."""
        return lines

    def add_to(self, parser: CommandParser, AIElement: AIElementFromLines):
        """Add a callable that creates and AIElement to the parser."""

    def of(self, AIElement: AIElementFromLines) -> BoundParsingSpecification:
        """Bind this spec to a callable that creates an AIElement."""
        return BoundParsingSpecification(self, AIElement)

    def __get__(
        self, ai_element: Optional[AIElement], AIElement: AIElementFromLines
    ) -> BoundParsingSpecification:
        """This is a shortcut for binding this spec to a class."""
        return self.of(AIElement)


TokenParserType = Callable[[str], Any]
TokenParserList = Sequence[TokenParserType]
TokenParserMatch = Sequence[Union[str, TokenParserType]]
TokenParserMatchList = Sequence[TokenParserMatch]
TokenParsersType = Union[TokenParserMatchList, TokenParserList]


class AIElement:
    """This is the basic element that is inside of an AI document.

    All subclasses have the same interface as ParsingSpecificationInterface.
    """

    spec = UnboundParsingSpecification()

    def become_child_of(self, section: CollectedSection) -> None:
        """This is called when the child becomes part of a section."""
        section.add_to_category(self.category, self)

    @property
    def category(self) -> Category:
        """Return the the category of this element."""
        return self.__class__

    @property
    def interpreter_categories(self):
        """The categories for interpretation."""
        return list(self.category.mro())[:-1]

    @classmethod
    def add_to(cls, parser: CommandParser):
        """Add the specification for this section of the file to the parser."""
        parser.add_spec(cls.spec)

    @classmethod
    def collect_from_string(cls, string: str):
        """Collect this section from a string."""
        return cls.collect_from_file(StringIO(string))

    @classmethod
    def collect_from_file(cls, file: TextIOBase):
        """Collect this section from a file-like object."""
        return cls.collect_from_lines(LineParser(file))

    @classmethod
    def collect_from_lines(cls, lines: Lines):
        """Collect this section from the stream."""
        return cls.spec.collect_from_lines(lines)

    @classmethod
    def from_string(cls, string: str):
        """Create from a string."""
        return cls.from_file(StringIO(string))

    @classmethod
    def from_file(cls, file: TextIOBase):
        """Create from a file-like object."""
        return cls.from_lines(LineParser(file))

    @classmethod
    def from_lines(cls, lines: Lines):
        """Parse this class using a CommandParser."""
        # We create a parser especially for this call because it only contains one element.
        # This should not be used in production and is intended for a test shortcut.
        parser = (
            cls.OuterParserClass()
            if cls.OuterParserClass is not None
            else CommandParser()
        )
        parser.add_spec(cls)
        return parser.parse(lines)

    OuterParserClass: Optional[Type[CommandParser]] = None
    example_string: Optional[str] = None

    @classmethod
    def example(cls) -> AIElement:
        """Create an example object."""
        if cls.example_string is None:
            raise ValueError(f"There is no example for {cls.__name__}!")
        return cls.from_string(cls.example_string)

    def __init__(self, lines: Lines):
        """Create a new element with the line."""
        self.lines = lines
        self._first_line = lines.current_line
        self._line_number = lines.current_line_number
        self._tokens: Optional[List[Any]] = None

    def __repr__(self) -> str:
        """repr(self)"""
        return f"{self.__class__.__name__}({repr(self.first_line)})@line{self.first_line_number}"

    token_parsers: TokenParsersType = []

    def parse_tokens(self) -> List[Any]:
        """The token parser for the first line.

        If self.token_parsers is a list of callables these will be applied.
        If self.token_parsers is a list of lists, all the stings in it must match and the matching one is applied.
        If in the last case, nothing matches length and tokens, the tokens are empty.
        """
        token_parsers: TokenParserMatch
        tokens = self.token_strings
        result = []
        if not self.token_parsers:
            return []
        if callable(self.token_parsers[0]):
            # we only have one token_parser to choose from
            token_parsers = self.token_parsers  # type: ignore
        else:
            # find the matching one
            priority = -1
            token_parsers = []
            matches: TokenParserMatchList = self.token_parsers  # type: ignore
            for _token_parsers in matches:
                if not all(
                    token == token_parser or not isinstance(token_parser, str)
                    for token_parser, token in zip(_token_parsers, tokens)
                ):
                    continue
                _priority = int(len(tokens) == len(_token_parsers))
                if _priority > priority:
                    priority = _priority
                    token_parsers = _token_parsers
        try:
            for token_parser, token in zip(token_parsers, tokens):
                result.append(token_parser(token) if callable(token_parser) else token)
        except ValueError as e:
            raise ValueError(
                f"{e.args[0]} in line {self.first_line_number}", *e.args[1:]
            )
        return result

    @property
    def token_strings(self) -> TokenList:
        """The first line split into tokens.

        This is the base of the parsed tokens.
        """
        return tokenize(self.first_line)

    @property
    def first_line(self) -> str:
        """The first line of this elememnt.

        This is usually the line that identifies the element.
        """
        return (
            self._first_line[2:]
            if self._first_line.startswith("%_")
            else self._first_line
        )

    @property
    def is_original(self):
        """Whether this line defines the original shape.

        i.e. the line starts with %_, but doesn't end up in the ArtDictionary parser
        (where it starts with %_/...)
        The line contains information that replaces the path commands or group or liveshape before it.
        """
        return self._first_line.startswith("%_")

    @property
    def first_line_number(self) -> int:
        """The number of first line of this elememnt."""
        return self._line_number + 1

    @property
    def tokens(self) -> List[Any]:
        """The parsed tokens in the first line.

        This is the cached result of parse_tokens_in_first_line().
        """
        if self._tokens is None:
            self._tokens = self.parse_tokens()
        return self._tokens

    @property
    def children(self) -> List[AIElement]:
        """The child elements of this.

        You should be able to recursively iterate over
        the whole AI document structure using this.
        If AIElements are not reachable though this, it is a bug.
        """
        return []


AIElementFromLines = Callable[[Lines], AIElement]
SpecType = Union[ParsingSpecificationInterface, Type[AIElement]]
Category = Type[AIElement]


class UnidentifiedLine(AIElement):
    """This is a line in the AI file that has not been identified."""

    def parse_tokens(self) -> List[Any]:
        """Unidentified lines do not have any tokens."""
        return []


class IgnoredLine(AIElement):
    """This is a line that we chose to ignore."""


SectionCollector = Callable[[AIElement], Optional[List[Any]]]
ParseResultType = Union[List[AIElement], Dict[Any, Any]]


class RAISE_LOOKUP_ERROR:
    pass


class CollectedSection(AIElement):
    """These lines contain the raw string data to process it into an object."""

    def __init__(self, lines: Lines):
        """Create a new collected section with a parser."""
        super().__init__(lines)
        self._has_result = False
        self._result: ParseResultType = []
        self._categories: Dict[Category, List[AIElement]] = defaultdict(list)

    @property
    def parsed_content(self) -> ParseResultType:
        """Return the parsed object."""
        if not self._has_result:
            self._has_result = True
            self._result = self._parse(self.get_parser())
        return self._result

    def _parse(self, parser: CommandParser) -> ParseResultType:
        """Consume all the data and return a parsed object.

        This is internal. Use get().
        """
        result = []
        while not self.lines.reached_end():
            child = parser.parse(self.lines)
            child.become_child_of(self)
            result.append(child)
        return result

    parser: CommandParser  # The parser for this class

    @classmethod
    def get_parser(cls) -> CommandParser:
        """Return the parser for all instances of this section."""
        if not "parser" in cls.__dict__:
            cls.parser = cls.create_parser()
        return cls.parser

    @classmethod
    def create_parser(cls) -> CommandParser:
        """Create a new parser for this section."""
        parser = (
            cls.InnerParserClass()
            if cls.InnerParserClass is not None
            else CommandParser()
        )
        cls.add_to_inner(parser)
        return parser

    InnerParserClass: Optional[
        Type[CommandParser]
    ] = None  # The parser class for the children

    @classmethod
    def add_to_inner(self, parser: CommandParser) -> None:
        """Add the specification for the inside of this section to the parser."""

    def add_to_category(self, category: Category, element: AIElement):
        """Add an element to a specific category."""
        self._categories[category].append(element)

    @property
    def categories(self) -> Dict[Category, List[AIElement]]:
        """The elements by category."""
        self.parsed_content
        return self._categories

    @overload
    def first(self, category: Category) -> AIElement:
        pass

    @overload
    def first(self, category: Category, default: None) -> Optional[AIElement]:
        pass

    @overload
    def first(self, category: Category, default: Any = RAISE_LOOKUP_ERROR()) -> Any:
        pass

    def first(self, category: Category, default: Any = RAISE_LOOKUP_ERROR()) -> Any:
        """Return the first element of a category.

        If no element is present, a LookupError is raised.
        You can set a default value.
        """
        elements = self.categories[category]
        if not elements:
            if isinstance(default, RAISE_LOOKUP_ERROR):
                raise EmptyCategoryError(
                    f"This {self.__class__.__name__} has no child in the {getattr(category, '__name__', category)} category."
                )
            return default
        return elements[0]

    @property
    def children(self) -> List[AIElement]:
        """The child elements of this.

        You should be able to recursively iterate over
        the whole AI document structure using this.
        If AIElements are not reachable though this, it is a bug.
        """
        return self.parsed_content if isinstance(self.parsed_content, list) else []

    def __getitem__(self, name_or_index):
        """Return the parsed content at the name or index.

        This raises a LookupError if impossible.
        Check with `in` first!
        """
        return self.parsed_content[name_or_index]

    def __contains__(self, name_or_index):
        """Whether a name or index is in the parsed content."""
        return name_or_index in self.parsed_content


class CommandParser(ParsingSpecificationInterface):
    """Create a set of parsers."""

    @classmethod
    def from_specs(cls, *specs: SpecType):
        """Create a new parser from a list of specs."""
        parser = cls()
        for spec in specs:
            parser.add_spec(spec)
        return parser

    def __init__(self):
        """Create a new CommandParser to fill with specifications."""
        self._start_of_line: Dict[str, AIElementFromLines] = {}
        self._end_of_line: Dict[str, AIElementFromLines] = {}

    def add_spec(self, *specs: SpecType):
        """Add parsing specifications to the parser."""
        for spec in specs:
            spec.add_to(self)

    def add_all_from_module(self, module: ModuleType):
        """Add all parsing specifications from a module to the parser.

        This only uses those mentioned in module.__all__.
        """
        for name in module.__all__:
            spec = getattr(module, name)
            if self.is_spec(spec):
                self.add_spec(spec)

    def add_line_start(self, id: str, AIElement: AIElementFromLines):
        """Identify a command by id."""
        self._start_of_line[id] = AIElement

    def add_line_end(self, id: str, AIElement: AIElementFromLines):
        """Identify a command by id."""
        self._end_of_line[id] = AIElement

    def ignore_line_start(self, *ids: str):
        """Ignore a command by id."""
        for id in ids:
            self.add_line_start(id, self.ignore)

    def ignore_line_end(self, *ids: str):
        """Ignore a command by id."""
        for id in ids:
            self.add_line_end(id, self.ignore)

    def add_to(self, other: CommandParser):
        """Add this parser to another parser."""
        for id, callable in self._start_of_line.items():
            other.add_line_start(id, callable)
        for id, callable in self._end_of_line.items():
            other.add_line_end(id, callable)

    def parse(self, lines: Lines) -> AIElement:
        """Read a line and decide who gets it."""
        line = lines.read_line()
        if not line:
            return EmptyLine(lines)
        if line.startswith("%_"):
            line = line[2:]
        method = self._start_of_line.get(self.get_start_of_line(line))
        if not method:
            method = self._end_of_line.get(
                self.get_end_of_line(line), self.parse_default
            )
        assert method is not None
        return method(lines)

    @staticmethod
    def get_start_of_line(line: str) -> str:
        """Return the start of the line to identify the command to be parsed."""
        return line.split(" ", 1)[0]

    @staticmethod
    def get_end_of_line(line: str) -> str:
        """Return the end of the line to identify the command to be used."""
        return line.rsplit(" ", 1)[-1]

    def parse_default(self, lines: Lines):
        """Default parse action if no command was identified."""
        return UnidentifiedLine(lines)

    def ignore(self, lines: Lines):
        """This can be used to ignore certain lines."""
        return IgnoredLine(lines)

    def from_string(self, string: str):
        """Parse a string."""
        return self.from_file(StringIO(string))

    def from_file(self, file: TextIOBase):
        """Parse a file-like object."""
        return self.from_lines(LineParser(file))

    def from_lines(self, lines: Lines):
        """Parse lines."""
        return self.parse(lines)


class SectionWithEnd(UnboundParsingSpecification):
    """This is a section in the file which ends at a certain line.

    Example:

        ...
        until
    """

    def __init__(
        self,
        until: UntilWithString,
        start_at_current_line=False,
        include_last_tested_line=False,
        continue_after_last_tested_line=True,
    ):
        """Create a section that has an end."""
        self.until = until if callable(until) else lambda line: line == until
        self.start_at_current_line = start_at_current_line
        self.include_last_tested_line = include_last_tested_line
        self.continue_after_last_tested_line = continue_after_last_tested_line

    def is_start_of_section(self, line: str) -> bool:
        """Check whether this is the start of the section to enable recursion."""
        return False

    def collect_from_lines(self, lines: Lines) -> Lines:
        """Collect this section from a parser."""
        start = lines.current_line_number
        depth = 1  # we are already inside

        def until(line: str):
            nonlocal depth
            if lines.current_line_number == start:
                return False
            if self.is_start_of_section(line):
                depth += 1
            if self.until(line):
                depth -= 1
            return depth == 0

        collected = lines.collect_until(
            until,
            start_at_current_line=self.start_at_current_line,
            include_last_tested_line=self.include_last_tested_line,
            continue_after_last_tested_line=self.continue_after_last_tested_line,
        )
        return collected


class StartOfLineSection(SectionWithEnd):
    """Section with a start and an end.

    Example:

        start...
        ...
        until
    """

    def __init__(
        self,
        start: str,
        until: UntilWithString,
        start_at_current_line=False,
        include_last_tested_line=False,
        continue_after_last_tested_line=True,
    ):
        """Create a section that and has an end."""
        super().__init__(
            until=until,
            start_at_current_line=start_at_current_line,
            include_last_tested_line=include_last_tested_line,
            continue_after_last_tested_line=continue_after_last_tested_line,
        )
        self.start = start

    def add_to(self, parser: CommandParser, AIElement: AIElementFromLines):
        """Add the AIElement to the parser according to this specification."""
        parser.add_line_start(self.start, AIElement)

    def is_start_of_section(self, line: str) -> bool:
        """Whether this line is the start of this section."""
        return self.start == CommandParser.get_start_of_line(line)


class EndOfLineSection(SectionWithEnd):
    """Section with a start and an end.

    Example:

        ...end
        ...
        until
    """

    def __init__(
        self,
        end: str,
        until: UntilWithString,
        start_at_current_line=False,
        include_last_tested_line=False,
        continue_after_last_tested_line=True,
    ):
        """Create a section that starts and ends with a line."""
        super().__init__(
            until=until,
            start_at_current_line=start_at_current_line,
            include_last_tested_line=include_last_tested_line,
            continue_after_last_tested_line=continue_after_last_tested_line,
        )
        self.end = end

    def add_to(self, parser: CommandParser, AIElement: AIElementFromLines):
        """Add the AIElement to the parser according to this specification."""
        parser.add_line_end(self.end, AIElement)

    def is_start_of_section(self, line: str) -> bool:
        """Whether this line is the start of this section."""
        return self.end == CommandParser.get_end_of_line(line)


class StartOfLine(UnboundParsingSpecification):
    """A command at the start of a line.

    Example:

        start...
    """

    def __init__(self, *start: str):
        """Parse a line that starts with a command."""
        self.start = start

    def add_to(self, parser: CommandParser, AIElement: AIElementFromLines):
        """Add this specification to the parser."""
        for start in self.start:
            parser.add_line_start(start, AIElement)

    def until(self, line: str) -> bool:
        """If this line matches."""
        line = line.lstrip()
        return line and line.split(maxsplit=1)[0] in self.start  # type: ignore


class EndOfLine(UnboundParsingSpecification):
    """A command at the end of a line.

    Example:

        ...end
    """

    def __init__(self, *end: str):
        """Parse a line that ends with a command."""
        self.end = end

    def add_to(self, parser: CommandParser, AIElement: AIElementFromLines):
        """Add this specification to the parser."""
        for end in self.end:
            parser.add_line_end(end, AIElement)

    def until(self, line: str) -> bool:
        """If this line matches."""
        line = line.rstrip()
        return line and line.rsplit(maxsplit=1)[-1] in self.end  # type: ignore


class SectionIdentifiedByFirstEnd(ParsingSpecificationInterface):
    """This is a section which is identified by its end.

    Example:

        START = SectionIdentifiedByFirstEnd(StartOfLine("START"))

        @START.until("S1")
        class S1(CollectedSection):
            # START
            # ...
            # S1

        @START.until(EndOfLine.until("S2"))
        class S2(CollectedSection):
            # START
            # ...
            # argument S2

        command_parser.add_spec(START)
    """

    def __init__(self, start: UnboundParsingSpecification, start_at_current_line=False):
        """Create a new section whose result is determined by its last line."""
        self.start = start
        self.ends: List[Tuple[UntilType, AIElementFromLines]] = []
        self.start_at_current_line = start_at_current_line

    def until(
        self, until: UntilWithString
    ) -> Callable[[Type[AIElement]], Type[AIElement]]:
        """Decorator for classes.

        This also sets the spec of the decorated class.
        """

        def until_decorator(cls: Type[AIElement]) -> Type[AIElement]:
            """Decorate a class."""
            self.add(cls, until)
            return cls

        return until_decorator

    def add(self, element: AIElementFromLines, end_of_the_section: UntilWithString):
        """Create the element for this end_of_the_section."""
        until = (
            (lambda line: line == end_of_the_section)
            if isinstance(end_of_the_section, str)
            else end_of_the_section
        )
        self.ends.append((until, element))

    def add_to(self, parser: CommandParser):
        """Add this specification to the parser."""
        self.start.add_to(parser, self.collect_lines_and_create_element)

    def collect_lines_and_create_element(self, lines: Lines) -> AIElement:
        """Collect the lines and create the AIElement."""
        create_element: AIElementFromLines

        def until(line: str) -> bool:
            """Check if one of the ends fits."""
            nonlocal create_element
            for until, create_element in self.ends:
                if until(line):
                    return True
            return False

        collected_lines = lines.collect_until(
            until,
            include_last_tested_line=True,
            start_at_current_line=self.start_at_current_line,
        )
        return create_element(collected_lines)


def generate_eol(
    spec: str, doc: str, parsers=TokenParserList, base=AIElement
) -> Type[AIElement]:
    """Generate a command that is placed at the end of a line.

    - spec is the specification.
      Example: "name Ln"
    - doc is the docstring.
    - parsers is a list of parsers e.g. from .parse
    - base is the base class to use.

    Example for a one-line new command:

        Ln : TypeAlias = generate_eol("name Ln", "See page 71 AI Spec.", [parse.ps])  # type: ignore

    Example for a more complex command:

        class Command(generate_eol("name Ln", "See page 71 AI Spec.", [parse.ps]):
            "An example command."

            def method(self):
                return self.name.upper()

    """
    split_spec = spec.strip().split()
    command = split_spec[-1]
    names = split_spec[:-1]
    parsers = parsers[:]
    for i, name in enumerate(names):
        if name in ("[", "]"):
            parsers.insert(i, parse.ignore)
    inner = {
        "spec": EndOfLine(command),
        "__doc__": f"{spec}\n\n{doc}",
        "token_parsers": parsers,
        "command": command,
    }
    frame = inspect.currentframe()
    if frame is not None:
        module = inspect.getmodule(frame.f_back)
        if module is not None:
            inner["__module__"] = module.__name__
    assert len(set(names)) == len(names), "There must not be a duplicate name."
    assert len(names) == len(
        parsers
    ), f"The command should have the same number of parsers as it has named arguments in the spec: {len(names)} names != {len(parsers)} parsers"
    example_string: Optional[str] = ""
    for name, parser in zip(names, parsers):
        if parser == parse.ignore:
            example_string += name  # type: ignore
        else:
            example_value = parse.examples.get(parser)
            if example_value is None:
                example_string = None
                break
            example_string += " " + repr(example_value)  # type: ignore
    if example_string is not None:
        example_string += " " + command
        inner["__doc__"] += "\n\nExample:\n\n    " + example_string
        inner["example_string"] = example_string
    for index, name, parser in zip(range(len(names)), names, parsers):

        def get_from_tokens(self, parser=parser, token_index=index):
            return self.tokens[token_index]

        get_from_tokens.__doc__ = f"the {name} argument of {command}"
        get_from_tokens.__name__ = name
        get_from_tokens.__annotations__["return"] = getattr(
            parser, "__annotations__", {}
        ).get("return", parser)
        inner[name] = property(get_from_tokens)
        assert not hasattr(
            AIElement, name
        ), f"AIElement.{name} exists. Please rename the argument."
    return type(command, (base,), inner)


class EmptyLine(AIElement):
    """This is just a line with no content."""


__all__ = [
    "EndOfLineSection",
    "StartOfLineSection",
    "UnboundParsingSpecification",
    "CommandParser",
    "IgnoreCollectedSection",
    "CollectedSection",
    "SectionWithEnd",
    "AIElement",
    "OneLineCommand",
    "IgnoredLine",
    "UnidentifiedLine",
    "StartOfLine",
    "EndOfLine",
    "ParsingSpecificationInterface",
    "BoundParsingSpecification",
    "TypeAlias",
    "SectionIdentifiedByFirstEnd",
]
