# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Common parser functions."""

from .post_script import unescape as ps

_bool = bool
_float = float
_int = int


def bool(flag: str) -> _bool:
    """Parse a flag which is "0" or "1"."""
    return flag != "0"


def float(string: str) -> _float:
    """Parse a float."""
    return _float(string)


def int(string: str) -> _int:
    """Parse an integer."""
    return _int(string)


def string(string: str) -> str:
    """Return the string that was passed to this."""
    return string


def ignore(string: str) -> None:
    """Ignore this value."""


examples = {int: 10, bool: 1, float: 12.3, string: "string", ps: "(post script text)"}


__all__ = ["ps", "bool", "int", "float", "string", "ignore", "examples"]
