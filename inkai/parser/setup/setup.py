# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the AI file's Setup Section."""
from inkai.parser.art_styles import ArtStyle
from inkai.parser.hierarchy_objects import Style
from inkai.parser.lazy import CollectedSection, CommandParser, StartOfLineSection
from .document_data import DocumentData
from .plugin_group_info import PluginGroupInfo
from .procset_exec import ProcsetExec
from .palette import Palette
from .text import TextDocument
from .gradient import Bn, Gradient


from typing import Dict, List, Optional


class Setup(CollectedSection):
    """The setup section.
    Example:
        %%BeginSetup
        ...
        %%EndSetup
    """

    spec = StartOfLineSection("%%BeginSetup", "%%EndSetup")

    @staticmethod
    def add_to_inner(parser: CommandParser):
        parser.ignore_line_start(
            "%AI5_Begin_NonPrinting",
            "%AI5_End_NonPrinting--",
            "Np",
        )
        parser.add_spec(
            Bn,
            PluginGroupInfo,
            ArtStyle,
            Palette,
            DocumentData,
            TextDocument,
            ProcsetExec,
            Gradient,
        )

    @property
    def art_styles(self) -> Dict[str, Style]:
        """The art styles that we use."""
        return {art_style.Name: art_style for art_style in self.first(ArtStyle, [])}  # type: ignore

    @property
    def palettes(self) -> List[Palette]:
        """A list of color palettes."""
        return self.categories[Palette]  # type: ignore

    @property
    def plugin_group_infos(self) -> List[PluginGroupInfo]:
        """A list of PluginGroupInfo."""
        return self.categories[PluginGroupInfo]  # type: ignore

    @property
    def document_data(self) -> Optional[DocumentData]:
        """The document metadata."""
        return self.first(DocumentData, None)  # type: ignore

    @property
    def text_document(self) -> Optional[TextDocument]:
        """The new style text definitions."""
        return self.first(TextDocument, None)  # type: ignore

    @property
    def gradients(self) -> Dict[str, Gradient]:
        """The gradient definitions."""
        return {gradient.name: gradient for gradient in self.categories[Gradient]}  # type: ignore
