# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Procset functionality within document setup."""
from inkai.parser.lazy import AIElement, EndOfLine


class ProcsetExec(AIElement):
    """<name> /initialize get exec
    Example:
        Adobe_cmykcolor /initialize get exec
    """

    spec = EndOfLine("exec")
