# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Reverse-engineered metadata for the AIDocument."""
from inkai.parser.data_hierarchy import DataHierarchyParser
from inkai.parser.lazy import CollectedSection, CommandParser, StartOfLineSection


class DocumentData(CollectedSection):
    """The hierarchical data structure with the metadata about the AI document.

    Example:
        %AI9_BeginDocumentData
        ...
        %AI9_EndDocumentData

    This is reverse-engineered.
    """

    spec = StartOfLineSection("%AI9_BeginDocumentData", "%AI9_EndDocumentData")

    def _parse(self, parser: CommandParser) -> dict:
        """Parse all the content."""
        return DataHierarchyParser.parse_lines(self.lines)
