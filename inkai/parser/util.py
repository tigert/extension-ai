# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Useful functions and definitions."""
import sys
from typing import NamedTuple

if sys.version_info < (3, 10):
    from typing_extensions import TypeAlias
else:
    from typing import TypeAlias


Point = NamedTuple("Point", [("x", float), ("y", float)])


__all__ = ["TypeAlias", "Point"]
