# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This is the module for all the live shapes."""
from __future__ import annotations
from typing import NamedTuple
from collections import defaultdict
from typing import Type, Dict, List, Any
from ..util import Point


class LiveShape:
    """This is a live shape definition inside of the ArtDictionary."""

    def __init__(self, art: dict):
        """Create a liveshape."""
        self._art = art

    @property
    def handler(self):
        """The handler that determines which shape it is."""
        return self._art["ai::LiveShape::HandlerName"]

    @property
    def params(self) -> Dict[str, Any]:
        """The parameters of the shape."""
        return self._art["ai::LiveShape::Params"]

    def is_rectangle(self) -> bool:
        """Whether this is a rectangle."""
        return self.handler == "ai::Rectangle"

    def convert(self, other):
        """Convert the other objects.

        Double dispatch.
        """


class Corner:
    r"""The corner of a rectangle.

    Imagine a rectangle with corners.
    The corners are round:

    right  +-----
          /
         /
        + left
        |            x center of rectangle

    y↑up  x→right


    The position of these corner points left and right are
    relative to the center of the rectangle,
    also they are NOT rotated around the center of the rectangle
    by the Angle of the rectangle.
    """

    def __init__(self, rectangle: Rectangle, index):
        """Create a new corner."""
        self.rectangle = rectangle
        self.index = index

    @property
    def RoundingType(self) -> str:
        """The rounding type of the corner.

        Examples: "Invalid"
        """
        return self.rectangle.params[f"ai::Rectangle::RoundingType::{self.index}"]

    @property
    def CornerType(self) -> str:
        """The corner type of the corner.

        Examples: "Invalid"
        """
        return self.rectangle.params[f"ai::Rectangle::CornerType::{self.index}"]

    @property
    def CornerRadius(self) -> float:
        """The radius of the corner."""
        return round(
            self.rectangle.params[f"ai::Rectangle::CornerRadius::{self.index}"], 8
        )

    def __eq__(self, other: Any):
        """a == b"""
        if not isinstance(other, Corner):
            return False
        return (
            self.CornerType == other.CornerType
            and abs(self.CornerRadius - other.CornerRadius) < 0.000001
            and self.RoundingType == other.RoundingType
        )

    def __hash__(self) -> int:
        """hash(self)"""
        return hash(self.RoundingType) ^ hash(self.CornerRadius) ^ hash(self.CornerType)

    @property
    def left(self) -> Point:
        """The left corner position if you look from the middle."""
        return self._corner_point(0)

    @property
    def right(self) -> Point:
        """The right corner position if you look from the middle."""
        return self._corner_point(1)

    def _corner_point(self, i: int):
        """Return the corner point."""
        x = self.rectangle.width / 2
        y = self.rectangle.height / 2
        if (self.index, i) in ((2, 0), (3, 1), (0, 0), (1, 1)):
            y -= self.CornerRadius
        else:
            x -= self.CornerRadius
        if self.index in (2, 1):
            x = -x
        if self.index in (0, 1):
            y = -y
        return Point(x, y)


Corners = NamedTuple(
    "Corners",
    [
        ("top_left", Corner),
        ("top_right", Corner),
        ("bottom_right", Corner),
        ("bottom_left", Corner),
    ],
)


class Rectangle(LiveShape):
    """This is a rectangle live shape.

    Example:

        Rectangle({
            '_type': 'Dictionary',
            'ai::LiveShape::HandlerName': 'ai::Rectangle',
            'ai::LiveShape::Params': {
                '_type': 'Dictionary',
                'ai::Rectangle::Angle': 0.0,
                'ai::Rectangle::CenterX': 8038.0,
                'ai::Rectangle::CenterY': 7898.0,
                'ai::Rectangle::Clockwise': True,
                'ai::Rectangle::CornerRadius::0': 47.5,
                'ai::Rectangle::CornerRadius::1': 47.5,
                'ai::Rectangle::CornerRadius::2': 47.5,
                'ai::Rectangle::CornerRadius::3': 47.5,
                'ai::Rectangle::CornerType::0': 'Normal',
                'ai::Rectangle::CornerType::1': 'Normal',
                'ai::Rectangle::CornerType::2': 'Normal',
                'ai::Rectangle::CornerType::3': 'Normal',
                'ai::Rectangle::Height': 180.0,
                'ai::Rectangle::InitialQuadrant': 3,
                'ai::Rectangle::RoundingType::0': 'Absolute',
                'ai::Rectangle::RoundingType::1': 'Absolute',
                'ai::Rectangle::RoundingType::2': 'Absolute',
                'ai::Rectangle::RoundingType::3': 'Absolute',
                'ai::Rectangle::Width': 230.0
            }
        })
    """

    def convert(self, other):
        """Convert the other object.

        Double dispatch.
        """
        other.convert_to_rectangle(self)

    @property
    def center(self) -> Point:
        """The center."""
        return Point(
            self.params["ai::Rectangle::CenterX"], self.params["ai::Rectangle::CenterY"]
        )

    @property
    def angle(self) -> float:
        """The angle in radial measure (not degrees!)."""
        return self.params["ai::Rectangle::Angle"]

    @property
    def width(self) -> float:
        """The center."""
        return self.params["ai::Rectangle::Width"]

    @property
    def height(self) -> float:
        """The center."""
        return self.params["ai::Rectangle::Height"]

    @property
    def corners(self) -> Corners:
        """The four corners of the rectangle.

        => [top-left, top-right, bottom-right, bottom-left]

            0 --- 1
            |     |
            3 --- 2
        """
        return Corners(*(Corner(self, (i + 2) % 4) for i in range(4)))

    def has_equal_corners(self) -> bool:
        """Whether all corners are equal."""
        corners = self.corners
        return corners[0] == corners[1] == corners[2] == corners[3]


class UnknownLiveShape(LiveShape):
    """This is a live shape for an unkown handler name."""


handler_name2live_shape: Dict[str, Type[LiveShape]] = defaultdict(
    lambda: UnknownLiveShape, {"ai::Rectangle": Rectangle}
)

__all__ = ["Point", "Rectangle", "LiveShape", "handler_name2live_shape"]
