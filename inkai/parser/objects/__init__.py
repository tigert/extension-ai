# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This package contains all the object definitions and layers.

Objects and layers can be encountered in the <script> Section of a document.
See page 31 AI Spec.

This module contains all objects in a parser named 'objects'.
"""
from ..lazy import CommandParser, CollectedSection
from ..color import Color
from . import layer
from . import new
from . import specified_1998
from . import path
from . import ai11_text
from . import image
from .object_section import ObjectSection
from ..data_section import Data
from .gradient_instance import Gradient

objects = CommandParser.from_specs(Color)
objects.add_all_from_module(layer)
objects.add_all_from_module(new)
objects.add_all_from_module(specified_1998)
objects.add_all_from_module(path)
objects.add_all_from_module(ai11_text)
objects.add_spec(Data)
objects.add_all_from_module(image)
objects.add_spec(Gradient)

__all__ = ["objects", "ObjectSection"]
