# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the AI file's Setup Section.

This section contains definitions and meta data that
is reused within the document.
"""
from __future__ import annotations
from inkai.parser.hierarchy_objects import Document
from inkai.parser.lazy import CommandParser
from inkai.parser.lines import Lines
from .setup.procset_exec import ProcsetExec
from .lines import Lines
from .header_comments import HeaderComments
from .prolog import Prolog
from .setup.setup import Setup
from .lazy import (
    CollectedSection,
    CommandParser,
    AIElement,
    StartOfLine,
    StartOfLineSection,
)
from typing import List, Optional
from .objects import objects
from collections import namedtuple
from math import ceil, floor
from .units import to_px


class PageTrailer(AIElement):
    """The page trailer.

    See AI Spec page 31.
    """

    spec = StartOfLine("%%PageTrailer")

    def __init__(self, lines: Lines):
        """Create a new PageTrailer."""
        super().__init__(lines)
        self.raw_content = lines.read_line()


class DocumentTrailer(CollectedSection):
    """The document trailer.

    See page 31 AI Spec.
    """

    spec = StartOfLineSection("%%Trailer", "%%EOF")

    @classmethod
    def add_to_inner(self, parser: CommandParser):
        """Parse the procset commands."""
        parser.add_spec(ProcsetExec)


class AIDocument(CollectedSection):
    """The AI document."""

    @classmethod
    def from_lines(cls, lines: Lines):
        """Create an AIDocument from lines."""
        return cls(lines)

    def __init__(self, lines: Lines):
        """Create a new AI document from lines in a file."""
        super().__init__(lines)
        self._header_comments = HeaderComments.collect_from_lines(lines)

    @classmethod
    def add_to_inner(cls, parser: CommandParser):
        """Add the inner sections to the parser."""
        parser.add_spec(Setup)
        parser.add_spec(Prolog)
        parser.add_spec(PageTrailer)
        parser.add_spec(DocumentTrailer)
        parser.add_spec(objects)

    @property
    def header_comments(self) -> HeaderComments:
        """The header comments of the document."""
        return self._header_comments

    @property
    def prolog(self) -> Prolog:
        """The prolog section of the document."""
        return self.first(Prolog)  # type: ignore

    @property
    def setup(self) -> Setup:
        """The setup section of the document."""
        return self.first(Setup)  # type: ignore

    @property
    def page_trailer(self) -> PageTrailer:
        """The PageTrailer section of the document."""
        return self.first(PageTrailer)  # type: ignore

    @property
    def document_trailer(self) -> DocumentTrailer:
        """The Trailer section of the document."""
        return self.first(DocumentTrailer)  # type: ignore

    @property
    def objects(self) -> List[AIElement]:
        """The visible content."""
        exclude = {
            self.setup,
            self.header_comments,
            self.prolog,
            self.page_trailer,
            self.document_trailer,
        }
        return [object for object in self.parsed_content if object not in exclude]

    @property
    def children(self):
        """The list of all elements in the document."""
        return (
            [self.header_comments, self.prolog, self.setup]
            + self.objects
            + [self.page_trailer, self.document_trailer]
        )

    @property
    def info(self) -> AIDocumentInformation:
        """Return information about the document."""
        return AIDocumentInformation(self)


class Page(namedtuple("Page", ("left", "bottom", "right", "top"))):
    """This is the position of a page."""

    @property
    def width(self) -> float:
        """The width of the page."""
        return self.right - self.left

    @property
    def height(self) -> float:
        """The height of the page."""
        return self.bottom - self.top


Point = namedtuple("Point", ("x", "y"))


class AIDocumentInformation:
    """Calculated information about the document."""

    def __init__(self, document: AIDocument):
        """Calculate information about the document."""
        self._document = document
        self._document_data = (
            self._document.setup.document_data.parsed_content  # type: ignore
            if document.setup.document_data
            else Document()
        )

    def _get_artboard(self) -> dict:
        """Return the artboard or {}

        Example:

            {'ArtboardUUID': '6cb1f831-1548-49bd-b4d1-54e72acb17a9',
            'DisplayMark': 0,
            'IsArtboardDefaultName': True,
            'IsArtboardSelected': True,
            'Name': 'ZeichenflÃ¤che 1',
            'PAR': 1.0,
            'PositionPoint1': PointRelToROrigin(x=0.0, y=128.0),
            'PositionPoint2': PointRelToROrigin(x=128.0, y=0.0),
            'RulerOrigin': Point(x=8127.0, y=8127.110168457),
            '_type': 'Dictionary'}
        """
        return self._document_data["Recorded"].get("ArtboardArray", [{}])[0]  # type: ignore

    @property
    def drawing_ruler_origin(self) -> Point:
        """Calculate the ruler origin from all the metadata available.

        This is stable across all AI versions.
            @property

        See docs/specification_amendments/units.md and AI Spec page 30.
        """
        # print(self._document.header_comments.file_format)
        art_size = self._document.header_comments.art_size
        crop_box = self._document.header_comments.cropmarks
        if crop_box is not None:
            width = crop_box.right - crop_box.left
            height = crop_box.top - crop_box.bottom
        elif art_size is not None:
            width = art_size.width
            height = art_size.height
        else:
            return Point(0, 0)
        templateBox = self._document.header_comments.template_box
        # print(width, height, templateBox, self.units, self.in_px)
        x = (width - templateBox.left - templateBox.right) / 2
        y = (height + templateBox.top + templateBox.bottom) / 2
        return Point(
            ceil(x) * self.large_canvas_scale, ceil(y) * self.large_canvas_scale
        )

    @property
    def canvas_ruler_origin(self) -> Optional[Point]:
        """This is the ruler origin for the canvas coordinate system.

        None if there is no canvas ruler origin.
        """
        if self._document.setup.text_document is not None:
            # The ruler origin (relative to the canvas) is always
            # initialized at floor(canvassize - artboardsize). However
            # the artboard size may change, or the artboard position,
            # or the artboard ruler origin. But none of that actually
            # changes the relation between Canvas and Drawing coords.
            # The only origin that stays constant is the TextDocument
            # ruler origin, introduced in CS (2003). Yes, really.
            _ = self._document.setup.text_document.parsed_content
            return self._document.setup.text_document.ruler_origin
        else:
            # For version < CS, it was not possible to move the
            # artboard, or have multiple artboards at all.
            # Trying to save a file with a moved first page as AI10
            # and reopening completely garbles the content.
            # But old files, or those with a single, unmoved
            # first page, should work fine with this code.
            # (Live shapes were introduced in v9)
            # This also appears how AI reconstructs the ruler origin
            # if the text document is deleted
            canvas_size = self._document.header_comments.canvas_size
            if canvas_size is None:
                return None
            page = self.drawing_first_page_position
            drawing_ruler_origin = self.drawing_ruler_origin

            return Point(
                floor(canvas_size / 2 - page.width / 2 + drawing_ruler_origin.x),
                floor(canvas_size / 2 - page.height / 2 + drawing_ruler_origin.y),
            )

    @property
    def canvas_first_page_position(self) -> Optional[Point]:
        """Compute the canvas ruler origin for the first page.

        None if there is no canvas ruler origin.
        """
        result = self._get_artboard().get("RulerOrigin")
        if result is not None:
            return result
        cro = self.canvas_ruler_origin
        if cro is None:
            return None
        page = self.drawing_first_page_position
        return Point(cro.x + page.left, cro.y + page.top)

    @property
    def drawing_first_page_position(self) -> Page:
        """This is information about the first page.

        This is calculated from the document.
        AI Spec page 30:

            All artwork elements, as well as the Bounding Box, Template Box, and Tile
            Box, are written out in coordinates relative to the ruler origin, with y
            increasing up and x increasing to the right, and bounds in the order left,
            bottom, right, top.

                y
                ^
                |<----- right ---->|
                |<- left ->|       |
                |          +-------+---
                |          |       |  ^
                |          | page  |  |
                |          |       |  | top
                |   bottom +-------+  |
                |      |              v
          (0,0) +--------------------------> x

        """
        art_board = self._get_artboard()
        if art_board:
            p1 = art_board["PositionPoint1"]
            p2 = art_board["PositionPoint2"]
            # print("art_board", art_board, self.large_canvas_scale)
            lcs = self.large_canvas_scale
            return Page(lcs * p1.x, lcs * (-p2.y), lcs * (p2.x), lcs * -p1.y)
        art_size = self._document.header_comments.art_size
        template_box = self._document.header_comments.template_box
        crop_box = self._document.header_comments.cropmarks
        if art_size is None:  # AI3 doesn't specify art_size
            height = -template_box.top - template_box.bottom
            return Page(
                0,
                0 if height < 0 else height,
                template_box.left + template_box.right,
                height if height < 0 else 0,
            )
        if crop_box is not None:
            # In some older version, the artboard array is sometimes omitted when
            # only one page exists. However CS4 only specifies the crop_box, the
            # art_size is wrong.
            width = crop_box.right - crop_box.left
            height = crop_box.top - crop_box.bottom
            x_origin = crop_box.left
            y_origin = -crop_box.top
        else:
            width = art_size.width
            height = art_size.height
            x_origin = -(width - template_box.left - template_box.right) / 2
            y_origin = -(height + template_box.bottom + template_box.top) / 2

        return Page(
            floor(x_origin),
            height + floor(y_origin),
            width + floor(x_origin),
            floor(y_origin),
        )

    @property
    def large_canvas_scale(self) -> float:
        """Return the header comment."""
        return self._document.header_comments.large_canvas_scale

    @property
    def units(self) -> str:
        """The units of this document.

        cm, mm, px, ...

        See docs/specification_amendments/units.md
        """
        return self._document.header_comments.units

    @property
    def in_px(self) -> float:
        """The scaling for the unit used to convert it to pixels.

        If the units of length are centimeters:
        length_in_px = length * in_px
        """
        return to_px(self.units)


__all__ = [
    "DocumentTrailer",
    "AIDocument",
    "AIDocumentInformation",
    "PageTrailer",
    "Page",
]
