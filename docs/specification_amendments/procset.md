<!--
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->

## BeginProcSet

This is added the `<prolog>` (page 14 AI Spec) as a `<procset>`.

Syntax:
```
<procset> ::= %%IncludeResource:procset <name>
              (or)
              %%BeginResource:...
              ...
              %%EndResource
              (or)
              %%BeginProcSet:...
              ...
              %%EndProcSet
```

The purpose seems to be to set a multiline proc.


## Procset Init

To initialize procsets, new ones were added:

```
<procset init> ::= <dict name>+ /initialize get exec
                 | <dict name> /<command> <arguments>*
```

Example:
```
%%BeginSetup # context
userdict /_useSmoothShade false put
userdict /_aicmykps false put
userdict /_forceToCMYK false put
Adobe_level2_AI5 /initialize get exec
Adobe_cshow /initialize get exec
Adobe_ColorImage_AI6 /initialize get exec
Adobe_shading_AI8 /initialize get exec
Adobe_Illustrator_AI5 /initialize get exec
```