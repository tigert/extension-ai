<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->


# Layers

See page 71 AI Spec

## Updated `Lb` command

*visible preview enabled/editable printing dimmed_images hasMultiLayerMasks visible? colorIndex red green blue template image_dim_percentage 0* **Lb**

Compared to the specification, the values `visible?`, `template`, `image_dim_percentage` and `0` were added. `visible?`, `template` and `image_dim_percentage` are present from AI8 on, the last `0` was added no later than CS2.

When setting a layer to hidden, `visible` and `visible?` are set to 0, and there is a `0 A` statement.

When setting a layer to Template, `visible=1`, `visible?=0`, but `1 A`.

## Example

```
%AI5_BeginLayer
0 1 1 1 0 0 0 0 79 128 255 0 50 0 Lb
(Layer 1) Ln
1 AE
%_/ArtDictionary :
%_/XMLUID : (Layer_1) ; (AI10_ArtUID) ,
%_;
%_
0 A
1 Xw
%AI5_BeginLayer
1 1 1 1 0 0 1 1 255 79 79 0 50 0 Lb
(Layer 2) Ln
0 AE
%_/ArtDictionary :
%_/XMLUID : (Layer_2) ; (AI10_ArtUID) ,
%_;
%_
0 A
0 Xw
LB
%AI5_EndLayer--
LB
%AI5_EndLayer--
```

The first layer (Layer 1) is invisible, the second layer (Layer 2) is a sublayer; **even though it is marked as visible, it inherits (actual) invisibility from Layer 1.** 

TBD: meaning of `Xw` and `AE`

## Nested Layers

In contrast to the specification, layers can be nested.
They do not seem to appear inside of groups though.
Also, they do not require objects to exist inside of them.

```
<layer> ::= %AI5_BeginLayer
            <Lb>
            <Ln>
            (
                <layer>
              | <object definitions>
            )
            <LB>
            %AI5_EndLayer
```

## ID

The id of a layer is inside this:

```
%_/XMLUID : (Layer_2) ; (AI10_ArtUID) ,
```
