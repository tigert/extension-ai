<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Mesh gradient

Gradient meshes work very similar to Inkscape, except that they are standalone objects, they can't be 
the fill of an other object (in other words, no stroke can be applied without a clone).

Mesh gradients are organized in cells. Cells may have a neighboring cell, in that case, nodes
and handles are duplicated.

Mesh gradients only apply to a single shape. For paths with multiple subpaths, AI either separates
them when creating a mesh gradient  (when they are non-overlapping) 
or refuses to create a gradient mesh (when they overlap).


```
/Mesh X!
%_2 /Version X#
%_/Cartesian /Type X#
%_[nx ny] /Size X#
%_/Data X#
<cell definition> * nx * ny
/End X!
```

A cell definition contains the position. For a rectangle, this is counted bottom to top, left to right.
Cell definitions are ordered by `posx` first.

A cell definition contains an arbitrary amount of nodes, and there are
* exactly 4 corner nodes (type `/N`).
* anchor nodes that existed on the path originally (type `/A`)
* helper nodes that were automatically created (and can not be edited by the user) to create a "natural" adaption to the shape (type `/S`)

All connections are cubic Bezier curvers. The handles may be retracted (happens when a mesh gradient
is created from an open shape).

```
<cell definition> =
<optional> %_/DeviceRGB /CS X#
%_[posx posy] /P X# 
%_[float] /R X#
<node definition> * n
```

At least for the corner nodes, the nodes seem to be ordered sequentially (counterclockwise, starting in the bottom left by default). Inkscape only has support for corner nodes in mesh gradients.
```
<node definition> =
%_[R G B opacity posx posy handle_1x handle_1y "1" handle_2x handle_2y "1"] /N X# <or>
%_[posx posy handle_1x handle_1y handle_2x handle_2y "1"] /A X# <or>
%_[posx posy handle_1x handle_1y handle_2x handle_2y "1"] /S X# 
```

`posx` and `posy` are specified in Drawing coordinates.

**Limitation**: We will achieve a perfect representation only for rectangle (or rectangle-like) meshes. 

For other meshes, there are two solutions: 
- construct the bezier for each edge and simplify it (optimize the handles such that it approximates the existing shape as good as possible).
- break the cells apart and interpolate the colors on the new corner nodes (nontrivial).
This is a 99.9% feature. For now, warn the user if there are more than 4 nodes in a cell.

**TODO**: The handle order is not yet clear. 
