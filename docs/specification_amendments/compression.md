<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Compression and AIPrivateData

The specification and the amendments in this folder are describing an "Adobe Illustrator file" (in this document, "data"). How to obtain this file differs from version to version.

## AI up to version 8

The `.ai` file is the data in plain text.

## AI from version 9 on

The `.ai` file is a PDF file, usually containing a preview or export of the drawing in PDF, in which the actual data is embedded in a compressed way. The file is split into an arbitrary number of PDF blocks, located on each page in `page["/PieceInfo"]["/Illustrator"]["/Private"]`. The blocks are named `/AIPrivateData\d+` and are ordered sequentially. Usually, the number of blocks is stored in `page["/PieceInfo"]["/Illustrator"]["/Private"]["/NumBlock"]`, but this does not seem to be required.


Depending on the AI version, different steps are required to extract the data.

### AI 9 - CS, file format versions 9.0-11.0

1. In each block, search for ``H\x89``. If it is not contained, discard the block.
2. Decompress each block individually using zlib-inflate.
3. Concatenate the resulting inflated blocks.

### CS 2 - CC Legacy, file format versions 12.0-17.0

1. Sequentially concatenate the data in all blocks.
2. Remove `%AI12_CompressedData` from the beginning of the first block
3. Decompress it using zlib-inflate.

### Current AI (v2020+), file format version 24.0

1. Sequentially concatenate the data in all blocks
2. Remove `%AI24_ZStandard_Data` from the beginning of the first block.
3. Decompress it using the [zstandard](https://en.wikipedia.org/wiki/Zstd) algorithm.

Note that AI is able to open the so-extracted plain text data directly, even if the file is newer than AI8.